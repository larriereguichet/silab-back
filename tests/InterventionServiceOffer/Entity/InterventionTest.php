<?php

namespace App\Tests\InterventionServiceOffer\Entity;

use App\Entity\Intervention;
use App\Entity\InterventionPriority;
use App\Entity\InterventionStatusHistorised;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;

class InterventionTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_getters(): void
    {
        $id = $this->generator->uuid();
        $createdAt = $this->generator->dateTimeBetween('-1 year', 'now');
        $interventionPriority = $this->generator->randomElement(InterventionPriority::cases())->value;
        $address = $this->generator->address();
        $description = $this->generator->realText();
        $imageData = $this->generator->text();
        $imageUrl = 'https://loremflickr.com/'
            .$this->generator->numberBetween(200, 360).'/'.$this->generator->numberBetween(200, 360)
            .'/street,trash'
            .'?lock='.$this->generator->numberBetween(1, 100);
        $actionType = 'SOUFFLAGE';
        $address = 'ici';
        $latitude = 46.5142;
        $longitude = 0.3655;
        $userEmail = 'jhonDoe@email.fr';
        $code = $this->generator->uuid();
        $creatorname = $this->generator->name();
        $interventionStatusHistory = [
               new InterventionStatusHistorised($id, $this->generator->word(), $this->generator->word(), new \DateTime()),
               new InterventionStatusHistorised($id, $this->generator->word(), $this->generator->word(), new \DateTime()),
               new InterventionStatusHistorised($id, $this->generator->word(), $this->generator->word(), new \DateTime()),
           ];

        $intervention = (new Intervention())
            ->setId($id)
            ->setPriority($interventionPriority)
            ->setActionType($actionType)
            ->setLatitude($latitude)
            ->setLongitude($longitude)
            ->setUserEmail($userEmail)
            ->setDescription($description)
            ->setImageB64(base64_encode($imageData))
            ->setCreatedAt($createdAt)
            ->setAddress($address)
            ->setCode($code)
            ->setCreatorName($creatorname)
            ->setStatusHistory($interventionStatusHistory)
            ->setImageUrl($imageUrl);

        $this->assertEquals($id, $intervention->getId());
        $this->assertEquals($interventionPriority, $intervention->getPriority());
        $this->assertEquals($description, $intervention->getDescription());
        $this->assertEquals($imageData, base64_decode($intervention->getImageB64()));
        $this->assertEquals($actionType, $intervention->getActionType());
        $this->assertEquals($latitude, $intervention->getLatitude());
        $this->assertEquals($longitude, $intervention->getLongitude());
        $this->assertEquals($userEmail, $intervention->getUserEmail());
        $this->assertEquals($id, $intervention->getId());
        $this->assertEquals($createdAt, $intervention->getCreatedAt());
        $this->assertEquals($address, $intervention->getAddress());
        $this->assertEquals($code, $intervention->getCode());
        $this->assertEquals($creatorname, $intervention->getCreatorname());
        $this->assertEquals($interventionStatusHistory, $intervention->getStatusHistory());
        $this->assertEquals($imageUrl, $intervention->getImageUrl());
    }

    /**
     * Ici on va principalement tester que la méthode est robuste jusqu'a la microseconde.
     * C'est notamment important car certain logiciels de GMAO (ex: Carl) peuvent créer automatiquement plusieurs
     * status très rapidement a la suite lors de la création d'interventions et nécéssite cette précision.
     *
     * @test
     */
    public function la_methode_get_current_status_retourne_le_status_le_plus_récent(): void
    {
        $status = new InterventionStatusHistorised(
            $this->generator->uuid(),
            $this->generator->text(15),
            $this->generator->email(),
            $this->generator->dateTimeThisMonth(),
        );

        $statusLePlusRécent = new InterventionStatusHistorised(
            $this->generator->uuid(),
            $this->generator->text(15),
            $this->generator->email(),
            \DateTime::createFromInterface($status->getCreatedAt())->add(\DateInterval::createFromDateString('1 microsecond')),
        );

        $statusLePlusVieux = new InterventionStatusHistorised(
            $this->generator->uuid(),
            $this->generator->text(15),
            $this->generator->email(),
            \DateTime::createFromInterface($status->getCreatedAt())->sub(\DateInterval::createFromDateString('1 microsecond')),
        );

        $statusHistory = [
            $status,
            $statusLePlusRécent,
            $statusLePlusVieux,
        ];

        $intervention = (new Intervention())
            ->setPriority($this->generator->randomElement(InterventionPriority::cases())->value)
            ->setStatusHistory($statusHistory);

        assertEquals($statusLePlusRécent, $intervention->getCurrentStatus());

        for ($nombreDeMélanges = 0; $nombreDeMélanges < 10; ++$nombreDeMélanges) {
            $intervention->setStatusHistory($this->generator->shuffleArray($statusHistory));
            assertEquals($statusLePlusRécent, $intervention->getCurrentStatus());
        }
    }
}
