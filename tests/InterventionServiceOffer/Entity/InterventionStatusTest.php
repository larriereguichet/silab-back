<?php

namespace App\Tests\InterventionServiceOffer\Entity;

use App\Entity\InterventionStatus;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class InterventionStatusTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_and_getters(): InterventionStatus
    {
        $interventionStatus = (new InterventionStatus(
            $label = $this->generator->word(),
        ))
        ->setInterventionId($interventionId = $this->generator->uuid())
        ->setCreatedBy($createdBy = $this->generator->word())
        ->setCreatedAt($createdAt = new \DateTime());

        $this->assertEquals($createdBy, $interventionStatus->getCreatedBy());
        $this->assertEquals($interventionId, $interventionStatus->getinterventionId());
        $this->assertEquals($createdAt, $interventionStatus->getCreatedAt());
        $this->assertEquals($label, $interventionStatus->getLabel());

        return $interventionStatus;
    }
}
