<?php

namespace App\Tests\InterventionServiceOffer\Entity;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class CarlConfigurationInterventionTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_getters(): void
    {
        $CarlConfigurationIntervention = new CarlConfigurationIntervention();
        $CarlConfigurationIntervention
            ->setTitle($title = $this->generator->words(2, true))
            ->setActionTypesMap($actionType = $this->generator->actionTypesMap())
            ->setDirections($directions = $this->generator->words(5))
            ->setCostCenters($costCenter = $this->generator->words(5));

        $this->assertEquals($title, $CarlConfigurationIntervention->getTitle());
        $this->assertEquals($actionType, $CarlConfigurationIntervention->getActionTypesMap());
        $this->assertEquals($directions, $CarlConfigurationIntervention->getDirections());
        $this->assertEquals($costCenter, $CarlConfigurationIntervention->getCostCenters());
        $this->assertEquals($title, $CarlConfigurationIntervention->getTitle());
    }
}
