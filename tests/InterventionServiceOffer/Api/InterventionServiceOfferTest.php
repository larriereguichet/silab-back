<?php

namespace App\Tests\InterventionServiceOffer\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Tests\InterventionServiceOffer\Factory\InterventionServiceOfferFactory;
use App\Tests\Shared\Api\ProtectedApiTestCase;
use App\Tests\Shared\Factory\UserFactory;
use Symfony\Component\Serializer\SerializerInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class InterventionServiceOfferTest extends ProtectedApiTestCase
{
    use ResetDatabase;
    use Factories;
    private Client $client;

    protected function setup(): void
    {
        $this->client = static::createClient();
    }

    /**
     *  @test
     */
    public function un_user_sans_le_role_adequat_se_voit_refuser_l_accès_à_une_offre_de_service_intervention(): void
    {
        $userSansRoleAdequatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userSansRoleAdequatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $serviceOfferId = InterventionServiceOfferFactory::createOne()->getId();

        $this->client->request('GET', "api/intervention-service-offers/$serviceOfferId", ['headers' => ['Accept' => 'application/json']]);

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function un_user_avec_le_role_adequat_peut_accéder_à_une_offre_de_service_intervention(): void
    {
        $serviceOffer = InterventionServiceOfferFactory::createOne();

        $userAvecRoleAdequatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ['SERVICEOFFER_'.$serviceOffer->getId().'_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleAdequatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $response = $this->client->request('GET', 'api/intervention-service-offers/'.$serviceOffer->getId(), ['headers' => ['Accept' => 'application/json']]);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(
            InterventionServiceOffer::class,
            null,
            'json',
            ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
        );

        $serviceOfferData = $response->toArray();

        $serializedServiceOffer = self::getContainer()->get(SerializerInterface::class)
            ->serialize(
                $serviceOffer->object(),
                'json',
                [
                    'skip_null_values' => true,
                    'groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS],
                ]
            );
        $this->assertSame($serializedServiceOffer, json_encode($serviceOfferData));
    }
}
