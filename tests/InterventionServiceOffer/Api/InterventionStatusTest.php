<?php

namespace App\Tests\InterventionServiceOffer\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Entity\Intervention;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Repository\Intervention\InterventionRepositoryInterface;
use App\Tests\InterventionServiceOffer\Factory\CarlConfigurationInterventionFactory;
use App\Tests\InterventionServiceOffer\Factory\InterventionServiceOfferFactory;
use App\Tests\Shared\Api\ProtectedApiTestCase;
use App\Tests\Shared\Exception\DataNeededForTestingNotAvailable;
use App\Tests\Shared\Factory\UserFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class InterventionStatusTest extends ProtectedApiTestCase
{
    use ResetDatabase;
    use Factories;

    private Client $client;
    private Proxy|InterventionServiceOffer $interventionServiceOfferProxyTested;
    private InterventionRepository $interventionRepository;

    protected function setup(): void
    {
        $this->client = static::createClient();
        $carlConfigurationInterventionProxy = CarlConfigurationInterventionFactory::createOne([
            'directions' => json_decode($_ENV['TEST_INTERVENTION_SUPERVISORS']),
            'costCenters' => json_decode($_ENV['TEST_INTERVENTION_COSTCENTERS']),
            'actionTypesMap' => json_decode($_ENV['TEST_INTERVENTION_ACTIONTYPES_MAP'], true),
        ]);
        $this->interventionServiceOfferProxyTested = InterventionServiceOfferFactory::createOne(['gmaoConfiguration' => $carlConfigurationInterventionProxy]);

        $this->interventionRepository = new InterventionRepository(
            $this->interventionServiceOfferProxyTested->object(),
        );
    }

    private function cloturer_l_intervention(int $serviceOfferId, string $interventionId, string $createdBy = 'john.doe@mail.fr'): mixed
    {
        return $this->client->request(
            'POST',
            "api/intervention-service-offers/$serviceOfferId/interventions/$interventionId/statuses",
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => [
                    'label' => 'CLOSED',
                ],
            ]
        );
    }

    private function une_intervention_clôturable(InterventionRepositoryInterface $interventionRepository): Intervention
    {
        $interventionsClôturables = $interventionRepository->findBy(
            ['statuses' => ['AWAITINGREAL', 'REQUEST']]
        );
        if (empty($interventionsClôturables)) {
            throw new DataNeededForTestingNotAvailable('Aucune intervention clôturable trouvée');
        }

        return array_values($interventionsClôturables)[0];
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_clotûrer_une_intervention_sans_être_authentifié(): void
    {
        try {
            $this->cloturer_l_intervention(
                $this->interventionServiceOfferProxyTested->getId(),
                $this->une_intervention_clôturable($this->interventionRepository)->getId()
            );

            $this->assertSame(401, $this->client->getResponse()->getStatusCode());
        } catch (DataNeededForTestingNotAvailable $exception) {
        }
    }

    /**
     * @test
     */
    public function un_user_sans_rôle_adéquat_ne_peut_pas_cloturer_d_intervention(): void
    {
        try {
            $userSansRolesProxy = UserFactory::createOne(
                [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => [],
                ]
            );

            $this->azureLogin(
                $this->client,
                $userSansRolesProxy->getEmail(),
                $_ENV['AZURE_TEST_PASSWORD']
            );

            $this->cloturer_l_intervention(
                $this->interventionServiceOfferProxyTested->getId(),
                $this->une_intervention_clôturable($this->interventionRepository)->getId()
            );

            $this->assertSame(403, $this->client->getResponse()->getStatusCode());
        } catch (DataNeededForTestingNotAvailable $exception) {
        }
    }

    /**
     * @test
     */
    public function un_user_avec_le_rôle_adéquat_peut_clôturer_une_intervention(): void
    {
        try {
            $interventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
            $userAvecRoleAdequatProxy = UserFactory::createOne(
                [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => ["SERVICEOFFER_{$interventionServiceOfferId}_ROLE_INTERVENTION_INTERVENANT"],
                ]
            );

            $this->azureLogin(
                $this->client,
                $userAvecRoleAdequatProxy->getEmail(),
                $_ENV['AZURE_TEST_PASSWORD']
            );

            $interventionClôturable = $this->une_intervention_clôturable($this->interventionRepository);

            $this->cloturer_l_intervention(
                $this->interventionServiceOfferProxyTested->getId(),
                $interventionClôturable->getId()
            );
            $this->assertSame(201, $this->client->getResponse()->getStatusCode());

            $refreshedInterventionTested = $this->interventionRepository->find(
                $interventionClôturable->getId()
            );

            $this->assertEquals(
                'CLOSED',
                $refreshedInterventionTested->getCurrentStatus()->getLabel()
            );
        } catch (DataNeededForTestingNotAvailable $exception) {
        }
    }
}
