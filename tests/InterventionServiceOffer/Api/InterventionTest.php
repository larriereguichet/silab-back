<?php

namespace App\Tests\InterventionServiceOffer\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Entity\Intervention;
use App\Entity\InterventionPriority;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Tests\InterventionServiceOffer\Factory\CarlConfigurationInterventionFactory;
use App\Tests\InterventionServiceOffer\Factory\InterventionServiceOfferFactory;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use App\Tests\Shared\Api\ProtectedApiTestCase;
use App\Tests\Shared\Factory\Ged\AlfrescoGedClientFactory;
use App\Tests\Shared\Factory\UserFactory;
use PHPUnit\Framework\Attributes\TestWith;
use Symfony\Component\Serializer\SerializerInterface;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class InterventionTest extends ProtectedApiTestCase
{
    use SetUpStockGeneratorTrait{
        SetUpStockGeneratorTrait::setUp as traitSetUp;
    }
    use ResetDatabase;
    use Factories;

    private Client $client;
    private Proxy|InterventionServiceOffer $interventionServiceOfferProxyTested;
    private InterventionRepository $interventionRepository;

    protected function setUp(): void
    {
        $this->traitSetUp();

        $this->client = static::createClient();

        $carlConfigurationInterventionProxy = CarlConfigurationInterventionFactory::createOne([
            'directions' => json_decode($_ENV['TEST_INTERVENTION_SUPERVISORS']),
            'costCenters' => json_decode($_ENV['TEST_INTERVENTION_COSTCENTERS']),
            'actionTypesMap' => json_decode($_ENV['TEST_INTERVENTION_ACTIONTYPES_MAP'], true),
        ]);

        $alfrescoGedClientProxy = AlfrescoGedClientFactory::createOne([
            'instanceUrl' => $_ENV['TEST_ALFRESCO_URL'],
            'username' => $_ENV['TEST_ALFRESCO_USER'],
            'password' => $_ENV['TEST_ALFRESCO_PASSWORD'],
        ]);

        $this->interventionServiceOfferProxyTested = InterventionServiceOfferFactory::createOne([
            'gmaoConfiguration' => $carlConfigurationInterventionProxy,
            'gedClient' => $alfrescoGedClientProxy,
            'gedClientConfiguration' => json_decode(
                $_ENV['TEST_GED_CLIENT_CONFIGURATION'],
                true
            ),
        ]);

        $this->interventionRepository = new InterventionRepository(
            $this->interventionServiceOfferProxyTested->object(),
        );
    }

    /**
     * @test
     */
    public function un_user_non_autentifié_se_voit_refuser_l_accès_aux_interventions(): void
    {
        $interventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $this->client->request(
            'GET',
            "api/intervention-service-offers/$interventionServiceOfferId/interventions",
            ['headers' => ['Accept' => 'application/json']]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function un_user_sans_le_role_adequat_se_voit_refuser_l_accès_aux_collections_d_interventions(): void
    {
        $userSansRolesProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userSansRolesProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $interventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();

        $filters = [];
        $this->client->request(
            'GET',
            "api/intervention-service-offers/$interventionServiceOfferId/interventions",
            ['headers' => ['Accept' => 'application/json'], 'query' => $filters,
            ]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function un_user_avec_le_role_adequat_peut_accéder_aux_collections_d_interventions(): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $userAvecRoleReadSurLOffreDeServiceProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ["SERVICEOFFER_{$sampleInterventionServiceOfferId}_ROLE_INTERVENTION_OBSERVATEUR"],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleReadSurLOffreDeServiceProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $filters = [];
        $this->client->request(
            'GET',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions",
            ['headers' => ['Accept' => 'application/json'], 'query' => $filters,
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertMatchesResourceCollectionJsonSchema(Intervention::class, null, 'json', ['groups' => ['Lister interventions']]);
    }

    /**
     * @test
     */
    public function un_user_non_autentifié_se_voit_refuser_l_accès_à_une_intervention(): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $sampleInterventionId = $this->une_nouvelle_intervention()->getId();

        $this->client->request(
            'GET',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions/$sampleInterventionId",
            ['headers' => ['Accept' => 'application/json']]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @param array<string,mixed> $parameters Permet d'écraser certaines valeurs par défault
     */
    private function une_nouvelle_intervention(array $parameters = []): Intervention
    {
        return $this->interventionRepository->save(
            (new Intervention())
                ->setPriority($this->generator->randomElement(InterventionPriority::cases())->value)
                ->setActionType($this->generator->randomKey(json_decode($_ENV['TEST_INTERVENTION_ACTIONTYPES_MAP'], true)))
                ->setLatitude($this->generator->latitude())
                ->setLongitude($this->generator->longitude())
                ->setUserEmail($_ENV['TEST_CARL_API_ACCOUNT_EMAIL'])
                ->setDescription(
                    array_key_exists('description', $parameters)
                    ?
                    $parameters['description']
                    :
                    $this->generator->text()
                )
                ->setImageB64(
                    array_key_exists('imageB64', $parameters)
                    ?
                    $parameters['imageB64']
                    :
                    base64_encode(
                        file_get_contents(
                            $this->generator->imageUrl(width: random_int(120, 780), height: random_int(120, 780), word: $this->generator->uuid())
                        )
                    )
                )
        );
    }

    /**
     * @test
     */
    public function un_user_sans_le_role_adequat_se_voit_refuser_l_accès_a_une_intervention(): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $sampleIntervention = $this->une_nouvelle_intervention();

        $nimporteQuelUser = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $nimporteQuelUser->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $this->client->request(
            'GET',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions/".$sampleIntervention->getId(),
            ['headers' => ['Accept' => 'application/json']]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function un_user_avec_les_rôles_adéquat_peut_accéder_a_une_intervention(): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $sampleImageB64 = base64_encode(
            file_get_contents(
                $this->generator->imageUrl(width: random_int(120, 780), height: random_int(120, 780), word: $this->generator->uuid())
            )
        );
        $sampleIntervention = $this->une_nouvelle_intervention(
            ['imageB64' => $sampleImageB64]
        );

        $userAvecRoleReadSurLOffreDeServiceProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ["SERVICEOFFER_{$sampleInterventionServiceOfferId}_ROLE_INTERVENTION_OBSERVATEUR"],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleReadSurLOffreDeServiceProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $interventionResponse = $this->client->request(
            'GET',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions/".$sampleIntervention->getId(),
            ['headers' => ['Accept' => 'application/json']]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(Intervention::class, null, 'json', ['groups' => ['Afficher détails intervention']]);

        // L'url de l'image est surchargé par le datahandler, nous testons l'image distinctement
        $this->assertEquals(
            $sampleImageB64,
            base64_encode(
                $this->client->request(
                    'GET',
                    $interventionResponse->toArray()['imageUrl']
                )->getContent()
            )
        );
        // nous mettons les imageUrl identiques pour la suite des tests
        $sampleInterventionWithInternalImageUrl = $sampleIntervention->setImageUrl($interventionResponse->toArray()['imageUrl']);

        // QUESTION_TECHNIQUE: devoir préciser le normalization_context est dommage, il devrait utiliser notre config par défaut
        $serializedSampleInterventionWithInternalImageUrl = self::getContainer()->get(SerializerInterface::class)->serialize($sampleInterventionWithInternalImageUrl, 'json', ['skip_null_values' => true, 'groups' => ['Afficher détails intervention']]);

        $this->assertSame($serializedSampleInterventionWithInternalImageUrl, $interventionResponse->getContent());
    }

    /**
     * @param array<string,mixed> $parameters Permet d'écraser certaines valeurs par défault
     *
     * @return array<string,mixed>
     */
    private function une_nouvelle_intervention_à_poster(array $parameters = []): array
    {
        $intervention = [
                'priority' => array_key_exists('priority', $parameters) ? $parameters['priority'] : 'normal',
                'actionType' => $this->generator->randomKey(json_decode($_ENV['TEST_INTERVENTION_ACTIONTYPES_MAP'], true)),
                'latitude' => $this->generator->latitude(),
                'longitude' => $this->generator->longitude(),
                'imageB64' => array_key_exists('imageB64', $parameters)
                    ?
                    $parameters['imageB64']
                    :
                    base64_encode(
                        file_get_contents(
                            $this->generator->imageUrl(width: random_int(120, 780), height: random_int(120, 780), word: $this->generator->uuid())
                        )
                    ),
                'description' => array_key_exists('imageB64', $parameters) ? $parameters['description'] : $this->generator->sentence(),
              ];

        return $intervention;
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_créer_une_intervention_sans_être_connecté(): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();

        $this->client->request(
            'POST',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions",
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => $this->une_nouvelle_intervention_à_poster(),
            ]
        );
        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_créer_une_intervention_sans_le_rôle_adéquat(): void
    {
        $userAvecRoleReadSurLOffreDeServiceProxy = UserFactory::createOne(
            [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => ['SERVICEOFFER_'.$this->interventionServiceOfferProxyTested->getId().'_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleReadSurLOffreDeServiceProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();

        $this->client->request(
            'POST',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions",
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => $this->une_nouvelle_intervention_à_poster(),
            ]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    // QUESTION_TECHNIQUE: pourquoi l'attribut TestWith provoque l'erreur "Attribute class "PHPUnit\Framework\Attributes\TestWith" not found" en dev mais pas sur la CI
    /**
     * @param array<string,mixed> $nouvelleInterventionParameters
     *
     * @testWith [{"imageB64":null}]
     *           [{"description":null}]
     *
     * @test
     */
    public function un_utilisateur_connecté_avec_le_rôle_adéquat_peut_créer_une_intervention(array $nouvelleInterventionParameters): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $userAvecRoleReadSurLOffreDeServiceProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ["SERVICEOFFER_{$sampleInterventionServiceOfferId}_ROLE_INTERVENTION_DECLARANT"],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleReadSurLOffreDeServiceProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $interventionACreer = $this->une_nouvelle_intervention_à_poster($nouvelleInterventionParameters);

        // Utilisation d'une latitude et longitude qui correspond au 26 Boulevard Solferino
        $interventionACreer['latitude'] = 46.584545;
        $interventionACreer['longitude'] = 0.338348;

        $response = $this->client->request(
            'POST',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions",
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => $interventionACreer,
            ]
        );

        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(Intervention::class, null, 'json', ['groups' => ['Afficher détails intervention']]);

        $responseData = json_decode($response->getContent(), true);

        // note importante, test désactivé en hotfix pour cause de soucis sur API ign (cf. src/Service/ReverseGeocoding.php)
        // $this->assertSame(
        //     $this->interventionServiceOfferProxyTested->getGmaoConfiguration()?->getActionTypesMap()[$interventionACreer['actionType']].', 26 Boulevard Solferino', $responseData['title']
        // );
        $this->assertSame($interventionACreer['priority'], $responseData['priority']);
        $this->assertSame($interventionACreer['actionType'], $responseData['actionType']);
        $this->assertSame($interventionACreer['latitude'], $responseData['latitude']);
        $this->assertSame($interventionACreer['longitude'], $responseData['longitude']);
        $this->assertSame($_ENV['AZURE_TEST_USERNAME'], $responseData['userEmail']);

        $this->assertEquals(
            $interventionACreer['imageB64'],
            is_null($responseData['imageUrl']) ? null :
            base64_encode(
                $this->client->request(
                    'GET',
                    $responseData['imageUrl']
                )->getContent()
            )
        );
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_modifier_une_intervention_sans_être_connecté(): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $sampleIntervention = $this->une_nouvelle_intervention();
        $interventionModifications = ['description' => ''];
        $this->client->request(
            'PUT',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions/".$sampleIntervention->getId(),
            [
            'headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => $interventionModifications,
            ]
        );
        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_modifier_une_intervention_sans_le_rôle_adéquat(): void
    {
        $userAvecRoleReadSurLOffreDeServiceProxy = UserFactory::createOne(
            [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => ['SERVICEOFFER_'.$this->interventionServiceOfferProxyTested->getId().'_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleReadSurLOffreDeServiceProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();

        $sampleIntervention = $this->une_nouvelle_intervention();

        $interventionModifications = ['description' => ''];

        $this->client->request(
            'PUT',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions/".$sampleIntervention->getId(),
            [
            'headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => $interventionModifications,
            ]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @param array<string,mixed> $initialInterventionParameters
     * @param array<string,mixed> $modifiedInterventionParameters
     *
     * @testWith [{"imageB64":null, "description":null},[]]
     *           [[],{"imageB64":null, "description":null}]
     *           [[],[]]
     *           [{"imageB64":null, "description":null},{"imageB64":null, "description":null}]
     *
     * @test
     */
    public function un_utilisateur_connecté_avec_le_rôle_adéquat_peut_modifier_une_intervention(array $initialInterventionParameters, array $modifiedInterventionParameters): void
    {
        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();
        $userAvecRoleReadSurLOffreDeServiceProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ["SERVICEOFFER_{$sampleInterventionServiceOfferId}_ROLE_INTERVENTION_DECLARANT"],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleReadSurLOffreDeServiceProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $sampleInterventionServiceOfferId = $this->interventionServiceOfferProxyTested->getId();

        $interventionInitiale = $this->une_nouvelle_intervention($initialInterventionParameters);

        // Utilisation d'une latitude et longitude qui correspond au 26 Boulevard Solferino
        $fullyModifiedIntervention = [
            'priority' => $this->generator->randomElement(InterventionPriority::cases())->value,
            'description' => $this->generator->text(),
            'actionType' => $this->generator->randomKey(json_decode($_ENV['TEST_INTERVENTION_ACTIONTYPES_MAP'], true)),
            'latitude' => 46.584545,
            'longitude' => 0.338348,
            'imageB64' => base64_encode(
                file_get_contents(
                    $this->generator->imageUrl(width: random_int(1, 1000), height: random_int(1, 1000), word: $this->generator->word())
                )
            ),
        ];

        $modificationsIntervention = array_merge($fullyModifiedIntervention, $modifiedInterventionParameters);

        $response = $this->client->request(
            'PUT',
            "api/intervention-service-offers/$sampleInterventionServiceOfferId/interventions/".$interventionInitiale->getId(),
            [
            'headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => $modificationsIntervention,
            ]
        );
        $this->assertMatchesResourceItemJsonSchema(Intervention::class, null, 'json', ['groups' => ['Afficher détails intervention']]);

        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');

        $responseData = json_decode($response->getContent(), true);
        // Note hotfix désactivé à cause des soucis avec le reverse geocoding
        // $this->assertNotEquals($interventionInitiale->getTitle(), $responseData['title']);
        $this->assertSame($modificationsIntervention['priority'], $responseData['priority']);
        $this->assertSame($modificationsIntervention['description'], $responseData['description']);
        $this->assertSame($modificationsIntervention['actionType'], $responseData['actionType']);
        $this->assertSame($modificationsIntervention['latitude'], $responseData['latitude']);
        $this->assertSame($modificationsIntervention['longitude'], $responseData['longitude']);
        $this->assertEquals(
            $modificationsIntervention['imageB64'],
            is_null($responseData['imageUrl']) ? null :
            base64_encode(
                $this->client->request(
                    'GET',
                    $responseData['imageUrl']
                )->getContent()
            )
        );
    }
}
