<?php

namespace App\Tests\InterventionServiceOffer\Factory;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<InterventionServiceOffer>
 *
 * @method        InterventionServiceOffer|Proxy                     create(array|callable $attributes = [])
 * @method static InterventionServiceOffer|Proxy                     createOne(array $attributes = [])
 * @method static InterventionServiceOffer|Proxy                     find(object|array|mixed $criteria)
 * @method static InterventionServiceOffer|Proxy                     findOrCreate(array $attributes)
 * @method static InterventionServiceOffer|Proxy                     first(string $sortedField = 'id')
 * @method static InterventionServiceOffer|Proxy                     last(string $sortedField = 'id')
 * @method static InterventionServiceOffer|Proxy                     random(array $attributes = [])
 * @method static InterventionServiceOffer|Proxy                     randomOrCreate(array $attributes = [])
 * @method static InterventionServiceOfferRepository|RepositoryProxy repository()
 * @method static InterventionServiceOffer[]|Proxy[]                 all()
 * @method static InterventionServiceOffer[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static InterventionServiceOffer[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static InterventionServiceOffer[]|Proxy[]                 findBy(array $attributes)
 * @method static InterventionServiceOffer[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static InterventionServiceOffer[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<InterventionServiceOffer> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<InterventionServiceOffer> createOne(array $attributes = [])
 * @phpstan-method static Proxy<InterventionServiceOffer> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<InterventionServiceOffer> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<InterventionServiceOffer> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<InterventionServiceOffer> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<InterventionServiceOffer> random(array $attributes = [])
 * @phpstan-method static Proxy<InterventionServiceOffer> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<InterventionServiceOffer> repository()
 * @phpstan-method static list<Proxy<InterventionServiceOffer>> all()
 * @phpstan-method static list<Proxy<InterventionServiceOffer>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<InterventionServiceOffer>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<InterventionServiceOffer>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<InterventionServiceOffer>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<InterventionServiceOffer>> randomSet(int $number, array $attributes = [])
 */
final class InterventionServiceOfferFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'image' => self::faker()->url(),
            'link' => "TODO: le link doit être généré à partir de l'id et pas stocké en bdd pour les service offers",
            'title' => self::faker()->words(5, true),
            'gmaoConfiguration' => CarlConfigurationInterventionFactory::new(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(InterventionServiceOffer $interventionServiceOffer): void {})
        ;
    }

    protected static function getClass(): string
    {
        return InterventionServiceOffer::class;
    }
}
