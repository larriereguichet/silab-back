<?php

namespace App\Tests\InterventionServiceOffer\Factory;

use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Gmao\Repository\CarlConfigurationInterventionRepository;
use App\Tests\Shared\Factory\Gmao\CarlClientFactory;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<CarlConfigurationIntervention>
 *
 * @method        CarlConfigurationIntervention|Proxy                     create(array|callable $attributes = [])
 * @method static CarlConfigurationIntervention|Proxy                     createOne(array $attributes = [])
 * @method static CarlConfigurationIntervention|Proxy                     find(object|array|mixed $criteria)
 * @method static CarlConfigurationIntervention|Proxy                     findOrCreate(array $attributes)
 * @method static CarlConfigurationIntervention|Proxy                     first(string $sortedField = 'id')
 * @method static CarlConfigurationIntervention|Proxy                     last(string $sortedField = 'id')
 * @method static CarlConfigurationIntervention|Proxy                     random(array $attributes = [])
 * @method static CarlConfigurationIntervention|Proxy                     randomOrCreate(array $attributes = [])
 * @method static CarlConfigurationInterventionRepository|RepositoryProxy repository()
 * @method static CarlConfigurationIntervention[]|Proxy[]                 all()
 * @method static CarlConfigurationIntervention[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static CarlConfigurationIntervention[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static CarlConfigurationIntervention[]|Proxy[]                 findBy(array $attributes)
 * @method static CarlConfigurationIntervention[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static CarlConfigurationIntervention[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<CarlConfigurationIntervention> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<CarlConfigurationIntervention> createOne(array $attributes = [])
 * @phpstan-method static Proxy<CarlConfigurationIntervention> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<CarlConfigurationIntervention> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<CarlConfigurationIntervention> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<CarlConfigurationIntervention> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<CarlConfigurationIntervention> random(array $attributes = [])
 * @phpstan-method static Proxy<CarlConfigurationIntervention> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<CarlConfigurationIntervention> repository()
 * @phpstan-method static list<Proxy<CarlConfigurationIntervention>> all()
 * @phpstan-method static list<Proxy<CarlConfigurationIntervention>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<CarlConfigurationIntervention>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<CarlConfigurationIntervention>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<CarlConfigurationIntervention>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<CarlConfigurationIntervention>> randomSet(int $number, array $attributes = [])
 */
final class CarlConfigurationInterventionFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'carlClient' => CarlClientFactory::new(),
            'title' => self::faker()->words(5, true),
            'photoAvantInterventionDoctypeId' => $_ENV['TEST_CARL_INTERVENTION_PHOTO_AVANT_INTER_DOCTYPE_ID'],
            'actionTypesMap' => json_decode($_ENV['TEST_INTERVENTION_ACTIONTYPES_MAP'], true),
            'woViewUrlSubPath' => $_ENV['TEST_WO_VIEW_URL_SUB_PATH'],
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(CarlConfigurationIntervention $carlConfigurationIntervention): void {})
        ;
    }

    protected static function getClass(): string
    {
        return CarlConfigurationIntervention::class;
    }
}
