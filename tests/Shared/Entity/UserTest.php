<?php

namespace App\Tests\Shared\Entity;

use App\Shared\User\Entity\User;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_and_getters(): void
    {
        $user = new User();

        $email = $this->generator->email();
        $user->setEmail($email);
        $this->assertEquals($email, $user->getEmail());
        $this->assertEquals($email, $user->getUserIdentifier());

        $roles = ['ROLE_USER'];
        $user->setRoles($roles);
        $this->assertEquals($roles, $user->getRoles());
    }
}
