<?php

namespace App\Tests\Shared\Entity;

use App\Shared\Carl\Entity\CarlClient;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class CarlClientTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_getters(): void
    {
        $carlClient = new CarlClient();
        $carlClient
            ->setTitle($title = $this->generator->words(2, true))
            ->setBaseUrl($baseUrl = $this->generator->url())
            ->setLogin($login = $this->generator->userName())
            ->setPassword($password = $this->generator->password())
            ->setPriorityMapping($priorityMapping = [$this->generator->word() => $this->generator->word()])
            ->setPageLimit($pageLimit = $this->generator->numberBetween(10 - 10000));

        $this->assertEquals($title, $carlClient->getTitle());
        $this->assertEquals($baseUrl, $carlClient->getBaseUrl());
        $this->assertEquals($login, $carlClient->getLogin());
        $this->assertEquals($password, $carlClient->getPassword());
        $this->assertEquals($priorityMapping, $carlClient->getPriorityMapping());
        $this->assertEquals($pageLimit, $carlClient->getPageLimit());
        $this->assertEquals($title, $carlClient->__toString());
    }
}
