<?php

namespace App\Tests\Shared\Entity;

use App\Entity\Ged\AlfrescoGedClient;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class AlfrescoGedClientTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_getters(): void
    {
        $alfrescoGedClient = new AlfrescoGedClient();
        $alfrescoGedClient
            ->setTitle($title = $this->generator->words(2, true))
            ->setUsername($username = $this->generator->word())
            ->setPassword($pasword = $this->generator->password())
            ->setInstanceUrl($instanceUrl = $this->generator->url());

        $this->assertEquals($title, $alfrescoGedClient->getTitle());
        $this->assertEquals($username, $alfrescoGedClient->getUsername());
        $this->assertEquals($pasword, $alfrescoGedClient->getPassword());
        $this->assertEquals($instanceUrl, $alfrescoGedClient->getInstanceUrl());
    }
}
