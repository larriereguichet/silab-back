<?php

namespace App\Tests\Shared\Entity;

use App\Entity\ServiceOffer\Template\RedirectServiceOffer;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class RedirectServiceOfferTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_getters(): void
    {
        $serviceOfferRedirect = new RedirectServiceOffer();
        $serviceOfferRedirect
            ->setRedirectUrl($redirectUrl = $this->generator->url())
            ->setLink($link = $this->generator->url())
            ->setTitle($title = $this->generator->words(2, true))
            ->setImage($image = $this->generator->url());

        $this->assertEquals($redirectUrl, $serviceOfferRedirect->getRedirectUrl());
        $this->assertEquals($link, $serviceOfferRedirect->getLink());
        $this->assertEquals($title, $serviceOfferRedirect->getTitle());
        $this->assertEquals($image, $serviceOfferRedirect->getImage());
    }
}
