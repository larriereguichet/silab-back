<?php

namespace App\Tests\Shared\Providers;

class DirectionProvider extends \Faker\Provider\Base
{
    public function directionName(): string
    {
        $availableDirections = [
            'Pôle Entretien Maintenance Sécurité',
            'CCAS Immobilier',
            'Direction Achats Moyens Généraux',
            'Direction Communication',
            'Direction Cabinet du Maire - Président',
            'Centre de Ressources Sud Ouest',
            'Direction Budget Finances',
            'Alimentation et agriculture',
        ];

        return $this->randomElement($availableDirections);
    }
}
