<?php

namespace App\Tests\Shared\Providers;

class InterventionProvider extends \Faker\Provider\Base
{
    public function interventionCode(): string
    {
        return $this->numerify('OT-#######');
    }

    /**
     * @return array<string,string>
     */
    public function actionTypesMap(): array
    {
        $actionTypesMap = [];
        $uniqueGenerator = $this->unique();

        for (
            $actionTypeNumber = 0;
            $actionTypeNumber < $this->numberBetween(2, 10);
            ++$actionTypeNumber
        ) {
            $actionTypesMap[$uniqueGenerator->randomAscii()] = $uniqueGenerator->word();
        }

        return $actionTypesMap;
    }
}
