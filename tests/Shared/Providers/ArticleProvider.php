<?php

namespace App\Tests\Shared\Providers;

class ArticleProvider extends \Faker\Provider\Base
{
    public function articleQuantity(): ArticleQuantity
    {
        $floatUnits = [
            'litres',
            'mètres',
            'cm',
            'cm²',
            'm3',
            'g',
        ];
        $integerUnits = [
            'unités',
            'boites',
            'cartons',
            'palettes',
        ];

        $combinations = [
            ['decimals' => 2, 'compatibleUnits' => $floatUnits],
            ['decimals' => 0, 'compatibleUnits' => $integerUnits],
        ];

        $selectedCombination = $this->randomElement($combinations);

        $quantity = new ArticleQuantity(
            $this->randomFloat($selectedCombination['decimals'], 0, 100),
            $this->randomElement($selectedCombination['compatibleUnits'])
        );

        return $quantity;
    }

    public function articleName(): string
    {
        return $this->randomElement(
            [
                'clef de 12',
                'clef de 13.789',
                'bidon d\'étincelles à meuler',
                'recolleuse à copeaux',
                'câble Wi-Fi',
                'Pince à couper le courant',
                'mesuromètre',
                'mouchoirs',
                'super trucs',
                'choses utiles',
                'choses inutiles',
            ]
        );
    }
}
