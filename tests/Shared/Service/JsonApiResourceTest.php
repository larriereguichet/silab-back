<?php

namespace App\Tests\Shared\Service;

use App\Exception\RuntimeException;
use App\Service\JsonApiResource;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class JsonApiResourceTest extends TestCase
{
    use SetUpStockGeneratorTrait{
        setUp as setUpGenerator;
    }

    /** @var array<mixed> */
    private array $jsonApiResourceResponseArray;
    private JsonApiResource $jsonApiResource;

    protected function setUp(): void
    {
        $this->setUpGenerator();

        $resourceResponseArray =
            [
                'id' => $this->generator->uuid(),
                'type' => $this->generator->word(),
                'attributes' => [
                    'attribute1' => $this->generator->word(),
                    'attribute2' => $this->generator->randomNumber(),
                ],
                'relationships' => [
                    'includedRelationship' => [
                        'data' => [
                            'id' => $this->generator->uuid(),
                            'type' => $this->generator->word(),
                        ],
                    ],
                    'nullRelationship' => [
                        'data' => null,
                    ],
                ],
            ];

        $includedResourcesResponseArray = [
            [
                'id' => $resourceResponseArray['relationships']['includedRelationship']['data']['id'],
                'type' => $resourceResponseArray['relationships']['includedRelationship']['data']['type'],
                'attributes' => [
                    'attribute1' => $this->generator->word(),
                ],
            ],
        ];

        $this->jsonApiResourceResponseArray = [
            'data' => [$resourceResponseArray],
            'included' => $includedResourcesResponseArray,
        ];

        $this->jsonApiResource = new JsonApiResource(
            $this->jsonApiResourceResponseArray['data'][0],
            $this->jsonApiResourceResponseArray['included']
        );
    }

    public function test_constructor(): void
    {
        $jsonApiResourceFixture = new JsonApiResource(
            $this->jsonApiResourceResponseArray['data'][0],
            $this->jsonApiResourceResponseArray['included']
        );

        $this->assertNotNull($jsonApiResourceFixture);
    }

    public function test_get_attributes(): void
    {
        $this->assertEquals(
            $this->jsonApiResourceResponseArray['data'][0]['attributes']['attribute1'],
            $this->jsonApiResource->getAttribute('attribute1')
        );

        $this->assertEquals(
            $this->jsonApiResourceResponseArray['data'][0]['attributes']['attribute2'],
            $this->jsonApiResource->getAttribute('attribute2')
        );

        $this->expectException(\Exception::class);
        $this->jsonApiResource->getAttribute('unknownAttribute');
    }

    public function test_get_relationship_attributes(): void
    {
        $this->assertEquals(
            $this->jsonApiResourceResponseArray['included'][0]['attributes']['attribute1'],
            $this->jsonApiResource->getRelationship('includedRelationship')->getAttribute('attribute1')
        );

        $this->expectException(\Exception::class);
        $this->jsonApiResource->getRelationship('unknownRelationship')->getAttribute('anyAttribute');

        $this->expectException(RuntimeException::class);
        $corruptedIncludedResources = [];
        $corruptedJsonApiResourse = new JsonApiResource(
            $this->jsonApiResourceResponseArray['data'][0],
            $corruptedIncludedResources
        );
        $corruptedJsonApiResourse->getRelationship('includedRelationship');
    }

    public function test_get_id(): void
    {
        $this->assertEquals(
            $this->jsonApiResourceResponseArray['data'][0]['id'],
            $this->jsonApiResource->getId()
        );
    }
}
