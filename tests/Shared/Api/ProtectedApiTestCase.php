<?php

namespace App\Tests\Shared\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Symfony\Bundle\Test\Client;
use TheNetworg\OAuth2\Client\Provider\Azure;
use TheNetworg\OAuth2\Client\Token\AccessToken;

class ProtectedApiTestCase extends ApiTestCase
{
    /**
     * Paramètre le client donné pour que les prochaines appels soient authentifié
     * via un token généré avec les credentials fournis.
     */
    protected function azureLogin(Client &$client, string $email, string $password): void
    {
        $accessToken = $this->getIdToken($email, $password);

        $client->setDefaultOptions(['headers' => ['authorization' => 'Bearer '.$accessToken]]);
    }

    private function getIdToken(string $email, string $password): string
    {
        /** @var AccessToken $accessToken */
        $accessToken = $this->getIdTokenFromROPC($email, $password);

        return $accessToken->getIdToken();
    }

    /**
     * /!\ Unsecure, only use this for testing purposes.
     */
    private function getIdTokenFromROPC(string $username, string $password): object
    {
        /** @var Azure $azureProvider */
        $azureProvider = static::getContainer()->get(Azure::class);

        return $azureProvider->getAccessToken('password', [
            'username' => $username,
            'password' => $password,
            'scope' => join(' ', $azureProvider->scope),
        ]);
    }
}
