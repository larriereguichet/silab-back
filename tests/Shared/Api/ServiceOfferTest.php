<?php

namespace App\Tests\Shared\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\Tests\InterventionServiceOffer\Factory\InterventionServiceOfferFactory;
use App\Tests\LogisticServiceOffer\Factory\LogisticServiceOfferFactory;
use App\Tests\Shared\Factory\UserFactory;
use Symfony\Component\Serializer\SerializerInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ServiceOfferTest extends ProtectedApiTestCase
{
    use ResetDatabase;
    use Factories;
    private Client $client;

    protected function setup(): void
    {
        $this->client = static::createClient();
    }

    public function un_utilisateur_non_authentifié_ne_voit_aucune_offre_de_service(): void
    {
        LogisticServiceOfferFactory::createOne();
        InterventionServiceOfferFactory::createOne();

        $serviceOffers = $this->client->request('GET', 'api/service-offers', ['headers' => ['Accept' => 'application/json']])
            ->toArray();

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertEmpty($serviceOffers);
    }

    public function un_utilisateur_authentifié_ne_voit_aucune_offre_de_service_si_il_n_a_pas_de_roles(): void
    {
        LogisticServiceOfferFactory::createOne();
        InterventionServiceOfferFactory::createOne();

        $userSansRolesProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );
        $this->azureLogin(
            $this->client,
            $userSansRolesProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $serviceOffers = $this->client->request('GET', 'api/service-offers', ['headers' => ['Accept' => 'application/json']])
            ->toArray();

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertEmpty($serviceOffers);
    }

    /**
     * @test
     */
    public function un_utilisateur_authentifié_ne_voit_que_les_offres_de_service_pour_lesquelles_il_a_les_droits(): void
    {
        InterventionServiceOfferFactory::createOne();
        $serviceOfferLiéAuUser = LogisticServiceOfferFactory::createOne();

        $userAvecDroitSurOffreDeService = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ['SERVICEOFFER_'.$serviceOfferLiéAuUser->getId().'_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE'],
            ]
        );
        $allowedServiceOffersIds = [$serviceOfferLiéAuUser->getId()];

        $this->azureLogin(
            $this->client,
            $userAvecDroitSurOffreDeService->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $serviceOffers = $this->client->request('GET', 'api/service-offers', ['headers' => ['Accept' => 'application/json']])
            ->toArray();
        $serviceOffersIds = array_column($serviceOffers, 'id');

        $this->assertMatchesResourceCollectionJsonSchema(ServiceOffer::class, null, 'json');
        $this->assertNotEmpty($serviceOffers);
        $this->assertEqualsCanonicalizing($allowedServiceOffersIds, $serviceOffersIds);
    }

    /**
     *  @test
     */
    public function un_user_sans_le_role_adequat_se_voit_refuser_l_accès_à_une_offre_de_service(): void
    {
        $userSansRoleAdequatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userSansRoleAdequatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $serviceOfferId = LogisticServiceOfferFactory::createOne()->getId();

        $this->client->request('GET', "api/service-offers/$serviceOfferId", ['headers' => ['Accept' => 'application/json']]);

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     *  @test
     */
    public function un_user_avec_le_role_adequat_peut_accéder_à_une_offre_de_service(): void
    {
        $serviceOffer = LogisticServiceOfferFactory::createOne();
        $serviceOfferId = $serviceOffer->getId();

        $userAvecRoleAdequatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ["SERVICEOFFER_{$serviceOfferId}_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE"],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleAdequatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $response = $this->client->request('GET', "api/service-offers/$serviceOfferId", ['headers' => ['Accept' => 'application/json']]);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(ServiceOffer::class, null, 'json');
        $serializedServiceOffer = self::getContainer()->get(SerializerInterface::class)->serialize($serviceOffer->object(), 'json', ['skip_null_values' => true]);
        $this->assertSame($serializedServiceOffer, $response->getContent());
    }
}
