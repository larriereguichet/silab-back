<?php

namespace App\Tests\Shared\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Shared\User\Entity\User;
use App\Tests\Shared\Factory\UserFactory;
use Symfony\Component\Serializer\SerializerInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class UserTest extends ProtectedApiTestCase
{
    use ResetDatabase;
    use Factories;
    private Client $client;

    protected function setup(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_récupérer_la_liste_des_utilisateurs_si_on_est_pas_connecté(): void
    {
        $this->client->request(
            'GET',
            'api/users',
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json']]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_récupérer_la_liste_des_utilisateurs_si_on_à_pas_rôle_adéquat(): void
    {
        $userAvecRolesAdéquatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRolesAdéquatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $this->client->request(
            'GET',
            'api/users',
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json']]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_peut_récupérer_la_liste_des_utilisateurs_si_on_à_le_rôle_adéquat(): void
    {
        $userAvecRolesAdéquatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ['ROLE_READ_USER'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRolesAdéquatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $this->client->request(
            'GET',
            'api/users',
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json']]
        );

        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');

        $this->assertMatchesResourceCollectionJsonSchema(User::class, null, 'json');
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_récupérer_un_utilisateur_si_on_est_pas_connecté(): void
    {
        $userTested = UserFactory::createOne();
        $this->client->request(
            'GET',
            'api/users/'.$userTested->getId(),
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json']]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_récupérer_un_utilisateur_si_on_à_pas_rôle_adéquat(): void
    {
        $userAvecRolesAdéquatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRolesAdéquatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $userTested = UserFactory::createOne();
        $this->client->request(
            'GET',
            'api/users/'.$userTested->getId(),
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json']]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_peut_récupérer_un_utilisateur_si_on_à_le_rôle_adéquat(): void
    {
        $userAvecRolesAdéquatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ['ROLE_READ_USER'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRolesAdéquatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $userTested = UserFactory::createOne();
        $userReturned = $this->client->request(
            'GET',
            'api/users/'.$userTested->getId(),
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json']]
        );

        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(User::class, null, 'json');

        $serializedUserTested = self::getContainer()->get(SerializerInterface::class)->serialize($userTested->object(), 'json', ['skip_null_values' => true]);
        $this->assertSame($serializedUserTested, $userReturned->getContent());
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_modifier_un_utilisateur_si_on_est_pas_connecté(): void
    {
        $userTested = UserFactory::createOne();
        $userModifications = [
                    'email' => 'email@test.net',
                    'roles' => ['ROLE_TEST'],
                ];
        $this->client->request(
            'PATCH',
            'api/users/'.$userTested->getId(),
            ['headers' => ['Content-type' => 'application/merge-patch+json', 'Accept' => 'application/json'],
            'json' => $userModifications,
            ]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_modifier_un_utilisateur_si_on_à_pas_le_rôle_adéquat(): void
    {
        $userAvecRolesAdéquatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRolesAdéquatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $userTested = UserFactory::createOne();
        $userModifications = [
                    'email' => 'email@test.net',
                    'roles' => ['ROLE_TEST'],
                ];
        $this->client->request(
            'PATCH',
            'api/users/'.$userTested->getId(),
            ['headers' => ['Content-type' => 'application/merge-patch+json', 'Accept' => 'application/json'],
            'json' => $userModifications,
            ]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function on_peut_modifier_un_utilisateur_si_on_à_le_rôle_adéquat(): void
    {
        $userAvecRolesAdéquatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ['ROLE_WRITE_USER'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRolesAdéquatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $userTested = UserFactory::createOne();
        $userModifications = [
                    'email' => 'email@test.net',
                    'roles' => ['ROLE_TEST'],
                ];
        $userReturned = $this->client->request(
            'PATCH',
            'api/users/'.$userTested->getId(),
            ['headers' => ['Content-type' => 'application/merge-patch+json', 'Accept' => 'application/json'],
            'json' => $userModifications,
            ]
        )->toArray();

        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');

        $this->assertMatchesResourceItemJsonSchema(User::class, null, 'json');

        $this->assertSame($userTested->object()->getRoles(), $userReturned['roles']);
    }
}
