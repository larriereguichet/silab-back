<?php

namespace App\Tests\Shared\User\Security\Azure;

use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TheNetworg\OAuth2\Client\Provider\Azure;
use TheNetworg\OAuth2\Client\Token\AccessToken;

use function PHPUnit\Framework\assertEquals;

class ResourceOwnerPasswordCredentialsTest extends WebTestCase
{
    public function test_token_version(): void
    {
        JWT::$timestamp = null;
        $azureProvider = static::getContainer()->get(Azure::class);
        assertEquals(
            'https://login.microsoftonline.com/'.$_ENV['AZURE_TENANT_ID'].'/oauth2/v2.0/authorize',
            $azureProvider->getBaseAuthorizationUrl()
        );
        assertEquals($_ENV['AZURE_CLIENT_ID'], $azureProvider->getClientId());

        /** @var AccessToken $accessToken */
        $accessToken = $azureProvider->getAccessToken('password', [
            'username' => $_ENV['AZURE_TEST_USERNAME'],
            'password' => $_ENV['AZURE_TEST_PASSWORD'],
            'scope' => join(' ', $azureProvider->scope),
        ]);

        $keys = $azureProvider->getJwtVerificationKeys();
        // $tokenClaims = (array) JWT::decode($accessToken, $keys);

        // assertEquals('2.0', $tokenClaims['ver']);
    }
}
