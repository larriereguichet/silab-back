<?php

namespace App\Tests\Shared\User\Security\Azure;

use App\Shared\User\Security\Azure\AzureAccessTokenHandler;
use App\Tests\Shared\Factory\UserFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TheNetworg\OAuth2\Client\Provider\Azure;
use TheNetworg\OAuth2\Client\Token\AccessToken;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class AzureAccessTokenHandlerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;

    /**
     * @test
     */
    public function on_peut_récupérer_le_badge_d_un_utilisateur_grace_au_token_fourni(): void
    {
        $user = UserFactory::createOne([
            'email' => $_ENV['AZURE_TEST_USERNAME'],
        ]
        );

        /** @var Azure $azureProvider */
        $azureProvider = static::getContainer()->get(Azure::class);
        /** @var AccessToken $accessToken */
        $accessToken = $azureProvider->getAccessToken('password', [
            'username' => $_ENV['AZURE_TEST_USERNAME'],
            'password' => $_ENV['AZURE_TEST_PASSWORD'],
            'scope' => join(' ', $azureProvider->scope),
        ]);

        /** @var AzureAccessTokenHandler $accessTokenHandler */
        $accessTokenHandler = static::getContainer()->get(AzureAccessTokenHandler::class);
        $userBadgeRécupéréGrâceAuToken = $accessTokenHandler->getUserBadgeFrom($accessToken->getIdToken());
        // $userDuBadge = $userBadgeRécupéréGrâceAuToken->getUser();

        // if (!$userDuBadge instanceof User) {
        //     throw new RuntimeException('Le User récupéré depuis le badge n\'est pas du bon type');
        // }

        // $this->assertEquals(
        //     $user->getId(),
        //     $userDuBadge->getId()
        // );
    }

    /**
     * @test
     */
    public function un_user_est_créé_automatiquement_en_bdd_lorsqu_un_utilisateur_se_connecte_pour_la_première_fois_avec_un_token_valide(): void
    {
        // $accessTokenHandler->getUserBadgeFrom($accessToken->getIdToken());
    }
}
