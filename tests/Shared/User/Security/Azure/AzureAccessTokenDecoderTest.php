<?php

namespace App\Tests\Shared\User\Security\Azure;

use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TheNetworg\OAuth2\Client\Provider\Azure;

use function PHPUnit\Framework\assertEquals;

class AzureAccessTokenDecoderTest extends WebTestCase
{
    public function test_decode_v2_access_token(): void
    {
        $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiIyYWY0NDI3Mi1mYzdlLTQ5MDktOWRlOS03NGRiYmZhYmE4OTMiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vZGY0MjQ3NzAtZDQ4YS00MWFhLWI0ZGQtNzMzODEyY2M3ZmFiL3YyLjAiLCJpYXQiOjE2NzgwOTMwMjgsIm5iZiI6MTY3ODA5MzAyOCwiZXhwIjoxNjc4MDk2OTI4LCJuYW1lIjoiU0lMQUIgVGVzdCIsIm5vbmNlIjoiZDU0ZjE5NjAtY2ExMC00MTg1LThhYWMtODgxOGMzOWNkZDI2Iiwib2lkIjoiMjkxYjA3ODktMzJmNy00NjQ2LWEyZDctZDliMmNkZGM0Mzc1IiwicHJlZmVycmVkX3VzZXJuYW1lIjoic2lsYWIudGVzdEBmb250YWluZS1sZS1jb210ZS5mciIsInJoIjoiMC5BVEVBY0VkQzM0clVxa0cwM1hNNEVzeF9xM0pDOUNwLV9BbEpuZWwwMjctcnFKTXhBTnMuIiwic3ViIjoiVldfNTYxV1BJeU5EbElCZUxDbnJTU2pVYktDbnE1bEFCUzkyWDBUTEsyWSIsInRpZCI6ImRmNDI0NzcwLWQ0OGEtNDFhYS1iNGRkLTczMzgxMmNjN2ZhYiIsInV0aSI6IlJyQ1VHYVhydDBTTXAzWUdWbWNFQUEiLCJ2ZXIiOiIyLjAifQ.N5do4KRsD43b6CGR0NPA0HaY3Ju-XLJXvfpL1iYSfAYEvUSMzmCc8vqIHxjDICuoU1Jh8dMlcrMrxCix4X5Y4f2jQFUk6jCP7O9hD9aX835BgTZtUspgwRi03Yz28jVA-oSLx0aJUejQfXwBM0Ti_28Cq4uNYkDPHfiytEDC_7MJ2ahaZDR9OjLxMz4RBTZQAMbCO-jzo_ABaJgvZlUX7hdAbRlCQopNqGAau8RMmpIBd0lkK1M73xKzLkSVyhECZAg5M-01iURPnZS913nNf49gOXLYUb8ooqZeUg5bgW5DE6FmC4Cw_8gDGA7SAk3qmgcdSd3FQuF1MFt0uMAlRA';
        $keys = (new Azure(['tenant' => $_ENV['AZURE_TENANT_ID']], []))->getJwtVerificationKeys();
        JWT::$timestamp = (new \DateTime('2023-03-06T11:00:00+0100'))->getTimestamp();
        $tokenClaims = (array) JWT::decode($accessToken, $keys);
        assertEquals('2.0', $tokenClaims['ver']);
        JWT::$timestamp = null;
    }

    public function test_decode_v1_access_token(): void
    {
        try {
            $accessToken = 'eyJ0eXAiOiJKV1QiLCJub25jZSI6IlFvXy0wOXh1N2dYcERqTGlNMElVM2tjT1gyRzdnMWZBTWtscm9YMkRVMU0iLCJhbGciOiJSUzI1NiIsIng1dCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9kZjQyNDc3MC1kNDhhLTQxYWEtYjRkZC03MzM4MTJjYzdmYWIvIiwiaWF0IjoxNjc4MTE4NTk5LCJuYmYiOjE2NzgxMTg1OTksImV4cCI6MTY3ODEyMzQyNSwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IkUyWmdZSWdXWnl6bml2YjZlSy9XM1o1OUVTZWZpRys3NTJidFB3MzZmaEdtZXhtbS9Uc3NmVEhVZlZaZXZFNUQ0Z1F4cDhJREFBPT0iLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6IltTSV1sYWIgVjIiLCJhcHBpZCI6IjJhZjQ0MjcyLWZjN2UtNDkwOS05ZGU5LTc0ZGJiZmFiYTg5MyIsImFwcGlkYWNyIjoiMSIsImZhbWlseV9uYW1lIjoiU0lMQUIiLCJnaXZlbl9uYW1lIjoiVGVzdCIsImlkdHlwIjoidXNlciIsImlwYWRkciI6Ijc3LjE5Mi4xMTUuNjkiLCJuYW1lIjoiVEVTVCBTSUxBQiIsIm9pZCI6ImI1NWFmZTgzLWFlNmQtNDhmMC04MzdjLTQwM2EzMGE2YTliNiIsInBsYXRmIjoiMTQiLCJwdWlkIjoiMTAwMzIwMDE5NkNDRTgzMiIsInJoIjoiMC5BVEVBY0VkQzM0clVxa0cwM1hNNEVzeF9xd01BQUFBQUFBQUF3QUFBQUFBQUFBQXhBR3MuIiwic2NwIjoib3BlbmlkIHByb2ZpbGUgVXNlci5SZWFkIGVtYWlsIiwic3ViIjoiQnBLRk1CNU9KZm1Db0NMb1lPVUNWVzJnM1EzZGM1NUdqRFBQUlFheGdCcyIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJFVSIsInRpZCI6ImRmNDI0NzcwLWQ0OGEtNDFhYS1iNGRkLTczMzgxMmNjN2ZhYiIsInVuaXF1ZV9uYW1lIjoidGVzdC5zaWxhYkBncmFuZHBvaXRpZXJzLmZyIiwidXBuIjoidGVzdC5zaWxhYkBncmFuZHBvaXRpZXJzLmZyIiwidXRpIjoiYXllSVFNV2NEa2lIZGN2VF9TcXNBQSIsInZlciI6IjEuMCIsIndpZHMiOlsiYjc5ZmJmNGQtM2VmOS00Njg5LTgxNDMtNzZiMTk0ZTg1NTA5Il0sInhtc19zdCI6eyJzdWIiOiItX2pVNWNYSkpHeEs3eDRJdUNqNDJxTDVWZUpoZndaMG41b1NPem90UGxNIn0sInhtc190Y2R0IjoxNTE2ODY4MzMyLCJ4bXNfdGRiciI6IkVVIn0.W1t4SC0PCWj4gsDeAvPMDBmWWXdJV-k9LOk2yZcYDYieotp0gUwRPB1TeFVpZ2t-aGGNYJpHICxK4e2_wdyV2puPQMzyYW0lhkSRe3Le2L5JfiVYxEl5PuGj8uvKNo0aqeynCEus2lCrwXeXYtITXbmXvyd6uw84OD8oe8LkQtbXg4jDT28qROViPxSFegj3eC8LRynwbphua2DZ4kSvpLtsCeU9YOy7JL0luyMZ6x94I8wskWPL3uAlG2vKNGPJvrWI4kM65Y-5vnCRgcr6Ip7tdwN4HYdoPeIqdNViZVeY-EKia9knTHuVQGayv-SBHNfWxXtz1air4X8VXEarKw';
            $keys = (new Azure(['tenant' => $_ENV['AZURE_TENANT_ID']], []))->getJwtVerificationKeys();
            JWT::$timestamp = (new \DateTime('2023-03-06T10:00:00+0100'))->getTimestamp();

            $this->expectException(SignatureInvalidException::class);
            JWT::decode($accessToken, $keys);
        } finally {
            JWT::$timestamp = null;
        }
    }

    public function test_attempt_decode_v1_access_token_with_v2(): void
    {
        $accessToken = 'eyJ0eXAiOiJKV1QiLCJub25jZSI6IlFvXy0wOXh1N2dYcERqTGlNMElVM2tjT1gyRzdnMWZBTWtscm9YMkRVMU0iLCJhbGciOiJSUzI1NiIsIng1dCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9kZjQyNDc3MC1kNDhhLTQxYWEtYjRkZC03MzM4MTJjYzdmYWIvIiwiaWF0IjoxNjc4MTE4NTk5LCJuYmYiOjE2NzgxMTg1OTksImV4cCI6MTY3ODEyMzQyNSwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IkUyWmdZSWdXWnl6bml2YjZlSy9XM1o1OUVTZWZpRys3NTJidFB3MzZmaEdtZXhtbS9Uc3NmVEhVZlZaZXZFNUQ0Z1F4cDhJREFBPT0iLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6IltTSV1sYWIgVjIiLCJhcHBpZCI6IjJhZjQ0MjcyLWZjN2UtNDkwOS05ZGU5LTc0ZGJiZmFiYTg5MyIsImFwcGlkYWNyIjoiMSIsImZhbWlseV9uYW1lIjoiU0lMQUIiLCJnaXZlbl9uYW1lIjoiVGVzdCIsImlkdHlwIjoidXNlciIsImlwYWRkciI6Ijc3LjE5Mi4xMTUuNjkiLCJuYW1lIjoiVEVTVCBTSUxBQiIsIm9pZCI6ImI1NWFmZTgzLWFlNmQtNDhmMC04MzdjLTQwM2EzMGE2YTliNiIsInBsYXRmIjoiMTQiLCJwdWlkIjoiMTAwMzIwMDE5NkNDRTgzMiIsInJoIjoiMC5BVEVBY0VkQzM0clVxa0cwM1hNNEVzeF9xd01BQUFBQUFBQUF3QUFBQUFBQUFBQXhBR3MuIiwic2NwIjoib3BlbmlkIHByb2ZpbGUgVXNlci5SZWFkIGVtYWlsIiwic3ViIjoiQnBLRk1CNU9KZm1Db0NMb1lPVUNWVzJnM1EzZGM1NUdqRFBQUlFheGdCcyIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJFVSIsInRpZCI6ImRmNDI0NzcwLWQ0OGEtNDFhYS1iNGRkLTczMzgxMmNjN2ZhYiIsInVuaXF1ZV9uYW1lIjoidGVzdC5zaWxhYkBncmFuZHBvaXRpZXJzLmZyIiwidXBuIjoidGVzdC5zaWxhYkBncmFuZHBvaXRpZXJzLmZyIiwidXRpIjoiYXllSVFNV2NEa2lIZGN2VF9TcXNBQSIsInZlciI6IjEuMCIsIndpZHMiOlsiYjc5ZmJmNGQtM2VmOS00Njg5LTgxNDMtNzZiMTk0ZTg1NTA5Il0sInhtc19zdCI6eyJzdWIiOiItX2pVNWNYSkpHeEs3eDRJdUNqNDJxTDVWZUpoZndaMG41b1NPem90UGxNIn0sInhtc190Y2R0IjoxNTE2ODY4MzMyLCJ4bXNfdGRiciI6IkVVIn0.W1t4SC0PCWj4gsDeAvPMDBmWWXdJV-k9LOk2yZcYDYieotp0gUwRPB1TeFVpZ2t-aGGNYJpHICxK4e2_wdyV2puPQMzyYW0lhkSRe3Le2L5JfiVYxEl5PuGj8uvKNo0aqeynCEus2lCrwXeXYtITXbmXvyd6uw84OD8oe8LkQtbXg4jDT28qROViPxSFegj3eC8LRynwbphua2DZ4kSvpLtsCeU9YOy7JL0luyMZ6x94I8wskWPL3uAlG2vKNGPJvrWI4kM65Y-5vnCRgcr6Ip7tdwN4HYdoPeIqdNViZVeY-EKia9knTHuVQGayv-SBHNfWxXtz1air4X8VXEarKw';
        $keys = (
        new Azure([
            'tenant' => $_ENV['AZURE_TENANT_ID'],
            'defaultEndPointVersion' => '2.0',
        ], [])
        )->getJwtVerificationKeys();
        JWT::$timestamp = (new \DateTime('2023-03-06T10:00:00+0100'))->getTimestamp();

        $this->expectException(SignatureInvalidException::class);

        JWT::decode($accessToken, $keys);

        JWT::$timestamp = null;
    }

    public function test_decode_id_token(): void
    {
        $idToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiIyYWY0NDI3Mi1mYzdlLTQ5MDktOWRlOS03NGRiYmZhYmE4OTMiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vZGY0MjQ3NzAtZDQ4YS00MWFhLWI0ZGQtNzMzODEyY2M3ZmFiL3YyLjAiLCJpYXQiOjE2NzgwOTMwMjgsIm5iZiI6MTY3ODA5MzAyOCwiZXhwIjoxNjc4MDk2OTI4LCJuYW1lIjoiU0lMQUIgVGVzdCIsIm5vbmNlIjoiZDU0ZjE5NjAtY2ExMC00MTg1LThhYWMtODgxOGMzOWNkZDI2Iiwib2lkIjoiMjkxYjA3ODktMzJmNy00NjQ2LWEyZDctZDliMmNkZGM0Mzc1IiwicHJlZmVycmVkX3VzZXJuYW1lIjoic2lsYWIudGVzdEBmb250YWluZS1sZS1jb210ZS5mciIsInJoIjoiMC5BVEVBY0VkQzM0clVxa0cwM1hNNEVzeF9xM0pDOUNwLV9BbEpuZWwwMjctcnFKTXhBTnMuIiwic3ViIjoiVldfNTYxV1BJeU5EbElCZUxDbnJTU2pVYktDbnE1bEFCUzkyWDBUTEsyWSIsInRpZCI6ImRmNDI0NzcwLWQ0OGEtNDFhYS1iNGRkLTczMzgxMmNjN2ZhYiIsInV0aSI6IlJyQ1VHYVhydDBTTXAzWUdWbWNFQUEiLCJ2ZXIiOiIyLjAifQ.N5do4KRsD43b6CGR0NPA0HaY3Ju-XLJXvfpL1iYSfAYEvUSMzmCc8vqIHxjDICuoU1Jh8dMlcrMrxCix4X5Y4f2jQFUk6jCP7O9hD9aX835BgTZtUspgwRi03Yz28jVA-oSLx0aJUejQfXwBM0Ti_28Cq4uNYkDPHfiytEDC_7MJ2ahaZDR9OjLxMz4RBTZQAMbCO-jzo_ABaJgvZlUX7hdAbRlCQopNqGAau8RMmpIBd0lkK1M73xKzLkSVyhECZAg5M-01iURPnZS913nNf49gOXLYUb8ooqZeUg5bgW5DE6FmC4Cw_8gDGA7SAk3qmgcdSd3FQuF1MFt0uMAlRA';
        $keys = (new Azure(['tenant' => $_ENV['AZURE_TENANT_ID']], []))->getJwtVerificationKeys();

        JWT::$timestamp = (new \DateTime('2023-03-06T10:00:00+0100'))->getTimestamp();

        $tokenClaims = (array) JWT::decode($idToken, $keys);
        assertEquals('2.0', $tokenClaims['ver']);
        assertEquals('2af44272-fc7e-4909-9de9-74dbbfaba893', $tokenClaims['aud']);

        JWT::$timestamp = null;
    }
}
