<?php

namespace App\Tests\Shared\User\Security\Azure;

use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpClient\HttpClient;
use TheNetworg\OAuth2\Client\Provider\Azure;
use TheNetworg\OAuth2\Client\Token\AccessToken;

class AzureGraphClientTest extends WebTestCase
{
    public function test_request_me(): void
    {
        $azureProvider = static::getContainer()->get(Azure::class);
        /** @var AccessToken $accessToken */
        $accessToken = $azureProvider->getAccessToken('password', [
            'username' => $_ENV['AZURE_TEST_USERNAME'],
            'password' => $_ENV['AZURE_TEST_PASSWORD'],
            'scope' => join(' ', $azureProvider->scope),
        ]);

        $client = HttpClient::createForBaseUri(
            'https://graph.microsoft.com/',
            [
                'auth_bearer' => $accessToken->getToken(),
                'headers' => ['Host' => 'graph.microsoft.com'],
            ]
        );
        $meResponse = $client->request('GET', '/v1.0/me');
        $me = json_decode($meResponse->getContent(), true);
        $this->assertStringContainsString('@', $me['userPrincipalName']);

        JWT::$timestamp = null;
    }
}
