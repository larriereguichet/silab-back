<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Tests\Shared\Exception;

class DataNeededForTestingNotAvailable extends \RuntimeException
{
}
