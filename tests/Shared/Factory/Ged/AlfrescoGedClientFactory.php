<?php

namespace App\Tests\Shared\Factory\Ged;

use App\Entity\Ged\AlfrescoGedClient;
use App\Repository\Ged\AlfrescoGedClientRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<AlfrescoGedClient>
 *
 * @method        AlfrescoGedClient|Proxy                     create(array|callable $attributes = [])
 * @method static AlfrescoGedClient|Proxy                     createOne(array $attributes = [])
 * @method static AlfrescoGedClient|Proxy                     find(object|array|mixed $criteria)
 * @method static AlfrescoGedClient|Proxy                     findOrCreate(array $attributes)
 * @method static AlfrescoGedClient|Proxy                     first(string $sortedField = 'id')
 * @method static AlfrescoGedClient|Proxy                     last(string $sortedField = 'id')
 * @method static AlfrescoGedClient|Proxy                     random(array $attributes = [])
 * @method static AlfrescoGedClient|Proxy                     randomOrCreate(array $attributes = [])
 * @method static AlfrescoGedClientRepository|RepositoryProxy repository()
 * @method static AlfrescoGedClient[]|Proxy[]                 all()
 * @method static AlfrescoGedClient[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static AlfrescoGedClient[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static AlfrescoGedClient[]|Proxy[]                 findBy(array $attributes)
 * @method static AlfrescoGedClient[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static AlfrescoGedClient[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<AlfrescoGedClient> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<AlfrescoGedClient> createOne(array $attributes = [])
 * @phpstan-method static Proxy<AlfrescoGedClient> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<AlfrescoGedClient> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<AlfrescoGedClient> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<AlfrescoGedClient> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<AlfrescoGedClient> random(array $attributes = [])
 * @phpstan-method static Proxy<AlfrescoGedClient> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<AlfrescoGedClient> repository()
 * @phpstan-method static list<Proxy<AlfrescoGedClient>> all()
 * @phpstan-method static list<Proxy<AlfrescoGedClient>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<AlfrescoGedClient>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<AlfrescoGedClient>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<AlfrescoGedClient>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<AlfrescoGedClient>> randomSet(int $number, array $attributes = [])
 */
final class AlfrescoGedClientFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'password' => self::faker()->password(),
            'instanceUrl' => self::faker()->url(),
            'title' => self::faker()->words(3, true),
            'username' => self::faker()->userName(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(AlfrescoGedClient $alfrescoGedClient): void {})
        ;
    }

    protected static function getClass(): string
    {
        return AlfrescoGedClient::class;
    }
}
