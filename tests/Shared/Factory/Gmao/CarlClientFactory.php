<?php

namespace App\Tests\Shared\Factory\Gmao;

use App\Repository\ServiceOffer\CarlClientRepository;
use App\Shared\Carl\Entity\CarlClient;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<CarlClient>
 *
 * @method        CarlClient|Proxy                     create(array|callable $attributes = [])
 * @method static CarlClient|Proxy                     createOne(array $attributes = [])
 * @method static CarlClient|Proxy                     find(object|array|mixed $criteria)
 * @method static CarlClient|Proxy                     findOrCreate(array $attributes)
 * @method static CarlClient|Proxy                     first(string $sortedField = 'id')
 * @method static CarlClient|Proxy                     last(string $sortedField = 'id')
 * @method static CarlClient|Proxy                     random(array $attributes = [])
 * @method static CarlClient|Proxy                     randomOrCreate(array $attributes = [])
 * @method static CarlClientRepository|RepositoryProxy repository()
 * @method static CarlClient[]|Proxy[]                 all()
 * @method static CarlClient[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static CarlClient[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static CarlClient[]|Proxy[]                 findBy(array $attributes)
 * @method static CarlClient[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static CarlClient[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<CarlClient> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<CarlClient> createOne(array $attributes = [])
 * @phpstan-method static Proxy<CarlClient> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<CarlClient> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<CarlClient> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<CarlClient> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<CarlClient> random(array $attributes = [])
 * @phpstan-method static Proxy<CarlClient> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<CarlClient> repository()
 * @phpstan-method static list<Proxy<CarlClient>> all()
 * @phpstan-method static list<Proxy<CarlClient>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<CarlClient>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<CarlClient>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<CarlClient>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<CarlClient>> randomSet(int $number, array $attributes = [])
 */
final class CarlClientFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'title' => self::faker()->words(5, true),
            'baseUrl' => $_ENV['TEST_CARL_BASEURL'],
            'login' => $_ENV['TEST_CARL_API_LOGIN'],
            'password' => $_ENV['TEST_CARL_API_PASSWORD'],
            'priorityMapping' => json_decode($_ENV['TEST_CARL_API_PRIORITY_MAPPING'], true),
            'pageLimit' => 100,
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(CarlClient $carlConfiguration): void {})
        ;
    }

    protected static function getClass(): string
    {
        return CarlClient::class;
    }
}
