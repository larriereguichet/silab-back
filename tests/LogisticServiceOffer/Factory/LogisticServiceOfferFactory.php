<?php

namespace App\Tests\LogisticServiceOffer\Factory;

use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<LogisticServiceOffer>
 *
 * @method        LogisticServiceOffer|Proxy                     create(array|callable $attributes = [])
 * @method static LogisticServiceOffer|Proxy                     createOne(array $attributes = [])
 * @method static LogisticServiceOffer|Proxy                     find(object|array|mixed $criteria)
 * @method static LogisticServiceOffer|Proxy                     findOrCreate(array $attributes)
 * @method static LogisticServiceOffer|Proxy                     first(string $sortedField = 'id')
 * @method static LogisticServiceOffer|Proxy                     last(string $sortedField = 'id')
 * @method static LogisticServiceOffer|Proxy                     random(array $attributes = [])
 * @method static LogisticServiceOffer|Proxy                     randomOrCreate(array $attributes = [])
 * @method static LogisticServiceOfferRepository|RepositoryProxy repository()
 * @method static LogisticServiceOffer[]|Proxy[]                 all()
 * @method static LogisticServiceOffer[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static LogisticServiceOffer[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static LogisticServiceOffer[]|Proxy[]                 findBy(array $attributes)
 * @method static LogisticServiceOffer[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static LogisticServiceOffer[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<LogisticServiceOffer> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<LogisticServiceOffer> createOne(array $attributes = [])
 * @phpstan-method static Proxy<LogisticServiceOffer> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<LogisticServiceOffer> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<LogisticServiceOffer> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<LogisticServiceOffer> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<LogisticServiceOffer> random(array $attributes = [])
 * @phpstan-method static Proxy<LogisticServiceOffer> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<LogisticServiceOffer> repository()
 * @phpstan-method static list<Proxy<LogisticServiceOffer>> all()
 * @phpstan-method static list<Proxy<LogisticServiceOffer>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<LogisticServiceOffer>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<LogisticServiceOffer>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<LogisticServiceOffer>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<LogisticServiceOffer>> randomSet(int $number, array $attributes = [])
 */
final class LogisticServiceOfferFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'image' => self::faker()->imageUrl(),
            'link' => "TODO: le link doit être généré à partir de l'id et pas stocké en bdd pour les service offers",
            'title' => self::faker()->words(5, true),
            'warehouseId' => self::faker()->uuid(),
            'gmaoConfiguration' => CarlGmaoConfigurationFactory::new(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(LogisticServiceOffer $logisticServiceOffer): void {})
        ;
    }

    protected static function getClass(): string
    {
        return LogisticServiceOffer::class;
    }
}
