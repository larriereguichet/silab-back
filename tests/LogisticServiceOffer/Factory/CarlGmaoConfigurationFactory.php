<?php

namespace App\Tests\LogisticServiceOffer\Factory;

use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\Tests\Shared\Factory\Gmao\CarlClientFactory;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<CarlGmaoConfiguration>
 *
 * @method        CarlGmaoConfiguration|Proxy                     create(array|callable $attributes = [])
 * @method static CarlGmaoConfiguration|Proxy                     createOne(array $attributes = [])
 * @method static CarlGmaoConfiguration|Proxy                     find(object|array|mixed $criteria)
 * @method static CarlGmaoConfiguration|Proxy                     findOrCreate(array $attributes)
 * @method static CarlGmaoConfiguration|Proxy                     first(string $sortedField = 'id')
 * @method static CarlGmaoConfiguration|Proxy                     last(string $sortedField = 'id')
 * @method static CarlGmaoConfiguration|Proxy                     random(array $attributes = [])
 * @method static CarlGmaoConfiguration|Proxy                     randomOrCreate(array $attributes = [])
 * @method static CarlGmaoConfigurationRepository|RepositoryProxy repository()
 * @method static CarlGmaoConfiguration[]|Proxy[]                 all()
 * @method static CarlGmaoConfiguration[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static CarlGmaoConfiguration[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static CarlGmaoConfiguration[]|Proxy[]                 findBy(array $attributes)
 * @method static CarlGmaoConfiguration[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static CarlGmaoConfiguration[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 *
 * @phpstan-method        Proxy<CarlGmaoConfiguration> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<CarlGmaoConfiguration> createOne(array $attributes = [])
 * @phpstan-method static Proxy<CarlGmaoConfiguration> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<CarlGmaoConfiguration> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<CarlGmaoConfiguration> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<CarlGmaoConfiguration> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<CarlGmaoConfiguration> random(array $attributes = [])
 * @phpstan-method static Proxy<CarlGmaoConfiguration> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<CarlGmaoConfiguration> repository()
 * @phpstan-method static list<Proxy<CarlGmaoConfiguration>> all()
 * @phpstan-method static list<Proxy<CarlGmaoConfiguration>> createMany(int $number, array|callable $attributes = [])
 * @phpstan-method static list<Proxy<CarlGmaoConfiguration>> createSequence(iterable|callable $sequence)
 * @phpstan-method static list<Proxy<CarlGmaoConfiguration>> findBy(array $attributes)
 * @phpstan-method static list<Proxy<CarlGmaoConfiguration>> randomRange(int $min, int $max, array $attributes = [])
 * @phpstan-method static list<Proxy<CarlGmaoConfiguration>> randomSet(int $number, array $attributes = [])
 */
final class CarlGmaoConfigurationFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'carlClient' =>  CarlClientFactory::new(),
            'title' => self::faker()->text(255),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(CarlGmaoConfiguration $carlGmaoConfiguration): void {})
        ;
    }

    protected static function getClass(): string
    {
        return CarlGmaoConfiguration::class;
    }
}
