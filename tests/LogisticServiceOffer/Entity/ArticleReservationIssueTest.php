<?php

namespace App\Tests\LogisticServiceOffer\Entity;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservationIssue;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class ArticleReservationIssueTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_and_getters(): void
    {
        $articleReservationIssue = new ArticleReservationIssue(
            $articleId = $this->generator->uuid(),
            $quantity = $this->generator->numberBetween(1, 50),
            $warehouseClerkEmail = $_ENV['TEST_CARL_API_ACCOUNT_EMAIL'],
            $interventionId = $this->generator->uuid(),
            $warehouseId = $this->generator->uuid(),
            $storageLocationId = $this->generator->uuid()
        );

        $this->assertEquals($articleId, $articleReservationIssue->getArticleId());
        $this->assertEquals($quantity, $articleReservationIssue->getQuantity());
        $this->assertEquals($warehouseClerkEmail, $articleReservationIssue->getWarehouseClerkEmail());
        $this->assertEquals($interventionId, $articleReservationIssue->getInterventionId());
        $this->assertEquals($warehouseId, $articleReservationIssue->getWarehouseId());
        $this->assertEquals($storageLocationId, $articleReservationIssue->getStorageLocationId());
    }
}
