<?php

namespace App\Tests\LogisticServiceOffer\Entity;

use App\LogisticServiceOffer\ArticleReservation\Entity\CarlArticleReservationId;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class ArticleReservationIdTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_and_getters(): CarlArticleReservationId
    {
        $articleReservationId = new CarlArticleReservationId(
            $articleId = $this->generator->uuid(),
            $interventionId = $this->generator->uuid(),
            $warehouseId = $this->generator->uuid(),
            $storageLocationId = $this->generator->uuid(),
        );

        // test de la clef primaire
        $this->assertEquals($articleId, $articleReservationId->articleId);
        $this->assertEquals($interventionId, $articleReservationId->interventionId);
        $this->assertEquals($warehouseId, $articleReservationId->warehouseId);
        $this->assertEquals($storageLocationId, $articleReservationId->storageLocationId);

        return $articleReservationId;
    }
}
