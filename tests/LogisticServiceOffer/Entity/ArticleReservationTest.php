<?php

namespace App\Tests\LogisticServiceOffer\Entity;

use App\Entity\InterventionStatusHistorised;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use PHPUnit\Framework\TestCase;

class ArticleReservationTest extends TestCase
{
    use SetUpStockGeneratorTrait;

    public function test_setters_and_getters(): ArticleReservation
    {
        $quantity = $this->generator->articleQuantity();
        $interventionId = $this->generator->uuid();

        $articleReservation = new ArticleReservation(
            $id = $this->generator->uuid(),
            $articleId = $this->generator->uuid(),
            $articleCode = $this->generator->uuid(),
            $articleDescription = $this->generator->articleName(),
            $quantity->getValue(),
            $availableQuantity = $quantity->getValue() + random_int(0, 100),
            $quantity->getUnit(),
            $warehouseId = $this->generator->uuid(),
            $warehouseCode = 'GENERAL',
            $storageLocationId = $this->generator->uuid(),
            $storageLocationCode = $this->generator->uuid(),
            $creatorName = "{$this->generator->firstName()} {$this->generator->lastName()}",
            $creatorDirectionName = $this->generator->directionName(),
            $expectedReceiptDate = $this->generator->dateTimeBetween('-1 year', '+1 month'),
            $interventionId,
            $interventionCode = $this->generator->interventionCode(),
            $interventionTitle = $this->generator->word(),
            $interventionStatusHistory = [
                new InterventionStatusHistorised($interventionId, $this->generator->word(), $this->generator->word(), new \DateTime()),
                new InterventionStatusHistorised($interventionId, $this->generator->word(), $this->generator->word(), new \DateTime()),
                new InterventionStatusHistorised($interventionId, $this->generator->word(), $this->generator->word(), new \DateTime()),
            ]
        );

        $this->assertEquals($id, $articleReservation->getId());
        $this->assertEquals($articleId, $articleReservation->getArticleId());
        $this->assertEquals($articleCode, $articleReservation->getArticleCode());
        $this->assertEquals($articleDescription, $articleReservation->getArticleDescription());
        $this->assertEquals($quantity->getValue(), $articleReservation->getQuantity());
        $this->assertEquals($availableQuantity, $articleReservation->getAvailableQuantity());
        $this->assertEquals($quantity->getUnit(), $articleReservation->getUnit());
        $this->assertEquals($warehouseId, $articleReservation->getWarehouseId());
        $this->assertEquals($warehouseCode, $articleReservation->getWarehouseCode());
        $this->assertEquals($storageLocationId, $articleReservation->getStorageLocationId());
        $this->assertEquals($storageLocationCode, $articleReservation->getStorageLocationCode());
        $this->assertEquals($creatorDirectionName, $articleReservation->getCreatorDirectionName());
        $this->assertEquals($creatorName, $articleReservation->getCreatorName());
        $this->assertEquals($expectedReceiptDate, $articleReservation->getExpectedReceiptDate());
        $this->assertEquals($interventionId, $articleReservation->getInterventionId());
        $this->assertEquals($interventionCode, $articleReservation->getInterventionCode());
        $this->assertEquals($interventionTitle, $articleReservation->getInterventionTitle());
        $this->assertEquals($interventionStatusHistory, $articleReservation->getInterventionStatusHistory());

        return $articleReservation;
    }
}
