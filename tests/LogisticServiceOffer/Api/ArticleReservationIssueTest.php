<?php

namespace App\Tests\LogisticServiceOffer\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservationIssue;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepository;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepositoryInterface;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Intervention\Repository\CarlInterventionRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\Tests\LogisticServiceOffer\Factory\LogisticServiceOfferFactory;
use App\Tests\Shared\Api\ProtectedApiTestCase;
use App\Tests\Shared\Exception\DataNeededForTestingNotAvailable;
use App\Tests\Shared\Factory\UserFactory;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ArticleReservationIssueTest extends ProtectedApiTestCase
{
    use ResetDatabase;
    use Factories;
    private Client $client;
    private Proxy|LogisticServiceOffer $logisticServiceOfferProxyTested;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->logisticServiceOfferProxyTested = LogisticServiceOfferFactory::createOne(['warehouseId' => $_ENV['TEST_STOCK_WAREHOUSE_ID']]);
    }

    private function getNewArticleReservationRepository(): ArticleReservationRepository
    {
        // Note, utiliser faker/foundry dans des dataproviders, c'est chiant, cf.
        //
        // 6) App\Tests\Api\ArticleReservationTest::un_user_authentifié_peut_accéder_aux_collections_de_réservations_par_magasin_et_intervention
        // The data provider specified for App\Tests\Api\ArticleReservationTest::un_user_authentifié_peut_accéder_aux_collections_de_réservations_par_magasin_et_intervention is invalid
        // Cannot get Foundry's configuration. If using faker in a data provider, consider passing attributes as a callable.
        //
        // /srv/api/tests/Api/ArticleReservationTest.php:167

        $carlGmaoConfiguration = $this->logisticServiceOfferProxyTested->getGmaoConfiguration();
        assert($carlGmaoConfiguration instanceof CarlGmaoConfiguration);
        $carlInterventionRepository = new CarlInterventionRepository($carlGmaoConfiguration);

        return new ArticleReservationRepository(
            $carlGmaoConfiguration,
            $carlInterventionRepository
        );
    }

    private function postArticleReservationIssue(string $articleReservationId, ArticleReservationIssue $articleReservationIssue): ResponseInterface
    {
        return $this->client->request(
            'POST',
            '/api/logistic-service-offers/'.$this->logisticServiceOfferProxyTested->getId().'/article-reservations/'.$articleReservationId.'/article-reservation-issues',
            ['headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
            'json' => [
                    'articleId' => $articleReservationIssue->getArticleId(),
                    'quantity' => $articleReservationIssue->getQuantity(),
                    'warehouseClerkEmail' => $articleReservationIssue->getWarehouseClerkEmail(),
                    'interventionId' => $articleReservationIssue->getInterventionId(),
                    'warehouseId' => $articleReservationIssue->getWarehouseId(),
                    'storageLocationId' => $articleReservationIssue->getStorageLocationId(),
                ],
            ]
        );
    }

    private function une_réservation_d_articles_avec_au_moins_autant_de_stock_que_la_quantité_demandée(string $warehouseId, ArticleReservationRepositoryInterface $articleReservationRepository): ArticleReservation
    {
        $articleReservations = $articleReservationRepository->findBy(['warehouseId' => $warehouseId]);

        foreach ($articleReservations as $articleReservation) {
            $ilYASuffisammentDeStockPourSatisfaireLaDemande = $articleReservation->getAvailableQuantity() >= $articleReservation->getQuantity();

            if ($ilYASuffisammentDeStockPourSatisfaireLaDemande) {
                return $articleReservation;
            }
        }

        throw new DataNeededForTestingNotAvailable('Impossible de trouver une réservation d\'articles avec un plus stock que la quantité demandée.');
    }

    /**
     * @test
     */
    public function une_sortie_de_stock_sans_etre_authentifié_est_refusé(): void
    {
        try {
            $articleReservation = $this->une_réservation_d_articles_avec_au_moins_autant_de_stock_que_la_quantité_demandée(
                $_ENV['TEST_STOCK_WAREHOUSE_ID'],
                $this->getNewArticleReservationRepository()
            );
            $articleReservationIssueToTest = new ArticleReservationIssue(
                $articleReservation->getArticleId(),
                $articleReservation->getQuantity(),
                $_ENV['TEST_CARL_API_ACCOUNT_EMAIL'],
                $articleReservation->getInterventionId() ?? null,
                $articleReservation->getWarehouseId() ?? null,
                $articleReservation->getStorageLocationId() ?? null
            );
            $this->postArticleReservationIssue($articleReservation->getId(), $articleReservationIssueToTest);
            $this->assertContains($this->client->getResponse()->getStatusCode(), [401, 500]);
        } catch (DataNeededForTestingNotAvailable $e) {
            // le test sera marqué risky
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @test
     */
    public function une_sortie_de_stock_sans_le_role_adéquat_est_refusé(): void
    {
        try {
            $articleReservation = $this->une_réservation_d_articles_avec_au_moins_autant_de_stock_que_la_quantité_demandée(
                $_ENV['TEST_STOCK_WAREHOUSE_ID'],
                $this->getNewArticleReservationRepository()
            );

            $userAvecRoleAdequatProxy = UserFactory::createOne(
                [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => [],
                ]
            );

            $this->azureLogin(
                $this->client,
                $userAvecRoleAdequatProxy->getEmail(),
                $_ENV['AZURE_TEST_PASSWORD']
            );

            $articleReservationIssueToTest = new ArticleReservationIssue(
                $articleReservation->getArticleId(),
                $articleReservation->getQuantity(),
                $_ENV['TEST_CARL_API_ACCOUNT_EMAIL'],
                $articleReservation->getInterventionId() ?? null,
                $articleReservation->getWarehouseId() ?? null,
                $articleReservation->getStorageLocationId() ?? null
            );
            $this->postArticleReservationIssue($articleReservation->getId(), $articleReservationIssueToTest);
            $this->assertSame(403, $this->client->getResponse()->getStatusCode());
        } catch (DataNeededForTestingNotAvailable $e) {
            // le test sera marqué risky
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @test
     */
    public function on_peut_sortir_tous_les_articles_d_une_réservation_si_la_quantité_disponible_est_suffisante(): void
    {
        try {
            $logisticServiceOfferId = $this->logisticServiceOfferProxyTested->getId();
            $userAvecRoleAdequatProxy = UserFactory::createOne(
                [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => ["SERVICEOFFER_{$logisticServiceOfferId}_ROLE_LOGISTIQUE_SORTIR_ARTICLES"],
                ]
            );

            $this->azureLogin(
                $this->client,
                $userAvecRoleAdequatProxy->getEmail(),
                $_ENV['AZURE_TEST_PASSWORD']
            );

            $articleReservation = $this->une_réservation_d_articles_avec_au_moins_autant_de_stock_que_la_quantité_demandée(
                $_ENV['TEST_STOCK_WAREHOUSE_ID'],
                $this->getNewArticleReservationRepository()
            );

            $this->assertGreaterThan(0, $articleReservation->getQuantity());
            $this->assertGreaterThanOrEqual($articleReservation->getQuantity(), $articleReservation->getAvailableQuantity());
            $articleReservationIssueToTest = new ArticleReservationIssue(
                $articleReservation->getArticleId(),
                $articleReservation->getQuantity(),
                $_ENV['TEST_CARL_API_ACCOUNT_EMAIL'],
                $articleReservation->getInterventionId() ?? null,
                $articleReservation->getWarehouseId() ?? null,
                $articleReservation->getStorageLocationId() ?? null
            );

            $response = $this->postArticleReservationIssue($articleReservation->getId(), $articleReservationIssueToTest);

            $response = $response->toArray();

            $this->assertResponseIsSuccessful();
            $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
            $this->assertEquals($articleReservationIssueToTest->getArticleId(), $response['articleId']);
            $this->assertEquals($articleReservationIssueToTest->getQuantity(), $response['quantity']);
            $this->assertEquals($articleReservationIssueToTest->getWarehouseClerkEmail(), $response['warehouseClerkEmail']);
            $this->assertEquals($articleReservationIssueToTest->getInterventionId(), $response['interventionId']);
            $this->assertEquals($articleReservationIssueToTest->getWarehouseId(), $response['warehouseId']);
            $this->assertEquals($articleReservationIssueToTest->getStorageLocationId(), $response['storageLocationId']);
            $this->assertEquals($_ENV['TEST_CARL_API_ACCOUNT_EMAIL'], $response['warehouseClerkEmail']);
        } catch (DataNeededForTestingNotAvailable $e) {
            // le test sera marqué risky
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function une_réservation_d_articles_avec_du_stock_mais_moins_que_la_quantité_demandée(string $warehouseId, ArticleReservationRepositoryInterface $articleReservationRepository): ArticleReservation
    {
        $articleReservations = $articleReservationRepository->findBy(['warehouseId' => $warehouseId]);

        foreach ($articleReservations as $articleReservation) {
            $laReservationADuStockDisponible = $articleReservation->getAvailableQuantity() > 0;
            $ilNYAPasAsséDeStockPourSatisfaireLaDemande = $articleReservation->getAvailableQuantity() < $articleReservation->getQuantity();

            if ($laReservationADuStockDisponible && $ilNYAPasAsséDeStockPourSatisfaireLaDemande) {
                return $articleReservation;
            }
        }

        throw new DataNeededForTestingNotAvailable('Impossible de trouver une réservation d\'articles avec un peu de stock mais moins que la quantité demandée.');
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_sortir_plus_d_articles_que_la_dispo_en_stock(): void
    {
        try {
            $logisticServiceOfferId = $this->logisticServiceOfferProxyTested->getId();
            $userAvecRoleAdequatProxy = UserFactory::createOne(
                [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => ["SERVICEOFFER_{$logisticServiceOfferId}_ROLE_LOGISTIQUE_SORTIR_ARTICLES"],
                ]
            );

            $this->azureLogin(
                $this->client,
                $userAvecRoleAdequatProxy->getEmail(),
                $_ENV['AZURE_TEST_PASSWORD']
            );

            $articleReservation = $this->une_réservation_d_articles_avec_du_stock_mais_moins_que_la_quantité_demandée(
                $_ENV['TEST_STOCK_WAREHOUSE_ID'],
                $this->getNewArticleReservationRepository()
            );

            $this->assertGreaterThan(0, $articleReservation->getAvailableQuantity());
            $this->assertLessThan($articleReservation->getQuantity(), $articleReservation->getAvailableQuantity());
            $articleReservationIssueToTest = new ArticleReservationIssue(
                $articleReservation->getArticleId(),
                $articleReservation->getQuantity(),
                $_ENV['TEST_CARL_API_ACCOUNT_EMAIL'],
                $articleReservation->getInterventionId() ?? null,
                $articleReservation->getWarehouseId() ?? null,
                $articleReservation->getStorageLocationId() ?? null
            );

            $this->postArticleReservationIssue($articleReservation->getId(), $articleReservationIssueToTest);
            $this->assertResponseStatusCodeSame(500);
        } catch (DataNeededForTestingNotAvailable $e) {
            // le test sera marqué risky
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function une_réservation_d_articles_avec_plus_de_stock_que_la_quantité_demandée(string $warehouseId, ArticleReservationRepositoryInterface $articleReservationRepository): ArticleReservation
    {
        $articleReservations = $articleReservationRepository->findBy(['warehouseId' => $warehouseId]);

        foreach ($articleReservations as $articleReservation) {
            $ilYAPlusDeStockQueLaQuantitéDemandée = $articleReservation->getAvailableQuantity() > $articleReservation->getQuantity();

            if ($ilYAPlusDeStockQueLaQuantitéDemandée) {
                return $articleReservation;
            }
        }

        throw new DataNeededForTestingNotAvailable('Impossible de trouver une réservation d\'articles avec un plus stock que la quantité demandée.');
    }

    /**
     * @test
     */
    public function on_ne_peut_pas_sortir_plus_d_articles_que_la_quantité_demandée(): void
    {
        try {
            $logisticServiceOfferId = $this->logisticServiceOfferProxyTested->getId();
            $userAvecRoleAdequatProxy = UserFactory::createOne(
                [
                    'email' => $_ENV['AZURE_TEST_USERNAME'],
                    'roles' => ["SERVICEOFFER_{$logisticServiceOfferId}_ROLE_LOGISTIQUE_SORTIR_ARTICLES"],
                ]
            );

            $this->azureLogin(
                $this->client,
                $userAvecRoleAdequatProxy->getEmail(),
                $_ENV['AZURE_TEST_PASSWORD']
            );

            $articleReservation = $this->une_réservation_d_articles_avec_plus_de_stock_que_la_quantité_demandée(
                $_ENV['TEST_STOCK_WAREHOUSE_ID'],
                $this->getNewArticleReservationRepository()
            );

            $this->assertGreaterThan(0, $articleReservation->getQuantity());
            $unPeuPlusQueLaQuantitéDemandée = $articleReservation->getQuantity() + 1;
            $this->assertGreaterThanOrEqual($unPeuPlusQueLaQuantitéDemandée, $articleReservation->getAvailableQuantity());
            $articleReservationIssueToTest = new ArticleReservationIssue(
                $articleReservation->getArticleId(),
                $unPeuPlusQueLaQuantitéDemandée,
                $_ENV['TEST_CARL_API_ACCOUNT_EMAIL'],
                $articleReservation->getInterventionId() ?? null,
                $articleReservation->getWarehouseId() ?? null,
                $articleReservation->getStorageLocationId() ?? null
            );

            $this->postArticleReservationIssue($articleReservation->getId(), $articleReservationIssueToTest);
            $this->assertResponseStatusCodeSame(500);
        } catch (DataNeededForTestingNotAvailable $e) {
            // le test sera marqué risky
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
