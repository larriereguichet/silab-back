<?php

namespace App\Tests\LogisticServiceOffer\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepository;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Intervention\Repository\CarlInterventionRepository;
use App\Tests\LogisticServiceOffer\Factory\LogisticServiceOfferFactory;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use App\Tests\Shared\Api\ProtectedApiTestCase;
use App\Tests\Shared\Factory\UserFactory;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class ArticleReservationTest extends ProtectedApiTestCase
{
    use SetUpStockGeneratorTrait{
        setUp as setUpGenerator;
    }
    use ResetDatabase;
    use Factories;
    private Client $client;

    protected function setUp(): void
    {
        $this->setUpGenerator();
        $this->client = static::createClient();
    }

    /**
     * @test
     */
    public function un_user_non_authentifié_se_voit_refuser_l_accès_aux_collections_de_réservations_par_magasin(): void
    {
        $logisticServiceOfferId = LogisticServiceOfferFactory::createOne()->getId();
        $this->client->request(
            'GET',
            'api/logistic-service-offers/'.$logisticServiceOfferId.'/article-reservations',
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());

        $logisticServiceOfferId = LogisticServiceOfferFactory::createOne()->getId();
        $interventionId = $this->generator->uuid();

        $this->client->request(
            'GET',
            'api/logistic-service-offers/'.$logisticServiceOfferId.'/interventions/'.$interventionId.'/article-reservations',
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function un_user_sans_le_role_adequat_se_voit_refuser_l_accès_aux_collections_de_réservations_par_magasin(): void
    {
        $userSansRolesProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userSansRolesProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $logisticServiceOfferId = LogisticServiceOfferFactory::createOne()->getId();

        $this->client->request(
            'GET',
            'api/logistic-service-offers/'.$logisticServiceOfferId.'/article-reservations',
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());

        $interventionId = $this->generator->uuid();
        $this->client->request(
            'GET',
            'api/logistic-service-offers/'.$logisticServiceOfferId.'/interventions/'.$interventionId.'/article-reservations',
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     *
     * @return array<mixed> un tableau de réservations filtré sur un magasin (brut depuis json)
     */
    public function un_user_avec_le_role_adequat_peut_accéder_aux_collections_de_réservations_par_magasin(): array
    {
        $logisticServiceOfferId = LogisticServiceOfferFactory::createOne(['warehouseId' => $_ENV['TEST_STOCK_WAREHOUSE_ID']])->getId();
        $userAvecRoleAdequatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ["SERVICEOFFER_{$logisticServiceOfferId}_ROLE_LOGISTIQUE_MAGASINIER"],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleAdequatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $articleReservationsResponseArray = $this->client->request(
            'GET',
            'api/logistic-service-offers/'.$logisticServiceOfferId.'/article-reservations',
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        )->toArray();

        $this->assertNotEmpty($articleReservationsResponseArray, 'Il doit y avoir des réservations pour pouvoir tester la feature');
        foreach ($articleReservationsResponseArray as $articleReservationsResponse) {
            $this->assertEquals(
                $articleReservationsResponse['warehouseCode'],
                $_ENV['TEST_STOCK_WAREHOUSE_CODE']
            );
        }

        return $articleReservationsResponseArray;
    }

    private function getNewArticleReservationRepository(): ArticleReservationRepository
    {
        // Note, utiliser faker/foundry dans des dataproviders, c'est chiant, cf.
        //
        // 6) App\Tests\Api\ArticleReservationTest::un_user_authentifié_peut_accéder_aux_collections_de_réservations_par_magasin_et_intervention
        // The data provider specified for App\Tests\Api\ArticleReservationTest::un_user_authentifié_peut_accéder_aux_collections_de_réservations_par_magasin_et_intervention is invalid
        // Cannot get Foundry's configuration. If using faker in a data provider, consider passing attributes as a callable.
        //
        // /srv/api/tests/Api/ArticleReservationTest.php:167

        $logisticServiceOfferProxy = LogisticServiceOfferFactory::createOne();
        $carlGmaoConfiguration = $logisticServiceOfferProxy->getGmaoConfiguration();
        assert($carlGmaoConfiguration instanceof CarlGmaoConfiguration);
        $carlInterventionRepository = new CarlInterventionRepository($carlGmaoConfiguration);

        return new ArticleReservationRepository(
            $carlGmaoConfiguration,
            $carlInterventionRepository
        );
    }

    /**
     * @test
     */
    public function un_user_authentifié_peut_accéder_aux_collections_de_réservations_par_magasin_et_intervention(): void
    {
        $logisticServiceOfferId = LogisticServiceOfferFactory::createOne(['warehouseId' => $_ENV['TEST_STOCK_WAREHOUSE_ID']])->getId();
        $userAvecRoleAdequatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ["SERVICEOFFER_{$logisticServiceOfferId}_ROLE_LOGISTIQUE_MAGASINIER"],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleAdequatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $interventionId = $this->getNewArticleReservationRepository()->findOneBy(['warehouseId' => $_ENV['TEST_STOCK_WAREHOUSE_ID']])->getInterventionId();
        $articleReservationsByInterventionIdResponseArray = $this->client->request(
            'GET',
            'api/logistic-service-offers/'.$logisticServiceOfferId.'/interventions/'.urlencode($interventionId).'/article-reservations',
            ['headers' => ['Accept' => 'application/json']]
        )->toArray();

        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');

        $this->assertMatchesResourceCollectionJsonSchema(ArticleReservation::class, null, 'json');

        $this->assertNotEmpty($articleReservationsByInterventionIdResponseArray);
        foreach ($articleReservationsByInterventionIdResponseArray as $articleReservation) {
            $this->assertEquals(
                $interventionId,
                $articleReservation['interventionId']
            );
            $this->assertEquals(
                $_ENV['TEST_STOCK_WAREHOUSE_CODE'],
                $articleReservation['warehouseCode']
            );
        }
    }
}
