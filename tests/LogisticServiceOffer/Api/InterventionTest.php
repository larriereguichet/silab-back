<?php

namespace App\Tests\LogisticServiceOffer\Api;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Entity\Intervention;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepository;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepositoryInterface;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Intervention\Repository\CarlInterventionRepository;
use App\Tests\LogisticServiceOffer\Factory\LogisticServiceOfferFactory;
use App\Tests\LogisticServiceOffer\Faker\Factory\SetUpStockGeneratorTrait;
use App\Tests\Shared\Api\ProtectedApiTestCase;
use App\Tests\Shared\Factory\UserFactory;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\ResponseInterface as HttpClientResponseInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class InterventionTest extends ProtectedApiTestCase
{
    use SetUpStockGeneratorTrait{
        setUp as setUpGenerator;
    }
    use ResetDatabase;
    use Factories;
    private Client $client;

    protected function setUp(): void
    {
        $this->setUpGenerator();
        $this->client = static::createClient();
    }

    /**
     * @test
     */
    public function un_user_non_authentifié_se_voit_refuser_l_accès_a_l_intervention_associée_à_une_réservation(): void
    {
        $sampleReservationDArticle = $this->une_réservation_d_articles($_ENV['TEST_STOCK_WAREHOUSE_ID'], $this->getNewCarlArticleReservationRepository());

        $logisticServiceOfferId = LogisticServiceOfferFactory::createOne(
            ['warehouseId' => $_ENV['TEST_STOCK_WAREHOUSE_ID']]
        )->getId();

        $this->client->request(
            'GET',
            "api/logistic-service-offers/$logisticServiceOfferId/interventions/".$sampleReservationDArticle->getInterventionId(),
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );

        $this->assertSame(401, $this->client->getResponse()->getStatusCode());
    }

    private function une_réservation_d_articles(string $warehouseId, ArticleReservationRepositoryInterface $articleReservationRepository): ArticleReservation
    {
        return $articleReservationRepository->findOneBy(['warehouseId' => $warehouseId]);
    }

    /**
     * @test
     */
    public function un_user_sans_le_role_adequat_se_voit_refuser_l_accès_a_l_intervention_associée_à_une_réservation(): void
    {
        $sampleReservationDArticle = $this->une_réservation_d_articles($_ENV['TEST_STOCK_WAREHOUSE_ID'], $this->getNewCarlArticleReservationRepository());

        $userSansRolesProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => [],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userSansRolesProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $logisticServiceOfferId = LogisticServiceOfferFactory::createOne(
            ['warehouseId' => $_ENV['TEST_STOCK_WAREHOUSE_ID']]
        )->getId();

        $this->client->request(
            'GET',
            "api/logistic-service-offers/$logisticServiceOfferId/interventions/".$sampleReservationDArticle->getInterventionId(),
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );

        $this->assertSame(403, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function un_user_avec_le_role_adequat_peut_accéder_a_l_intervention_associée_à_une_réservation(): void
    {
        $sampleReservationDArticle = $this->une_réservation_d_articles($_ENV['TEST_STOCK_WAREHOUSE_ID'], $this->getNewCarlArticleReservationRepository());
        $logisticServiceOffer = LogisticServiceOfferFactory::createOne(['warehouseId' => $_ENV['TEST_STOCK_WAREHOUSE_ID']]);
        $carlGmaoConfiguration = $logisticServiceOffer->getGmaoConfiguration();
        assert($carlGmaoConfiguration instanceof CarlGmaoConfiguration);

        $sampleInterventionLiée =
            (new CarlInterventionRepository(
                $carlGmaoConfiguration
            ))
            ->find($sampleReservationDArticle->getInterventionId());
        $userAvecRoleAdequatProxy = UserFactory::createOne(
            [
                'email' => $_ENV['AZURE_TEST_USERNAME'],
                'roles' => ['SERVICEOFFER_'.$logisticServiceOffer->getId().'_ROLE_LOGISTIQUE_MAGASINIER'],
            ]
        );

        $this->azureLogin(
            $this->client,
            $userAvecRoleAdequatProxy->getEmail(),
            $_ENV['AZURE_TEST_PASSWORD']
        );

        $interventionResponse = $this->client->request(
            'GET',
            'api/logistic-service-offers/'.$logisticServiceOffer->getId().'/interventions/'.$sampleReservationDArticle->getInterventionId(),
            [
                'headers' => ['Accept' => 'application/json'],
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(Intervention::class, null, 'json', ['groups' => ['Afficher détails intervention']]);
        $this->assertInterventionMatchesResponse($sampleInterventionLiée, $interventionResponse);
    }

    private function assertInterventionMatchesResponse(Intervention $intervention, HttpClientResponseInterface $interventionResponse): void
    {
        $serializedSampleIntervention = self::getContainer()->get(SerializerInterface::class)->serialize($intervention, 'json', ['skip_null_values' => true]);

        $this->assertSame($serializedSampleIntervention, $interventionResponse->getContent());
    }

    private function getNewCarlArticleReservationRepository(): ArticleReservationRepository
    {
        // Note, utiliser faker/foundry dans des dataproviders, c'est chiant, cf.
        //
        // 6) App\Tests\Api\ArticleReservationTest::un_user_authentifié_peut_accéder_aux_collections_de_réservations_par_magasin_et_intervention
        // The data provider specified for App\Tests\Api\ArticleReservationTest::un_user_authentifié_peut_accéder_aux_collections_de_réservations_par_magasin_et_intervention is invalid
        // Cannot get Foundry's configuration. If using faker in a data provider, consider passing attributes as a callable.
        //
        // /srv/api/tests/Api/ArticleReservationTest.php:167

        $logisticServiceOfferProxy = LogisticServiceOfferFactory::createOne();
        $carlGmaoConfiguration = $logisticServiceOfferProxy->getGmaoConfiguration();
        assert($carlGmaoConfiguration instanceof CarlGmaoConfiguration);
        $carlInterventionRepository = new CarlInterventionRepository($carlGmaoConfiguration);

        return new ArticleReservationRepository(
            $carlGmaoConfiguration,
            $carlInterventionRepository
        );
    }
}
