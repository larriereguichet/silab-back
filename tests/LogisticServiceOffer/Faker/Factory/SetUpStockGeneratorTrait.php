<?php

namespace App\Tests\LogisticServiceOffer\Faker\Factory;

use App\Tests\Shared\Providers\ArticleProvider;
use App\Tests\Shared\Providers\DirectionProvider;
use App\Tests\Shared\Providers\InterventionProvider;
use Faker\Generator;

trait SetUpStockGeneratorTrait
{
    protected Generator
    |DirectionProvider
    |InterventionProvider
    |ArticleProvider $generator;

    protected function setUp(): void
    {
        $this->generator = StockGeneratorFactory::create();
    }
}
