<?php

namespace App\Tests\LogisticServiceOffer\Faker\Factory;

use App\Tests\Shared\Providers\ArticleProvider;
use App\Tests\Shared\Providers\DirectionProvider;
use App\Tests\Shared\Providers\InterventionProvider;
use Faker\Factory;
use Faker\Generator;
use Faker\Provider\fr_FR\Company;
use Faker\Provider\fr_FR\Person;
use Faker\Provider\fr_FR\Text;
use Faker\Provider\Internet;
use Faker\Provider\Lorem;

class StockGeneratorFactory
{
    public static function create(string $locale = Factory::DEFAULT_LOCALE): Generator
    {
        $generator = Factory::create($locale);
        $generator->addProvider(new Internet($generator));
        $generator->addProvider(new Lorem($generator));
        $generator->addProvider(new Text($generator));
        $generator->addProvider(new Person($generator));
        $generator->addProvider(new Company($generator));
        $generator->addProvider(new DirectionProvider($generator));
        $generator->addProvider(new InterventionProvider($generator));
        $generator->addProvider(new ArticleProvider($generator));

        return $generator;
    }
}
