ARG PHP_VERSION=8.1

########### BASE ###########
FROM php:${PHP_VERSION}-fpm-alpine AS base
# persistent / runtime deps
RUN apk add --no-cache \
        acl \
        fcgi \
        file \
        gettext
# install required php extension
ARG APCU_VERSION=5.1.21
RUN set -eux ;\
    apk add --no-cache --virtual .build-deps $PHPIZE_DEPS icu-dev libzip-dev zlib-dev; \
    docker-php-ext-configure zip;\
    docker-php-ext-install -j$(nproc) intl zip ;\
    pecl install apcu-${APCU_VERSION} ;\
    pecl clear-cache; \
    docker-php-ext-enable opcache apcu intl;\
    runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --no-cache --virtual .api-phpexts-rundeps $runDeps ; \
	apk del .build-deps

# set working directory
USER 82
WORKDIR /srv/api
USER 0
# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# install symfonyCli
RUN set -eux;\
    apk add --no-cache --virtual .install_deps bash ;\
    curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash ;\
    apk add symfony-cli ;\
    apk del .install_deps
# setup php-fpm
COPY docker/php/php-fpm.d/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf

# setting healthcheck
COPY docker/php/docker-healthcheck.sh /usr/local/bin/docker-healthcheck
RUN chmod +x /usr/local/bin/docker-healthcheck
HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD ["docker-healthcheck"]

###> recipes ###
###> doctrine/doctrine-bundle ###
RUN apk add --no-cache --virtual .pgsql-deps postgresql-dev; \
	docker-php-ext-install -j$(nproc) pdo_pgsql; \
	apk add --no-cache --virtual .pgsql-rundeps so:libpq.so.5; \
	apk del .pgsql-deps
###< doctrine/doctrine-bundle ###
###< recipes ###

# install instantClient
WORKDIR /tmp
RUN wget -qO instantclient-basiclite.zip https://download.oracle.com/otn_software/linux/instantclient/2111000/instantclient-basiclite-linux.x64-21.11.0.0.0dbru.zip && unzip -q instantclient-basiclite.zip; \
    wget -qO instantclient-sdk.zip https://download.oracle.com/otn_software/linux/instantclient/2111000/instantclient-sdk-linux.x64-21.11.0.0.0dbru.zip && unzip -q instantclient-sdk.zip; \
    mv instantclient*/ /usr/lib/instantclient; \
    rm -rf instantclient-*.zip; \
    ln -s /lib/libc.so.6 /usr/lib/libresolv.so.2
ENV LD_LIBRARY_PATH /usr/lib/instantclient
ENV NLS_LANG=AMERICAN_AMERICA.UTF8
# Configure and install OCI8
RUN apk add --no-cache --virtual .oci8-deps libaio libc6-compat gcompat; \
    docker-php-ext-configure oci8 --with-oci8=instantclient,/usr/lib/instantclient; \
    docker-php-ext-install -j$(nproc) oci8; \
    docker-php-ext-configure pdo_oci --with-pdo_oci=instantclient,/usr/lib/instantclient; \
    docker-php-ext-install -j$(nproc) pdo_oci; \
	apk add --no-cache --virtual .oci8-rundeps libaio gcompat; \
    apk del .oci8-deps

########### DEVELOPMENT ###########
FROM base AS development
ARG XDEBUG_VERSION=3.1.3
VOLUME /srv/api
WORKDIR /srv/api

ENV UTILS \
    git \
    nano \
    openssl \
    openssh \
    tree  \
    util-linux \
    iputils \
    grep \
    iproute2 \
    mlocate \
    bash \
    bash-completion \
    bind-tools
ENV SONARLINT_DEP \
    openjdk17-jre-headless

# install aditionnal package for development
RUN apk add --no-cache $UTILS $SONARLINT_DEP
# installing xdebug
RUN set -eux; \
    apk add --no-cache --virtual .build-deps $PHPIZE_DEPS; \
    pecl install xdebug-$XDEBUG_VERSION; \
    docker-php-ext-enable xdebug; \
    apk del .build-deps
# copying conf
COPY docker/php/conf.d/api-platform.dev.ini $PHP_INI_DIR/conf.d/api-platform.ini
COPY docker/devcontainer/* /root/

RUN symfony server:ca:install

CMD ["symfony", "serve", "--port=5001"]

########### BUILD ###########
FROM base AS build
COPY docker/php/conf.d/api-platform.prod.ini $PHP_INI_DIR/conf.d/api-platform.ini
# prevent the reinstallation of vendors at every changes in the source code
USER 82
WORKDIR /srv/api
COPY composer.json composer.lock symfony.lock ./
RUN set -eux; \
	composer install --prefer-dist --no-dev --no-scripts --no-progress; \
    composer clear-cache

COPY --chown=82:82  . .

RUN set -eux; \
    ls -la; \
    mkdir -p var/cache var/log; \
	composer dump-autoload --classmap-authoritative --no-dev; \
	composer dump-env prod; \
	composer run-script --no-dev post-install-cmd; \
	chmod +x bin/console; sync
USER 0

########### WEBSERVER ###########
FROM abdennour/nginx-distroless-unprivileged AS webserver

WORKDIR /srv/api

COPY --chown=1001:0 docker/webserver/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build --chown=1001:0 /srv/api/public /srv/api/public

USER 1001

########### PRODUCTION ###########
FROM build AS symfony-php

VOLUME /srv/api/var/log

COPY docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint
ENTRYPOINT [ "docker-entrypoint" ]
CMD ["php-fpm"]
