<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230512124936 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout du connecteur ged Alfresco';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE ged_client_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE alfresco_ged_client (id INT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, repository_url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ged_client (id INT NOT NULL, title VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE alfresco_ged_client ADD CONSTRAINT FK_26385CDBBF396750 FOREIGN KEY (id) REFERENCES ged_client (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention_service_offer ADD ged_client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention_service_offer ADD CONSTRAINT FK_9EA0E6159BF5C46C FOREIGN KEY (ged_client_id) REFERENCES ged_client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9EA0E6159BF5C46C ON intervention_service_offer (ged_client_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE intervention_service_offer DROP CONSTRAINT FK_9EA0E6159BF5C46C');
        $this->addSql('DROP SEQUENCE ged_client_id_seq CASCADE');
        $this->addSql('ALTER TABLE alfresco_ged_client DROP CONSTRAINT FK_26385CDBBF396750');
        $this->addSql('DROP TABLE alfresco_ged_client');
        $this->addSql('DROP TABLE ged_client');
        $this->addSql('DROP INDEX IDX_9EA0E6159BF5C46C');
        $this->addSql('ALTER TABLE intervention_service_offer DROP ged_client_id');
    }
}
