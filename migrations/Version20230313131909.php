<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230313131909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Met à jour les lien des ods en accord avec le routeur front';
    }

    public function up(Schema $schema): void
    {
        $serviceOffers = $this->connection->fetchAllAssociative('SELECT id, link, template FROM service_offer');
        foreach ($serviceOffers as $serviceOffer) {
            $serviceOfferIsLogisticOrIntervention = in_array(
                $serviceOffer['template'],
                ['logistic', 'intervention']
            );
            if ($serviceOfferIsLogisticOrIntervention) {
                $prefix = match ($serviceOffer['template']) {
                    'logistic' => 'stock',
                    'intervention' => 'interventions',
                };

                $this->addSql(
                    'UPDATE service_offer SET link = :link WHERE id = :id',
                    [
                        'id' => $serviceOffer['id'],
                        'link' => "/app/$prefix/".$serviceOffer['id'],
                    ]);
            }
        }
    }

    public function down(Schema $schema): void
    {
        // retour en arrière impossible
    }
}
