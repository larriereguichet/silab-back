<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221115130607 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE service_offer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE link_service_offer (id INT NOT NULL, link VARCHAR(255) NOT NULL, is_target_blank BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE service_offer (id INT NOT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE link_service_offer ADD CONSTRAINT FK_F53C66C5BF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE service_offer_id_seq CASCADE');
        $this->addSql('ALTER TABLE link_service_offer DROP CONSTRAINT FK_F53C66C5BF396750');
        $this->addSql('DROP TABLE link_service_offer');
        $this->addSql('DROP TABLE service_offer');
    }
}
