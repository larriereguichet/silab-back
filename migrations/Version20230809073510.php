<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230809073510 extends AbstractMigration
{
    private const ALFRESCO_REPOSITORY_SUFFIX = '/alfresco/api/-default-/public/cmis/versions/1.1/browser/';

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // On doit corriger les url alfresco pour ne retenir que l'url de l'intance
        $alfrescoGedClients = $this->connection->fetchAllAssociative('SELECT id, repository_url FROM alfresco_ged_client');
        foreach ($alfrescoGedClients as $alfrescoGedClient) {
            $alfrescoGedClientInstanceUrl = str_replace(
                self::ALFRESCO_REPOSITORY_SUFFIX,
                '',
                $alfrescoGedClient['repository_url']
            );

            $this->addSql(
                'UPDATE alfresco_ged_client SET repository_url = :repository_url WHERE id = :id',
                [
                    'repository_url' => $alfrescoGedClientInstanceUrl,
                    'id' => $alfrescoGedClient['id'],
                ]
            );
        }

        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE alfresco_ged_client RENAME COLUMN repository_url TO instance_url');
    }

    public function down(Schema $schema): void
    {
        $alfrescoGedClients = $this->connection->fetchAllAssociative('SELECT id, instance_url FROM alfresco_ged_client');
        foreach ($alfrescoGedClients as $alfrescoGedClient) {
            $alfrescoGedClientRepositoryUrl = $alfrescoGedClient['instance_url'].self::ALFRESCO_REPOSITORY_SUFFIX;

            $this->addSql(
                'UPDATE alfresco_ged_client SET instance_url = :instance_url WHERE id = :id',
                [
                    'instance_url' => $alfrescoGedClientRepositoryUrl,
                    'id' => $alfrescoGedClient['id'],
                ]
            );
        }

        $this->addSql('ALTER TABLE alfresco_ged_client RENAME COLUMN instance_url TO repository_url');
    }
}
