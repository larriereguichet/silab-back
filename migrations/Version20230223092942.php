<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230223092942 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE intervention_service_offer (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE logistic_service_offer (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE redirect_service_offer (id INT NOT NULL, redirect_url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE intervention_service_offer ADD CONSTRAINT FK_9EA0E615BF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logistic_service_offer ADD CONSTRAINT FK_26CD5CE9BF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE redirect_service_offer ADD CONSTRAINT FK_7E62E14FBF396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logistic DROP CONSTRAINT fk_54bd53bdbf396750');
        $this->addSql('ALTER TABLE intervention DROP CONSTRAINT fk_d11814abbf396750');
        $this->addSql('ALTER TABLE redirect DROP CONSTRAINT fk_c30c9e2bbf396750');
        $this->addSql('DROP TABLE logistic');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('DROP TABLE redirect');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE logistic (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE intervention (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE redirect (id INT NOT NULL, redirect_url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE logistic ADD CONSTRAINT fk_54bd53bdbf396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT fk_d11814abbf396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE redirect ADD CONSTRAINT fk_c30c9e2bbf396750 FOREIGN KEY (id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention_service_offer DROP CONSTRAINT FK_9EA0E615BF396750');
        $this->addSql('ALTER TABLE logistic_service_offer DROP CONSTRAINT FK_26CD5CE9BF396750');
        $this->addSql('ALTER TABLE redirect_service_offer DROP CONSTRAINT FK_7E62E14FBF396750');
        $this->addSql('DROP TABLE intervention_service_offer');
        $this->addSql('DROP TABLE logistic_service_offer');
        $this->addSql('DROP TABLE redirect_service_offer');
    }
}
