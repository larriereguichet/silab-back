<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230803092522 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Rend l\'association d\'une configuration obligatoire pour les ods de type intervention. ATTENTION: peut planter si des ods n\'ont pas de config gmao';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intervention_service_offer ALTER gmao_configuration_id SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE intervention_service_offer ALTER gmao_configuration_id DROP NOT NULL');
    }
}
