<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230224165742 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_service_offer (user_id INT NOT NULL, service_offer_id INT NOT NULL, PRIMARY KEY(user_id, service_offer_id))');
        $this->addSql('CREATE INDEX IDX_6E2AC789A76ED395 ON user_service_offer (user_id)');
        $this->addSql('CREATE INDEX IDX_6E2AC789AAB02FD3 ON user_service_offer (service_offer_id)');
        $this->addSql('ALTER TABLE user_service_offer ADD CONSTRAINT FK_6E2AC789A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_service_offer ADD CONSTRAINT FK_6E2AC789AAB02FD3 FOREIGN KEY (service_offer_id) REFERENCES service_offer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_service_offer DROP CONSTRAINT FK_6E2AC789A76ED395');
        $this->addSql('ALTER TABLE user_service_offer DROP CONSTRAINT FK_6E2AC789AAB02FD3');
        $this->addSql('DROP TABLE user_service_offer');
    }
}
