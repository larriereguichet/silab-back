<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230525080959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE carl_configuration ADD login VARCHAR(255) DEFAULT \'à changer\' NOT NULL');
        $this->addSql('ALTER TABLE carl_configuration ADD password VARCHAR(255) DEFAULT \'à changer\' NOT NULL');
        $this->addSql('ALTER TABLE carl_configuration ADD base_url VARCHAR(255) DEFAULT \'à changer\' NOT NULL');
        $this->addSql('ALTER TABLE carl_configuration ADD page_limit INT DEFAULT 100 NOT NULL');
        $this->addSql('ALTER TABLE carl_configuration ADD priority_mapping JSON DEFAULT \'[]\' NOT NULL');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP CONSTRAINT fk_cb236d31f7c55b56');
        $this->addSql('DROP INDEX idx_cb236d31f7c55b56');
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN carl_configuration_id TO carl_client_id');
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD CONSTRAINT FK_CB236D31D7206B30 FOREIGN KEY (carl_client_id) REFERENCES carl_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_CB236D31D7206B30 ON carl_configuration_intervention (carl_client_id)');
        $this->addSql('ALTER TABLE logistic_service_offer ADD carl_client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE logistic_service_offer ADD CONSTRAINT FK_26CD5CE9D7206B30 FOREIGN KEY (carl_client_id) REFERENCES carl_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_26CD5CE9D7206B30 ON logistic_service_offer (carl_client_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE logistic_service_offer DROP CONSTRAINT FK_26CD5CE9D7206B30');
        $this->addSql('DROP INDEX IDX_26CD5CE9D7206B30');
        $this->addSql('ALTER TABLE logistic_service_offer DROP carl_client_id');
        $this->addSql('ALTER TABLE carl_configuration_intervention DROP CONSTRAINT FK_CB236D31D7206B30');
        $this->addSql('DROP INDEX IDX_CB236D31D7206B30');
        $this->addSql('ALTER TABLE carl_configuration_intervention RENAME COLUMN carl_client_id TO carl_configuration_id');
        $this->addSql('ALTER TABLE carl_configuration_intervention ADD CONSTRAINT fk_cb236d31f7c55b56 FOREIGN KEY (carl_configuration_id) REFERENCES carl_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_cb236d31f7c55b56 ON carl_configuration_intervention (carl_configuration_id)');
        $this->addSql('ALTER TABLE carl_configuration DROP login');
        $this->addSql('ALTER TABLE carl_configuration DROP password');
        $this->addSql('ALTER TABLE carl_configuration DROP base_url');
        $this->addSql('ALTER TABLE carl_configuration DROP page_limit');
        $this->addSql('ALTER TABLE carl_configuration DROP priority_mapping');
    }
}
