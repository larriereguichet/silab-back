<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231019133433 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migre la configuration carl vers l\'entité approriée';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE logisticserviceoffer_gmao_configuration_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE logisticserviceoffer_carl_gmao_configuration (id INT NOT NULL, carl_client_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_47A8F5B3D7206B30 ON logisticserviceoffer_carl_gmao_configuration (carl_client_id)');
        $this->addSql('CREATE TABLE logisticserviceoffer_gmao_configuration (id INT NOT NULL, title VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE logisticserviceoffer_carl_gmao_configuration ADD CONSTRAINT FK_47A8F5B3D7206B30 FOREIGN KEY (carl_client_id) REFERENCES carl_client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logisticserviceoffer_carl_gmao_configuration ADD CONSTRAINT FK_47A8F5B3BF396750 FOREIGN KEY (id) REFERENCES logisticserviceoffer_gmao_configuration (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logistic_service_offer ADD gmao_configuration_id INT');
        $this->addSql('ALTER TABLE logistic_service_offer ADD CONSTRAINT FK_26CD5CE96C9802C7 FOREIGN KEY (gmao_configuration_id) REFERENCES logisticserviceoffer_gmao_configuration (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_26CD5CE96C9802C7 ON logistic_service_offer (gmao_configuration_id)');
        $logisticServiceOffers = $this->connection->fetchAllAssociative('SELECT id, (select title from service_offer where service_offer.id = logistic_service_offer.id) as title, carl_client_id FROM logistic_service_offer');
        foreach ($logisticServiceOffers as $logisticServiceOffer) {
            $carlClientTitle = $this->connection->fetchOne('SELECT title FROM carl_client WHERE id = '.$logisticServiceOffer['carl_client_id']);
            $gmaoConfigurationTitle = $logisticServiceOffer['title'].' - '.$carlClientTitle;
            $this->addSql("INSERT INTO logisticserviceoffer_gmao_configuration VALUES (nextval('logisticserviceoffer_gmao_configuration_id_seq'), '$gmaoConfigurationTitle', 'carl')");
            $this->addSql("INSERT INTO logisticserviceoffer_carl_gmao_configuration VALUES (currval('logisticserviceoffer_gmao_configuration_id_seq'),".$logisticServiceOffer['carl_client_id'].')');
            $this->addSql("UPDATE logistic_service_offer SET gmao_configuration_id = currval('logisticserviceoffer_gmao_configuration_id_seq') where id = ".$logisticServiceOffer['id']);
        }

        $this->addSql('ALTER TABLE logistic_service_offer ALTER gmao_configuration_id SET NOT NULL');

        $this->addSql('ALTER TABLE logistic_service_offer DROP CONSTRAINT fk_26cd5ce9d7206b30');
        $this->addSql('DROP INDEX idx_26cd5ce9d7206b30');
        $this->addSql('ALTER TABLE logistic_service_offer DROP carl_client_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE logistic_service_offer ADD carl_client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE logistic_service_offer ADD CONSTRAINT fk_26cd5ce9d7206b30 FOREIGN KEY (carl_client_id) REFERENCES carl_client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_26cd5ce9d7206b30 ON logistic_service_offer (carl_client_id)');

        // on remet les id qui vont bien
        $this->addSql('UPDATE logistic_service_offer SET carl_client_id = (select carl_client_id from logisticserviceoffer_carl_gmao_configuration where logistic_service_offer.gmao_configuration_id = logisticserviceoffer_carl_gmao_configuration.id)');

        $this->addSql('ALTER TABLE logistic_service_offer DROP CONSTRAINT FK_26CD5CE96C9802C7');
        $this->addSql('DROP SEQUENCE logisticserviceoffer_gmao_configuration_id_seq CASCADE');
        $this->addSql('ALTER TABLE logisticserviceoffer_carl_gmao_configuration DROP CONSTRAINT FK_47A8F5B3D7206B30');
        $this->addSql('ALTER TABLE logisticserviceoffer_carl_gmao_configuration DROP CONSTRAINT FK_47A8F5B3BF396750');
        $this->addSql('DROP TABLE logisticserviceoffer_carl_gmao_configuration');
        $this->addSql('DROP TABLE logisticserviceoffer_gmao_configuration');
        $this->addSql('DROP INDEX IDX_26CD5CE96C9802C7');
        $this->addSql('ALTER TABLE logistic_service_offer DROP gmao_configuration_id');
    }
}
