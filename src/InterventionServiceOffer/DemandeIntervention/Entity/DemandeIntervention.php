<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Filter\SearchFilter;
use App\InterventionServiceOffer\DemandeIntervention\State\CreateDemandeInterventionDataHandler;
use App\InterventionServiceOffer\DemandeIntervention\State\GetCollectionDemandeInterventionDataHandler;
use App\InterventionServiceOffer\DemandeIntervention\State\GetDemandeInterventionDataHandler;
use App\InterventionServiceOffer\DemandeIntervention\State\UpdateDemandeInterventionDataHandler;
use App\InterventionServiceOffer\Equipement\Entity\Equipement;
use App\Shared\ObjectWithImage\Entity\ObjectWithImageInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[Post(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_DEMANDER_INTERVENTIONS')",
    processor: CreateDemandeInterventionDataHandler::class,
    normalizationContext: ['groups' => [DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION]],
    denormalizationContext: ['groups' => [DemandeIntervention::GROUP_CREER_DEMANDE_INTERVENTION]],
)]
#[Put(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_MODIFIER_DEMANDES_INTERVENTIONS')",
    processor: UpdateDemandeInterventionDataHandler::class,
    normalizationContext: ['groups' => [DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION]],
    denormalizationContext: ['groups' => [DemandeIntervention::GROUP_MODIFIER_DEMANDE_INTERVENTION]],
)]
#[Get()]
#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS')",
    provider: GetDemandeInterventionDataHandler::class,
    normalizationContext: ['groups' => [DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION]]
)]
#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/demandes-interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS')",
    provider: GetCollectionDemandeInterventionDataHandler::class,
    normalizationContext: ['groups' => [DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS]],
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'statuses' => 'exact',
        ]
)]
class DemandeIntervention implements ObjectWithImageInterface
{
    public const GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION = 'Afficher détails demande d\'intervention';
    public const GROUP_LISTER_DEMANDES_INTERVENTIONS = 'Lister les demandes d\'interventions';
    public const GROUP_CREER_DEMANDE_INTERVENTION = 'Créer demande d\'intervention';
    public const GROUP_MODIFIER_DEMANDE_INTERVENTION = 'Modifier demande d\'intervention';

    #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?string $id = null;

    #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?string $code;

    #[Groups([DemandeIntervention::GROUP_CREER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_MODIFIER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private string $title;

    #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private DemandeInterventionStatus $currentStatus;

    #[Groups([DemandeIntervention::GROUP_CREER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_MODIFIER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?string $description = null;

    #[Groups([DemandeIntervention::GROUP_CREER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_MODIFIER_DEMANDE_INTERVENTION])]
    private ?string $imageB64 = null;

    #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?string $imageUrl = null;

    #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?string $createdBy;

    #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?\DateTime $createdAt;

    #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?\DateTime $updatedAt;

    #[Groups([DemandeIntervention::GROUP_CREER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_MODIFIER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
    private ?Equipement $relatedEquipement = null; // QUESTION_TECHNIQUE: Si Equipement est une Api_ressource lors du put api platform recupere l'IRI eqpt/id de la route fantoche #[get()] et lance une rerreur car il n'y a pas de provier. Une solution parmis d'autre, mettre un iri qui existe c-a-d prefixé par l'ODS.

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCurrentStatus(): DemandeInterventionStatus
    {
        return $this->currentStatus;
    }

    public function setCurrentStatus(DemandeInterventionStatus $currentStatus): self
    {
        $this->currentStatus = $currentStatus;

        return $this;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setImageB64(?string $imageB64): self
    {
        $this->imageB64 = $imageB64;

        return $this;
    }

    public function getImageB64(): ?string
    {
        return $this->imageB64;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setCreatedBy(string $userEmail): self
    {
        $this->createdBy = $userEmail;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setRelatedEquipement(?Equipement $relatedEquipement): self
    {
        $this->relatedEquipement = $relatedEquipement;

        return $this;
    }

    public function getRelatedEquipement(): ?Equipement
    {
        return $this->relatedEquipement;
    }
}
