<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity;

use ApiPlatform\Metadata\Post;
use App\InterventionServiceOffer\DemandeIntervention\State\AddStatusToDemandeInterventionDataHandler;
use App\Shared\ObjectStatus\Entity\ObjectStatusInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[Post(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/demandes-interventions/{demandeInterventionId}/statuses',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'demandeInterventionId' => 'demandeInterventionId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_SOUMETTRE_LA_DEMANDE_APRES_ATTENTE_INFORMATION')",
    denormalizationContext: ['groups' => [ObjectStatusInterface::GROUP_AJOUTER_NOUVEAU_STATUT]],
    read: false,
    processor: AddStatusToDemandeInterventionDataHandler::class,
)]
class DemandeInterventionStatus implements ObjectStatusInterface
{
    public function __construct(
        #[Groups([ObjectStatusInterface::GROUP_AJOUTER_NOUVEAU_STATUT, ObjectStatusInterface::GROUP_AFFICHER_STATUT, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private string $code,
        #[Groups([ObjectStatusInterface::GROUP_AFFICHER_STATUT, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private ?string $label = null,
        #[Groups([ObjectStatusInterface::GROUP_AFFICHER_STATUT, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private ?string $createdBy = null,
        #[Groups([ObjectStatusInterface::GROUP_AFFICHER_STATUT, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private ?\DateTimeInterface $createdAt = null
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
