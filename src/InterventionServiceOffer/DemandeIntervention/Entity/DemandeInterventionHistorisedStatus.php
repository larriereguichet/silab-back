<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Entity;

use ApiPlatform\Metadata\GetCollection;
use App\InterventionServiceOffer\DemandeIntervention\State\GetDemandeInterventionStatusHistoryDataHandler;
use App\Shared\ObjectStatus\Entity\ObjectHistorisedStatusInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/demandes-interventions/{demandeInterventionId}/statusHistory',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'demandeInterventionId' => 'demandeInterventionId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CONSULTER_DEMANDES_INTERVENTIONS')",
    provider: GetDemandeInterventionStatusHistoryDataHandler::class,
    normalizationContext: ['groups' => [ObjectHistorisedStatusInterface::GROUP_AFFICHER_HISTORIQUE_STATUTS]],
)]
class DemandeInterventionHistorisedStatus extends DemandeInterventionStatus implements ObjectHistorisedStatusInterface
{
    public function __construct(
        #[Groups([ObjectHistorisedStatusInterface::GROUP_AFFICHER_HISTORIQUE_STATUTS])]
        private string $code,
        #[Groups([ObjectHistorisedStatusInterface::GROUP_AFFICHER_HISTORIQUE_STATUTS])]
        private ?string $label = null,
        #[Groups([ObjectHistorisedStatusInterface::GROUP_AFFICHER_HISTORIQUE_STATUTS])]
        private ?string $createdBy = null,
        #[Groups([ObjectHistorisedStatusInterface::GROUP_AFFICHER_HISTORIQUE_STATUTS])]
        private ?\DateTimeInterface $createdAt = null,
        #[Groups([ObjectHistorisedStatusInterface::GROUP_AFFICHER_HISTORIQUE_STATUTS])]
        private ?string $comment = null
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getComment(): string
    {
        return $this->comment;
    }
}
