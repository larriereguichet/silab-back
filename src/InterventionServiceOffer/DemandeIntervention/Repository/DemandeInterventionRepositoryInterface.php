<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Repository;

use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeInterventionHistorisedStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeInterventionStatus;
use Doctrine\Persistence\ObjectRepository;

interface DemandeInterventionRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): DemandeIntervention;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<DemandeIntervention>
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    /**
     * @return array<DemandeIntervention>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): DemandeIntervention;

    public function update(string $id, DemandeIntervention $demandeIntervention, string $userEmail): DemandeIntervention;

    public function delete(string $id): void;

    public function save(DemandeIntervention $demandeIntervention): DemandeIntervention;

    public function transistionToStatus(string $id, DemandeInterventionStatus $status): DemandeInterventionStatus;

    /**
     * @return array<DemandeInterventionHistorisedStatus>
     */
    public function getStatusHistory(string $id): array;
}
