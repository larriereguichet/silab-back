<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Repository;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeInterventionStatus;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;

class DemandeInterventionRepository implements DemandeInterventionRepositoryInterface
{
    private DemandeInterventionRepositoryInterface $specificDemandeInterventionRepository;

    public function __construct(
        protected InterventionServiceOffer $interventionServiceOffer
    ) {
        try {
            $this->specificDemandeInterventionRepository = match ($interventionServiceOffer->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => new CarlDemandeInterventionRepository(
                    $interventionServiceOffer
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOffer->getGmaoConfiguration()::class);
        }
    }

    public function delete(string $id): void
    {
        $this->specificDemandeInterventionRepository->delete($id);
    }

    public function find($id): DemandeIntervention
    {
        $sampleInterventionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return array_pop($sampleInterventionArray);
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        return $this->specificDemandeInterventionRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): DemandeIntervention
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return DemandeIntervention::class;
    }

    public function update(string $id, DemandeIntervention $demandeIntervention, string $userEmail): DemandeIntervention
    {
        $hasImageB64 = !is_null($demandeIntervention->getImageB64());

        if ($hasImageB64 && $this->interventionServiceOffer->utiliseUneGED()) {
            $originalDemandeIntervention = $this->specificDemandeInterventionRepository->find($id);
            $gedClient = $this->interventionServiceOffer->getGedClient();

            $originalDemandeInterventionHasImage = !empty($originalDemandeIntervention->getImageUrl());

            if ($originalDemandeInterventionHasImage) {
                $originalInterventionImageIsDifferentThanTheNewOne = base64_encode($gedClient->getFile($originalDemandeIntervention->getImageUrl())) !== $demandeIntervention->getImageB64();
                if ($originalInterventionImageIsDifferentThanTheNewOne) {
                    $gedClient->updateFile(
                        $this->find($id)->getImageUrl(),
                        $demandeIntervention->getImageB64(),
                        $demandeIntervention->getTitle()
                    );
                }
                $demandeIntervention->setImageUrl($originalDemandeIntervention->getImageUrl()); // note A terme il faudrait récuperer le nouvel URL; actuellement il est identique à l'original
            } else {
                $gedConfiguration = $this->interventionServiceOffer->getGedClientConfiguration();
                if (is_null($gedConfiguration)) {
                    throw new RuntimeException("La configuration du client ged est absente ou corrompue pour l'offre de service ".$this->interventionServiceOffer->getTitle().'.');
                }
                $gedDocumentData = array_map(
                    function ($variable) use ($originalDemandeIntervention) {
                        return $variable($originalDemandeIntervention, $this->interventionServiceOffer);
                    },
                    self::getGedClientConfigurationVariable()
                );

                $imageUrl = $gedClient->persistFile(
                    $demandeIntervention->getImageB64(),
                    $originalDemandeIntervention->getTitle(),
                    $gedConfiguration,
                    $gedDocumentData
                );
                $demandeIntervention->setImageUrl($imageUrl);
            }

            $demandeIntervention->setImageB64(null);
        }

        return $this->specificDemandeInterventionRepository->update($id, $demandeIntervention, $userEmail);
    }

    public function save(DemandeIntervention $demandeIntervention): DemandeIntervention
    {
        $hasImageB64 = !is_null($demandeIntervention->getImageB64());
        if ($hasImageB64 && $this->interventionServiceOffer->utiliseUneGED()) {
            $gedConfiguration = $this->interventionServiceOffer->getGedClientConfiguration();
            if (is_null($gedConfiguration)) {
                throw new RuntimeException("La configuration du client ged est absente ou corrompue pour l'offre de service ".$this->interventionServiceOffer->getTitle().'.');
            }
            $gedDocumentData = array_map(
                function ($gedDocumentVariable) use ($demandeIntervention) {
                    return $gedDocumentVariable($demandeIntervention, $this->interventionServiceOffer);
                },
                self::getGedClientConfigurationVariable()
            );

            $imageUrl = $this->interventionServiceOffer->getGedClient()->persistFile(
                $demandeIntervention->getImageB64(),
                $demandeIntervention->getTitle(),
                $gedConfiguration,
                $gedDocumentData
            );
            $demandeIntervention->setImageUrl($imageUrl);
            $demandeIntervention->setImageB64(null);
        }

        return $this->specificDemandeInterventionRepository->save($demandeIntervention);
    }

    /**
     * @return array<string,mixed>
     */
    public static function getGedClientConfigurationVariable(): array
    {
        return [
                'Object.decription' => function (DemandeIntervention $demandeIntervention, InterventionServiceOffer $interventionServiceOffer): ?string {return $demandeIntervention->getDescription(); },
                'Object.title' => function (DemandeIntervention $demandeIntervention, InterventionServiceOffer $interventionServiceOffer): string {return $demandeIntervention->getTitle(); },
                'ServiceOffer.title' => function (DemandeIntervention $demandeIntervention, InterventionServiceOffer $interventionServiceOffer): string {return $interventionServiceOffer->getTitle(); },
        ];
    }

    public function transistionToStatus(string $id, DemandeInterventionStatus $status): DemandeInterventionStatus
    {
        return $this->specificDemandeInterventionRepository->transistionToStatus($id, $status);
    }

    public function getStatusHistory(string $id): array
    {
        return $this->specificDemandeInterventionRepository->getStatusHistory($id);
    }
}
