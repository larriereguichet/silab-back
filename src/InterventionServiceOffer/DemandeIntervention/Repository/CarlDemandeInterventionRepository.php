<?php

namespace App\InterventionServiceOffer\DemandeIntervention\Repository;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeInterventionHistorisedStatus;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeInterventionStatus;
use App\InterventionServiceOffer\Equipement\Entity\Equipement;
use App\InterventionServiceOffer\Equipement\Repository\CarlEquipementRepository;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Service\JsonApiResource;
use App\Service\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\ValueObject\CarlEqptIdAndType;
use App\Shared\Carl\ValueObject\CarlObject;
use App\Shared\Helpers;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlDemandeInterventionRepository implements DemandeInterventionRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    private CarlConfigurationIntervention $carlConfigurationIntervention;
    private CarlClient $carlClient;
    private CarlEquipementRepository $carlEquipementRepository;

    public function __construct(
        protected InterventionServiceOffer $interventionServiceOffer
    ) {
        // Il serait préférable de récupérer directemetn la config gmao
        $carlConfigurationIntervention = $interventionServiceOffer->getGmaoConfiguration();
        if (is_null($carlConfigurationIntervention)) {
            throw new RuntimeException('Veuillez attacher une configuration GMAO à cette offre de service');
        }

        assert($carlConfigurationIntervention instanceof CarlConfigurationIntervention);
        $this->carlConfigurationIntervention = $carlConfigurationIntervention;
        $this->carlClient = $this->carlConfigurationIntervention->getCarlClient();
        if (is_null($this->carlClient)) {
            throw new RuntimeException('Veuillez configurer un client Carl pour cette offre de service');
        }

        $this->carlEquipementRepository = new CarlEquipementRepository($this->interventionServiceOffer);
    }

    public function delete(string $id): void
    {
        $this->carlClient->deleteObject('mr', $id);
    }

    public function find($id): DemandeIntervention
    {
        $sampleDemandeInterventionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleDemandeInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleDemandeInterventionArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return array_pop($sampleDemandeInterventionArray);
    }

    /**
     * @return array<DemandeIntervention>
     */
    public function findAll(): array
    {
        throw new RuntimeException('methode non implémenté');
    }

    /**
     * @return array<DemandeIntervention>
     */
    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        if (null !== $orderBy) {
            throw new RuntimeException('Le paramètre orderBy n\'est pas encore implémenté');
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'relatedEquipement.id',
                'statuses',
            ]
        );

        // si l'on doit filtrer par équipement, la requête carl est très différente,
        // on traite alors ça dans une autre méthode plus spécifique
        if (array_key_exists('relatedEquipement.id', $criteria)) {
            return $this->findByRelatedEquipementIds(
                $criteria['relatedEquipement.id'],
                Helpers::arrayWithoutKeys($criteria, ['relatedEquipement.id'])
            );
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $filters = [];

        $id = $propertyAccessor->getValue($criteria, '[id]');
        if (!empty($id)) {
            $filters['id'] = $id;
        }

        $statuses = $propertyAccessor->getValue($criteria, '[statuses]');
        if (!empty($statuses)) {
            $filters['statusCode'] = $this->getCarlStatusCodesFromSilabStatusCodes($statuses);
        }

        $includes = [
            'createdBy',
            'longDesc',
            'MREqpt',
            'MREqpt.eqpt',
            'statusChangedBy',
        ];

        $fields = [
           'mr' => [
               'code',
               'description',
               'creationDate',
               'modifyDate',
               'longDesc',
               'statusCode',
               'statusChangedDate',
               'statusChangedBy',
               ],
            'actor' => [
                'id',
            ],
            'description' => [
                'rawDescription',
            ],
        ];

        $orderBy = [
            '-modifyDate',
        ];

        $demandeInterventionResources = $this->carlClient->getCarlObjectsRaw(
            'mr',
            $filters,
            $includes,
            $fields,
            $orderBy,
        );
        /** @var array<DemandeIntervention> $demandeInterventions */
        $demandeInterventions = [];
        foreach ($demandeInterventionResources['data'] as $demandeInterventionResourcesData) {
            $demandeInterventionRessource = new JsonApiResource($demandeInterventionResourcesData, $demandeInterventionResources['included']);

            $relatedEquipement = null;
            $mrEqptsResources = $demandeInterventionRessource->getRelationshipCollection('MREqpt');
            foreach ($mrEqptsResources as $mrEqptResource) {
                $isDirectEquipement = true === $mrEqptResource->getAttribute('directEqpt');
                $isAnAllowedLocationEqptType = in_array($mrEqptResource->getRelationship('eqpt')->getType(), CarlClient::CARL_ALLOWED_LOCATION_EQPT_TYPES);
                if (
                    $isDirectEquipement
                    && $isAnAllowedLocationEqptType
                ) {
                    $eqptRessource = $mrEqptResource->getRelationship('eqpt');
                    if (!is_null($eqptRessource)) {
                        $carlObjectEqpt = new CarlObject($eqptRessource->getId(), $eqptRessource->getType());
                        $relatedEquipement = new Equipement($carlObjectEqpt->getSilabId(), $eqptRessource->getAttribute('description'));
                    }
                    break;
                }
            }

            $demandeInterventions[] = (new DemandeIntervention())
                ->setId($demandeInterventionRessource->getId())
                ->setCode($demandeInterventionRessource->getAttribute('code'))
                ->setTitle($demandeInterventionRessource->getAttribute('description'))
                ->setCurrentStatus($this->getCurrentStatusFromApiResource($demandeInterventionRessource))
                ->setCreatedBy($this->carlClient->getUserEmailFromIdSafe($demandeInterventionRessource->getRelationship('createdBy')->getId()))
                ->setCreatedAt(new \DateTime($demandeInterventionRessource->getAttribute('creationDate')))
                ->setUpdatedAt(new \DateTime($demandeInterventionRessource->getAttribute('modifyDate')))
                ->setImageUrl($this->carlClient->getObjectDocumentUrl($demandeInterventionRessource->getId(), $this->carlConfigurationIntervention->getPhotoAvantInterventionDoctypeId()))
                ->setDescription($demandeInterventionRessource->getRelationship('longDesc')?->getAttribute('rawDescription'))
                ->setRelatedEquipement($relatedEquipement);
        }

        return $demandeInterventions;
    }

    private function getCurrentStatusFromApiResource(JsonApiResource $demandeInterventionRessource): DemandeInterventionStatus
    {
        return new DemandeInterventionStatus(
            code: $this->getSilabStatusCodeFromCarlStatusCode($demandeInterventionRessource->getAttribute('statusCode')),
            label: $this->carlClient->getStatusDescriptionFromCodeAndFlowCode(
                $demandeInterventionRessource->getAttribute('statusCode'),
                'MRSTATUS'
            ),
            createdBy: $this->carlClient->getUserEmailFromIdSafe($demandeInterventionRessource->getRelationship('createdBy')->getId()),
            createdAt: new \DateTime($demandeInterventionRessource->getAttribute('statusChangedDate')),
        );
    }

    /**
     * @param array<string>       $relatedEquipementIds
     * @param array<string,mixed> $criteria
     *
     * @return array<DemandeIntervention>
     */
    public function findByRelatedEquipementIds(array $relatedEquipementIds, array $criteria): array
    {
        // on doit convertir les id récupérés
        $relatedEquipementDecodedCarlIds = array_map(
            function (string $relatedEquipementId) {
                return CarlEqptIdAndType::fromJson($relatedEquipementId)->getId();
            },
            $relatedEquipementIds
        );

        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'statuses',
            ]
        );

        $includes = [
            'MR',
            'MR.createdBy',
            'MR.longDesc',
            'MR.statusChangedBy',
            'eqpt',
        ];

        $fields = [
           'mr' => [
               'code',
               'description',
               'creationDate',
               'modifyDate',
               'createdBy',
               'longDesc',
               'statusCode',
               'statusChangedDate',
               'statusChangedBy',
               ],
            'actor' => [
                'id',
            ],
            'description' => [
                'rawDescription',
            ],
        ];

        $filters = [
            'eqpt.id' => $relatedEquipementDecodedCarlIds,
        ];

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $statuses = $propertyAccessor->getValue($criteria, '[statuses]');
        if (!empty($statuses)) {
            $filters['MR.statusCode'] = $this->getCarlStatusCodesFromSilabStatusCodes($statuses);
        }

        $orderBy = [
            '-modifyDate',
        ];

        $mrEqptResources = $this->carlClient->getCarlObjectsRaw(
            'mreqpt',
            $filters,
            $includes,
            $fields,
            $orderBy,
        );

        /** @var array<DemandeIntervention> $demandeInterventions */
        $demandeInterventions = [];
        foreach ($mrEqptResources['data'] as $mrEqptResourcesData) {
            $mrEqptRessource = new JsonApiResource($mrEqptResourcesData, $mrEqptResources['included']);
            $demandeInterventionRessource = $mrEqptRessource->getRelationship('MR');
            $eqptRessource = $mrEqptRessource->getRelationship('eqpt');
            $carlObjectEqpt = new CarlObject($eqptRessource->getId(), $eqptRessource->getType());
            $demandeInterventions[] = (new DemandeIntervention())
            ->setId($demandeInterventionRessource->getId())
            ->setCode($demandeInterventionRessource->getAttribute('code'))
            ->setTitle($demandeInterventionRessource->getAttribute('description'))
            ->setCurrentStatus($this->getCurrentStatusFromApiResource($demandeInterventionRessource))
            ->setCreatedBy($this->carlClient->getUserEmailFromIdSafe($demandeInterventionRessource->getRelationship('createdBy')->getId()))
            ->setCreatedAt(new \DateTime($demandeInterventionRessource->getAttribute('creationDate')))
            ->setUpdatedAt(new \DateTime($demandeInterventionRessource->getAttribute('modifyDate')))
            ->setImageUrl($this->carlClient->getObjectDocumentUrl($demandeInterventionRessource->getId(), $this->carlConfigurationIntervention->getPhotoAvantInterventionDoctypeId()))
            ->setDescription($demandeInterventionRessource->getRelationship('longDesc')?->getAttribute('rawDescription'))
            ->setRelatedEquipement(
                new Equipement($carlObjectEqpt->getSilabId(), $eqptRessource->getAttribute('description'))
            );
        }

        return $demandeInterventions;
    }

    public function findOneBy(array $criteria): DemandeIntervention
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName(): string
    {
        return DemandeIntervention::class;
    }

    public function update(string $id, DemandeIntervention $demandeIntervention, string $userEmail): DemandeIntervention
    {
        $originalDemandeIntervention = $this->find($id);

        if (!is_null($demandeIntervention->getImageB64())) {
            throw new RuntimeException("Carl ne prend pas en charge les fichiers en base64. Il ne doit pas exister d'attribut 'imageB64' dans l'entité DemandeIntervention transmise à carl (merci de connecter une GED)");
        }

        $attributes = [];

        // Note: quand nous gèrerons plusieurs images, il faudra modifier l'appel et passer simplement le tableau
        $this->carlClient->updateUrlDocumentLinks(
            newAttachmentsUrls: is_null($demandeIntervention->getImageUrl()) ? [] : [$demandeIntervention->getImageUrl()],
            objectId: $id,
            entityJavaBean: 'com.carl.xnet.works.backend.bean.AbstractMRBean',
            docTypeId: $this->carlConfigurationIntervention->getPhotoAvantInterventionDoctypeId(),
            userEmail: $userEmail
        );

        $attributes[CarlClient::CARL_WO_MR_IMAGE_FIELD] = $demandeIntervention->getImageUrl();

        $attributes['description'] = $demandeIntervention->getTitle();

        $relationships = [];

        if (!empty($demandeIntervention->getDescription())) {
            if (!empty($originalDemandeIntervention->getDescription())) {
                $this->carlClient->patchCarlObject(
                    'description',
                    $this->carlClient->getCarlObject('mr', $id, ['longDesc'])
                        ->getRelationship('longDesc')
                        ->getId(),
                    ['description' => $demandeIntervention->getDescription()]
                );
            } else {
                $this->carlClient->addDescriptionToObjectRelationshipsArray($relationships, $demandeIntervention->getDescription());
            }
        } elseif (!empty($originalDemandeIntervention->getDescription())) {
            $this->carlClient->deleteObjectDescription($id, 'mr');
        }

        $isNewEquipementDifferentFromActual = $demandeIntervention->getRelatedEquipement() !== $originalDemandeIntervention->getRelatedEquipement();
        if ($isNewEquipementDifferentFromActual) {
            if (!is_null($originalDemandeIntervention->getRelatedEquipement())) {
                // supression des rattachements d'équipement existants
                $attachedEquipementsData = $this->carlClient->getCarlObject('mr', $id, includes: ['MREqpt'])
                        ->getRelationshipCollection('MREqpt');
                foreach ($attachedEquipementsData as $attachedEquipementData) {
                    $this->carlClient->deleteObject(
                        'mreqpt', $attachedEquipementData->getId()
                    );
                }
            }

            if (!is_null($demandeIntervention->getRelatedEquipement())) {
                // Création du nouvel attachement
                $this->attachDemandeInterventionToEquipement($id, $demandeIntervention->getRelatedEquipement()->getId());

                // Mise a jour de l'organisme en charge
                $relatedEquipementIdAndType = CarlEqptIdAndType::fromJson($demandeIntervention->getRelatedEquipement()->getId());

                $relatedEquipementApiResource = $this->carlClient->getCarlObject(
                    $relatedEquipementIdAndType->getType(),
                    $relatedEquipementIdAndType->getId(),
                    ['site'],
                    ['site' => ['id']]
                );

                $relationships['site']['data'] = [
                    'id' => $relatedEquipementApiResource->getRelationship('site')->getId(),
                    'type' => 'site',
                ];
            }
        }

        $this->carlClient->patchCarlObject('mr', $id, $attributes, $relationships);

        return $this->find($id);
    }

    public function save(DemandeIntervention $demandeIntervention): DemandeIntervention
    {
        $attributesPreset = $this->carlConfigurationIntervention->getCarlAttributesPresetForDemandeInterventionCreation();
        $relationShipsPreset = $this->carlConfigurationIntervention->getApiFormatedCarlRelationshipsPresetForDemandeInterventionCreation();
        $relatedEquipementIdAndType = CarlEqptIdAndType::fromJson($demandeIntervention->getRelatedEquipement()->getId());

        $relatedEquipementApiResource = $this->carlClient->getCarlObject(
            $relatedEquipementIdAndType->getType(),
            $relatedEquipementIdAndType->getId(),
            ['site'],
            ['site' => ['id']]
        );

        if (!empty($demandeIntervention->getImageB64())) {
            throw new RuntimeException("Carl ne prend pas en charge les fichiers en base64. Il ne doit pas exister d'attribut 'imageB64' dans l'entité Intervention transmise à carl (merci de connecter une GED)");
        }
        $demandeInterventionArray = [
            'data' => [
                'type' => 'mr',
                'attributes' => array_merge(
                    [
                       'description' => $demandeIntervention->getTitle(),
                    ],
                    $attributesPreset
                ),
                'relationships' => array_merge(
                    [
                        'createdBy' => [
                            'data' => [
                                'id' => $this->carlClient->getUserIdFromEmail($demandeIntervention->getCreatedBy()),
                                'type' => 'actor',
                            ],
                        ],
                        'site' => [
                            'data' => [
                                'id' => $relatedEquipementApiResource->getRelationship('site')->getId(),
                                'type' => 'site',
                            ],
                        ],
                    ],
                    $relationShipsPreset
                ),
            ],
        ];

        if (!is_null($demandeIntervention->getDescription())) {
            $this->carlClient->addDescriptionToObjectRelationshipsArray($demandeInterventionArray['data']['relationships'], $demandeIntervention->getDescription());
        }

        $createdDemandeInterventionApiResource = $this->carlClient->postCarlObject('mr', json_encode($demandeInterventionArray));

        $relatedEquipement = $demandeIntervention->getRelatedEquipement();
        if (!is_null($relatedEquipement)) {
            $this->attachDemandeInterventionToEquipement($createdDemandeInterventionApiResource->getId(), $relatedEquipement->getId());
            $demandeIntervention->setRelatedEquipement($this->carlEquipementRepository->find($relatedEquipement->getId()));
        }

        if ($demandeIntervention->getImageUrl()) {
            $this->ajouterPhoto(
                $demandeIntervention->getImageUrl(),
                $createdDemandeInterventionApiResource->getId(),
                $this->carlClient->getUserIdFromEmail($demandeIntervention->getCreatedBy())
            );
        }

        return $this->find($createdDemandeInterventionApiResource->getId());
    }

    private function ajouterPhoto(string $imageUrl, string $demandeInterventionId, string $userId): void
    {
        $this->carlClient->addDocumentLinkToObject(
            $imageUrl,
            $demandeInterventionId,
            'com.carl.xnet.works.backend.bean.AbstractMRBean',
            $userId,
            $this->carlConfigurationIntervention->getPhotoAvantInterventionDoctypeId()
        );
    }

    private function attachDemandeInterventionToEquipement(string $demandeInterventionId, string $equipmentId): void
    {
        // 1. suppression des liaisons existantes
        // 1.1 récupération des eqpt liés
        $carlMrEqpts = $this->carlClient->getCarlObjectsRaw(
            entity: 'mreqpt',
            filters: ['MR.id' => [$demandeInterventionId]],
        );
        // 1.2 suppression des eqpt liés
        foreach ($carlMrEqpts['data'] as $carlMrEqpt) {
            $this->carlClient->deleteObject(
                entity: 'mreqpt',
                id: $carlMrEqpt['id']);
        }

        // 2. ajout de l'équipement direct
        $carlEqptIdAndType = CarlEqptIdAndType::fromJson($equipmentId);
        $this->linkEqptToMr(
            equipement: $carlEqptIdAndType,
            mrId: $demandeInterventionId,
            directEqpt: true
        );

        // 3. ajout des équipements parents de l'équipement direct
        $parentsLinkequipments = $this->carlClient->getCarlObject(
            entity: $carlEqptIdAndType->getType(),
            id: $carlEqptIdAndType->getId(),
            includes: ['parents', 'parents.parent']
        )->getRelationshipCollection('parents');

        foreach ($parentsLinkequipments as $parentLinkequipment) {
            $this->linkEqptAndAncestorsToMrRecursive(equipement: $parentLinkequipment->getRelationship('parent'), mrId: $demandeInterventionId);
        }
    }

    private function linkEqptAndAncestorsToMrRecursive(JsonApiResource $equipement, string $mrId): void
    {
        $this->linkEqptToMr(
            equipement: $equipement,
            mrId: $mrId
        );

        $parentsLinkequipments = $this->carlClient->getCarlObject(
            entity: $equipement->getType(),
            id: $equipement->getId(),
            includes: ['parents', 'parents.parent']
        )->getRelationshipCollection('parents');

        foreach ($parentsLinkequipments as $parentLinkequipment) {
            if (!is_null($parentLinkequipment->getRelationship('parent'))) {
                $this->linkEqptAndAncestorsToMrRecursive(equipement: $parentLinkequipment->getRelationship('parent'), mrId: $mrId);
            }
        }
    }

    private function linkEqptToMr(JsonApiResource|CarlObject $equipement, string $mrId, bool $directEqpt = false): JsonApiResource
    {
        $carlMrEqptArray = [
            'data' => [
                'type' => 'mreqpt',
                'attributes' => [
                    'directEqpt' => $directEqpt,
                ],
                'relationships' => [
                    'MR' => [
                        'data' => [
                            'id' => $mrId,
                            'type' => 'mr',
                        ],
                    ],
                    'eqpt' => [
                        'data' => [
                            'id' => $equipement->getId(),
                            'type' => $equipement->getType(),
                        ],
                    ],
                ],
            ],
        ];

        return $this->carlClient->postCarlObject('mreqpt', json_encode($carlMrEqptArray));
    }

    public function transistionToStatus(string $id, DemandeInterventionStatus $status): DemandeInterventionStatus
    {
        $this->carlClient->transitionToStatus(
            'mr',
            $id,
            $this->getCarlStatusCodeFromSilabStatusCode($status->getCode()),
            CarlClient::DEMANDEINTERVENTION_STATUS_WORKFLOW
        );

        return $this->find($id)->getCurrentStatus();
    }

    /**
     * @param array<string> $silabStatuses
     *
     * @return array<string>
     */
    private function getCarlStatusCodesFromSilabStatusCodes(array $silabStatuses): array
    {
        return array_unique(array_reduce(
            $silabStatuses,
            function ($carlStatuses, $silabStatus) {
                if (!array_key_exists($silabStatus, CarlClient::SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING)) {
                    throw new RuntimeException("Le status $silabStatus n'a pas de statut carl correspondant, cf. CarlClient::SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING.");
                }

                return array_merge($carlStatuses, CarlClient::SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING[$silabStatus]);
            },
            []
        ));
    }

    private function getCarlStatusCodeFromSilabStatusCode(string $silabStatusCode): string
    {
        $carlStatusCode = null;

        foreach (CarlClient::CARL_TO_SILAB_DEMANDEINTERVENTION_STATUS_MAPPING as $currentCarlStatusCode => $silabStatusCodes) {
            if (in_array($silabStatusCode, $silabStatusCodes)) {
                if (!is_null($carlStatusCode)) {
                    throw new RuntimeException("Plusieurs status de demande d'intervention Silab sont possible pour le status carl $silabStatusCode, veuillez vérifier le mapping.");
                }
                $carlStatusCode = $currentCarlStatusCode;
            }
        }

        if (is_null($carlStatusCode)) {
            throw new RuntimeException("Aucun status de demande d'intervention Silab trouvé pour le status carl $silabStatusCode, veuillez vérifier le mapping.");
        }

        return $carlStatusCode;
    }

    private function getSilabStatusCodeFromCarlStatusCode(string $carlStatusCode): string
    {
        $silabStatusCode = null;

        foreach (CarlClient::SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING as $currentSilabStatusCode => $carlStatusCodes) {
            if (in_array($carlStatusCode, $carlStatusCodes)) {
                if (!is_null($silabStatusCode)) {
                    throw new RuntimeException("Plusieurs status de demande d'intervention Silab sont possible pour le status carl $carlStatusCode, veuillez vérifier le mapping.");
                }
                $silabStatusCode = $currentSilabStatusCode;
            }
        }

        if (is_null($silabStatusCode)) {
            throw new RuntimeException("Aucun status de demande d'intervention Silab trouvé pour le status carl $carlStatusCode, veuillez vérifier le mapping.");
        }

        return $silabStatusCode;
    }

    public function getStatusHistory(string $id): array
    {
        $filters = [
            'origin.id' => [$id],
        ];
        $includes = [
            'statusChangedBy',
        ];
        $fields = [
            'actor' => ['id'],
        ];
        $unsortedHistorisedStatusesResponse = $this->carlClient->getCarlObjectsRaw(
            entity: 'mrhistostatus',
            filters: $filters,
            includes: $includes,
            fields: $fields,
        );

        /** @var array<DemandeInterventionHistorisedStatus> $statusHistory */
        $statusHistory = [];
        foreach ($unsortedHistorisedStatusesResponse['data'] as $unsortedHistorisedStatusesResponseData) {
            $historisedStatusApiResource = new JsonApiResource($unsortedHistorisedStatusesResponseData, $unsortedHistorisedStatusesResponse['included']);

            $statusHistory[] = new DemandeInterventionHistorisedStatus(
                $historisedStatusApiResource->getAttribute('statusCode'),
                $this->carlClient->getStatusDescriptionFromCodeAndFlowCode(
                    $historisedStatusApiResource->getAttribute('statusCode'),
                    'MRSTATUS'
                ),
                $this->carlClient->getUserEmailFromIdSafe($historisedStatusApiResource->getRelationship('statusChangedBy')->getId()),
                new \DateTime($historisedStatusApiResource->getAttribute('statusChangedDate')),
                $historisedStatusApiResource->getAttribute('comment')
            );
        }

        // Note: on fait le tri ici car en rest ça ne marche pas actuellement (sur cette entitée en tt cas)
        usort(
            $statusHistory,
            function (DemandeInterventionHistorisedStatus $a, DemandeInterventionHistorisedStatus $b) {
                return ((int) $b->getCreatedAt()->format('Uu')) - ((int) $a->getCreatedAt()->format('Uu'));
            }
        );

        return $statusHistory;
    }
}
