<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\Shared\Exception\InsufficientRolesException;
use App\Shared\User\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeleteDemandeInterventionController extends AbstractController
{
    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository, private Security $security
    ) {
    }

    // QUESTION_TECHNIQUE: Comment faire avec api-platform, si on utilise une operation delete on a une erreur similaire a https://github.com/api-platform/core/issues/5827
    #[Route('/api/intervention-service-offers/{serviceOfferId}/demandes-interventions/{id}',
        name: 'delete_demande_intervention', methods: ['DELETE'])]
    public function deleteDemandeIntervention(string $serviceOfferId, string $id): Response
    {
        // Securité :
        $curentUser = $this->security->getUser();
        assert($curentUser instanceof User);

        if ($this->security->isGranted("SERVICEOFFER_{$serviceOfferId}_ROLE_INTERVENTION_SUPPRIMER_DEMANDES_INTERVENTIONS")) {
            $interventionServiceOffer = $this->interventionServiceOfferRepository->find($serviceOfferId);

            $demandeInterventionRepository = new DemandeInterventionRepository($interventionServiceOffer);

            $demandeInterventionRepository->delete($id);

            return new Response(status: 204);
        } else {
            throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour supprimer cette demande d'intervention");
        }
    }
}
