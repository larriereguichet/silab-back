<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;
use App\Shared\Exception\BadRequestException;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final class CreateDemandeInterventionDataHandler implements ProcessorInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
        private Security $security
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): DemandeIntervention
    {
        assert($operation instanceof Post);
        assert($data instanceof DemandeIntervention);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $demandeInterventionRepository = new DemandeInterventionRepository($interventionServiceOffer);
        /*        $equipementRepository = new EquipementRepository($interventionServiceOffer);

                 $lesEquipementsLiésSontObligatoiresSurCetteODS = !empty($interventionServiceOffer->getAvailableEquipementsIds());
                $laDemandeEnvoyéeNeContientPasDEquipementLié = is_null($data->getRelatedEquipement());
                if ($lesEquipementsLiésSontObligatoiresSurCetteODS && $laDemandeEnvoyéeNeContientPasDEquipementLié) {
                    throw new BadRequestException("La configuration de l'offre de service définissant des équipements il faut obligatoirement rattacher la demande d'intervention à un équipement.");
                }

                $equipementsInclusEtLeursDescendants = array_reduce(
                    $interventionServiceOffer->getAvailableEquipementsIds(),
                    function ($accumulatedEquipements, $availableEquipementId) use ($equipementRepository) {
                        return array_merge(
                            $accumulatedEquipements,
                            [$availableEquipementId],
                            array_map(
                                function ($equipement) {return $equipement->getId(); },
                                $equipementRepository->findChildren($availableEquipementId)
                            )
                        );
                    },
                    []
                );
                $laDemandeCrééeNEstPasAssocieeAUnEquipementInclusDansCetteOds = !in_array(
                    $data
                        ->getRelatedEquipement()
                        ->getId(),
                    $equipementsInclusEtLeursDescendants
                );
                if ($lesEquipementsLiésSontObligatoiresSurCetteODS && $laDemandeCrééeNEstPasAssocieeAUnEquipementInclusDansCetteOds) {
                    throw new AccessDeniedException('Vous ne pouvez pas créer une demande associée à un équipement qui ne concerne pas votre offre de service.');
                } */

        $currentUser = $this->security->getUser();
        assert($currentUser instanceof User);

        $data->setCreatedBy($currentUser->getEmail());

        $demandeInterventionWithInternalImageUrl = $this->replaceObjectExternalImageUrlWithInternalImageUrl(
            $interventionServiceOffer,
            $demandeInterventionRepository->save($data)
        );

        assert($demandeInterventionWithInternalImageUrl instanceof DemandeIntervention);

        return $demandeInterventionWithInternalImageUrl;
    }
}
