<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeInterventionHistorisedStatus;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final class GetDemandeInterventionStatusHistoryDataHandler implements ProviderInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<DemandeInterventionHistorisedStatus>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $demandeInterventionRepository = new DemandeInterventionRepository($interventionServiceOffer);

        $demandeIntervention = $demandeInterventionRepository->find($uriVariables['demandeInterventionId']);

        $laDemandeNEstPasAssocieeAUnEquipementDéfinitDansCetteOds = !in_array(
            $demandeIntervention
                ->getRelatedEquipement()
                ->getId(),
            $interventionServiceOffer->getAvailableEquipementsIds()
        );
        $lesEquipementsLiesSontObligatoiresSurCetteODS = !empty($interventionServiceOffer->getAvailableEquipementsIds());
        // quickfix silab-front#454
        // if ($lesEquipementsLiesSontObligatoiresSurCetteODS && $laDemandeNEstPasAssocieeAUnEquipementDéfinitDansCetteOds) {
        //     throw new AccessDeniedException("L'intervention à laquelle vous tentez d'accéder est associée à un équipement qui ne concerne pas votre offre de service.");
        // }

        return $demandeInterventionRepository->getStatusHistory($demandeIntervention->getId());
    }
}
