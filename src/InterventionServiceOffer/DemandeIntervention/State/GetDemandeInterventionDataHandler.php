<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final class GetDemandeInterventionDataHandler implements ProviderInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): DemandeIntervention
    {
        assert($operation instanceof Get);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $demandeInterventionRepository = new DemandeInterventionRepository($interventionServiceOffer);

        $demandeInterventionWithInternalImageUrl = $this->replaceObjectExternalImageUrlWithInternalImageUrl(
            $interventionServiceOffer,
            $demandeInterventionRepository->find($uriVariables['id'])
        );
        $laDemandeNEstPasAssocieeAUnEquipementDéfinitDansCetteOds = !in_array(
            $demandeInterventionWithInternalImageUrl
                ->getRelatedEquipement()
                ?->getId(),
            $interventionServiceOffer->getAvailableEquipementsIds()
        );
        $lesEquipementsLiesSontObligatoiresSurCetteODS = !empty($interventionServiceOffer->getAvailableEquipementsIds());
        // quickfix silab-front#454
        // if ($lesEquipementsLiesSontObligatoiresSurCetteODS && $laDemandeNEstPasAssocieeAUnEquipementDéfinitDansCetteOds) {
        //     throw new AccessDeniedException("L'intervention à laquelle vous tentez d'accéder est associée à un équipement qui ne concerne pas votre offre de service.");
        // }

        assert($demandeInterventionWithInternalImageUrl instanceof DemandeIntervention);

        return $demandeInterventionWithInternalImageUrl;
    }
}
