<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Put;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;
use App\Shared\Exception\BadRequestException;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final class UpdateDemandeInterventionDataHandler implements ProcessorInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
        private Security $security
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): DemandeIntervention {
        assert($operation instanceof Put);
        assert($data instanceof DemandeIntervention);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $demandeInterventionRepository = new DemandeInterventionRepository($interventionServiceOffer);
        /*               $equipementRepository = new EquipementRepository($interventionServiceOffer);

               $initialDemandeIntervention = $demandeInterventionRepository->find($uriVariables['id']);

        $lesEquipementsLiesSontObligatoiresSurCetteODS = !empty($interventionServiceOffer->getAvailableEquipementsIds());
               $laDemandeEnvoyeeNeContientPasDEquipementLie = is_null($data->getRelatedEquipement());
               if ($lesEquipementsLiesSontObligatoiresSurCetteODS && $laDemandeEnvoyeeNeContientPasDEquipementLie) {
                   throw new BadRequestException("La configuration de l'offre de service définissant des équipements il faut obligatoirement rattacher la demande d'intervention à un équipement.");
               }

               $equipementsInclusDansCetteOdsEtLeursDescendants = array_reduce(
                   $interventionServiceOffer->getAvailableEquipementsIds(),
                   function ($accumulatedEquipements, $availableEquipementId) use ($equipementRepository) {
                       return array_merge(
                           $accumulatedEquipements,
                           [$availableEquipementId],
                           array_map(
                               function ($equipement) {return $equipement->getId(); },
                               $equipementRepository->findChildren($availableEquipementId)
                           )
                       );
                   },
                   []
               );
               $laDemandeInitialeNEstPasAssocieeAUnEquipementInclusDansCetteOds = !in_array(
                   $initialDemandeIntervention
                       ->getRelatedEquipement()
                       ->getId(),
                   $equipementsInclusDansCetteOdsEtLeursDescendants
               );
               // quickfix silab-front#454
               if ($lesEquipementsLiesSontObligatoiresSurCetteODS && $laDemandeInitialeNEstPasAssocieeAUnEquipementInclusDansCetteOds) {
                   throw new AccessDeniedException("La demande d'intervention à laquelle vous tentez d'accéder n'est pas associée à un équipement qui concerne votre offre de service.");
               }

               $laDemandeMiseAJourNEstPasAssocieeAUnEquipementInclusDansCetteOds = !in_array(
                   $data
                       ->getRelatedEquipement()
                       ->getId(),
                   $equipementsInclusDansCetteOdsEtLeursDescendants
               );
               // quickfix silab-front#454
               if ($lesEquipementsLiesSontObligatoiresSurCetteODS && $laDemandeMiseAJourNEstPasAssocieeAUnEquipementInclusDansCetteOds) {
                   throw new AccessDeniedException('Vous ne pouvez pas associer à cette demande un équipement qui ne concerne pas votre offre de service.');
               }
*/
        $currentUser = $this->security->getUser();
        assert($currentUser instanceof User);

        $demandeInterventionWithInternalImageUrl = $this->replaceObjectExternalImageUrlWithInternalImageUrl(
            $interventionServiceOffer,
            $demandeInterventionRepository->update($uriVariables['id'], $data, $currentUser->getEmail())
        );

        assert($demandeInterventionWithInternalImageUrl instanceof DemandeIntervention);

        return $demandeInterventionWithInternalImageUrl;
    }
}
