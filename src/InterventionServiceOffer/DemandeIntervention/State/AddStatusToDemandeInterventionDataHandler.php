<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeInterventionStatus;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

final class AddStatusToDemandeInterventionDataHandler implements ProcessorInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
        private Security $security
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): DemandeInterventionStatus
    {
        assert($operation instanceof Post);
        assert($data instanceof DemandeInterventionStatus);
        $currentUser = $this->security->getUser();
        assert($currentUser instanceof User);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $demandeInterventionRepository = new DemandeInterventionRepository($interventionServiceOffer);

        // todo: qui peut faire quoi et comment

        return $demandeInterventionRepository->transistionToStatus($uriVariables['demandeInterventionId'], new DemandeInterventionStatus(code: $data->getCode(), createdBy: $currentUser->getEmail()));
    }
}
