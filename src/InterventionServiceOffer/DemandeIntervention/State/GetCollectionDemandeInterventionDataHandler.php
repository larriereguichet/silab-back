<?php

namespace App\InterventionServiceOffer\DemandeIntervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

final class GetCollectionDemandeInterventionDataHandler implements ProviderInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<DemandeIntervention>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $demandeInterventionRepository = new DemandeInterventionRepository($interventionServiceOffer);

        $criteria = $context['filters'] ?? [];

        // on veut toujours des tableaux
        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        $lOffreDeServicePorteSurDesEquipements = !empty($interventionServiceOffer->getAvailableEquipementsIds());
        $onAFiltréSurDesEquipements = array_key_exists('relatedEquipement.id', $criteria);
        if ($lOffreDeServicePorteSurDesEquipements) {
            // on restreint les équipements à ceux définits dans l'ods
            if ($onAFiltréSurDesEquipements) {
                $criteria['relatedEquipement.id'] = array_intersect($criteria['relatedEquipement.id'], $interventionServiceOffer->getAvailableEquipementsIds());
            }
            // par défault on restreint aux equipements de l'ODS
            if (!$onAFiltréSurDesEquipements) {
                $criteria['relatedEquipement.id'] = $interventionServiceOffer->getAvailableEquipementsIds();
            }
        }

        $demandesInterventionsWithInternalImageUrl = $this->replaceObjectsExternalImageUrlWithInternalImageUrl(
            $interventionServiceOffer,
            $demandeInterventionRepository->findBy($criteria)
        );

        return $demandesInterventionsWithInternalImageUrl;
    }
}
