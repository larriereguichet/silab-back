<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use ApiPlatform\Exception\RuntimeException as ExceptionRuntimeException;
use App\Entity\Intervention;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Repository\Intervention\InterventionRepositoryInterface;

class InterventionRepository implements InterventionRepositoryInterface
{
    private InterventionRepositoryInterface $specificInterventionRepository;

    public function __construct(
        protected InterventionServiceOffer $interventionServiceOffer
    ) {
        try {
            $this->specificInterventionRepository = match ($interventionServiceOffer->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => new CarlInterventionRepository(
                    $interventionServiceOffer
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOffer->getGmaoConfiguration()::class);
        }
    }

    public function update(string $id, Intervention $entity, string $userEmail): Intervention
    {
        $entity->setTitleFromActionTypeAndCoordinates($this->interventionServiceOffer->getGmaoConfiguration()->getActionTypesMap());

        $hasImageB64 = !is_null($entity->getImageB64());

        if ($hasImageB64 && $this->interventionServiceOffer->utiliseUneGED()) {
            $originalIntervention = $this->specificInterventionRepository->find($id);
            $gedClient = $this->interventionServiceOffer->getGedClient();

            $originalInterventionHasImage = !empty($originalIntervention->getImageUrl());

            if ($originalInterventionHasImage) {
                $originalInterventionImageIsDifferentThanTheNewOne = base64_encode($gedClient->getFile($originalIntervention->getImageUrl())) !== $entity->getImageB64();
                if ($originalInterventionImageIsDifferentThanTheNewOne) {
                    $gedClient->updateFile(
                        $this->find($id)->getImageUrl(),
                        $entity->getImageB64(),
                        $entity->getTitle()
                    );
                }
                $entity->setImageUrl($originalIntervention->getImageUrl()); // note A terme il faudrait récuperer le nouvel URL; actuellement il est identique à l'original
            } else {
                $gedConfiguration = $this->interventionServiceOffer->getGedClientConfiguration();
                if (is_null($gedConfiguration)) {
                    throw new ExceptionRuntimeException("La configuration du client ged est absente ou corrompue pour l'offre de service ".$this->interventionServiceOffer->getTitle().'.');
                }
                $gedDocumentData = array_map(
                    function ($variable) use ($originalIntervention) {
                        return $variable($originalIntervention, $this->interventionServiceOffer);
                    },
                    self::getGedClientConfigurationVariable()
                );

                $imageUrl = $gedClient->persistFile(
                    $entity->getImageB64(),
                    $originalIntervention->getTitle(),
                    $gedConfiguration,
                    $gedDocumentData
                );
                $entity->setImageUrl($imageUrl);
            }

            $entity->setImageB64(null);
        }

        return $this->specificInterventionRepository->update($id, $entity, $userEmail);
    }

    public function find(mixed $id): Intervention
    {
        $sampleInterventionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return array_pop($sampleInterventionArray);
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        $criteria['interventionActionTypes'] ??= $this->getAllowedActionTypesIds();
        $orderBy ??= ['createdAt'];

        if ($this->areActionTypesOutsideServiceOfferScope($criteria['interventionActionTypes'])) {
            throw new RuntimeException("Vous avez demandé à voir des natures d'interventions qui sortent du cadre de cette offre de service : ".implode(', ', array_diff($criteria['interventionActionTypes'], $this->getAllowedActionTypesIds())));
        }

        $interventions = $this->specificInterventionRepository->findBy($criteria, $orderBy, $limit, $offset);
        foreach ($interventions as $intervention) {
            $intervention->setGmaoObjectViewUrl(
                $this->interventionServiceOffer->getGmaoConfiguration()->getInterventionViewUrl($intervention)
            );
        }

        return $interventions;
    }

    public function findAll(): array
    {
        return $this->findBy([]);
    }

    public function findOneBy(array $criteria): Intervention
    {
        $sampleInterventionArray = $this->findBy($criteria);

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException('Aucune intervention trouvée avec les critères donnés');
        }

        return array_values($sampleInterventionArray)[0];
    }

    public function save(Intervention $entity): Intervention
    {
        if ($this->isActionTypeOutsideServiceOfferScope($entity->getActionType())) {
            throw new RuntimeException("La nature d'intervention ".$entity->getActionType().' ne pas être assignée à une intervention Carl. Les valeurs accéptées sont les suivantes : '.implode(', ', $this->getAllowedActionTypesIds()));
        }

        if (is_null($entity->getLatitude())) {
            throw new RuntimeException("La latitude n'a pas été définie");
        }

        if (is_null($entity->getLongitude())) {
            throw new RuntimeException("La longitude n'a pas été définie");
        }

        $entity->setTitleFromActionTypeAndCoordinates($this->interventionServiceOffer->getGmaoConfiguration()->getActionTypesMap());

        $hasImageB64 = !is_null($entity->getImageB64());
        if ($hasImageB64 && $this->interventionServiceOffer->utiliseUneGED()) {
            $gedConfiguration = $this->interventionServiceOffer->getGedClientConfiguration();
            if (is_null($gedConfiguration)) {
                throw new RuntimeException("La configuration du client ged est absente ou corrompue pour l'offre de service ".$this->interventionServiceOffer->getTitle().'.');
            }
            $gedDocumentData = array_map(
                function ($variable) use ($entity) {
                    return $variable($entity, $this->interventionServiceOffer);
                },
                self::getGedClientConfigurationVariable()
            );

            $imageUrl = $this->interventionServiceOffer->getGedClient()->persistFile(
                $entity->getImageB64(),
                $entity->getTitle(),
                $gedConfiguration,
                $gedDocumentData
            );
            $entity->setImageUrl($imageUrl);
            $entity->setImageB64(null);
        }

        return $this->find($this->specificInterventionRepository->save($entity)->getId());
    }

    public function getClassName()
    {
        return Intervention::class;
    }

    /**
     * @param array<string> $actionTypesIds
     */
    private function areActionTypesOutsideServiceOfferScope(array $actionTypesIds): bool
    {
        $requestedActionTypesThatAreOutsideOfServiceOfferScope =
            array_diff(
                $actionTypesIds,
                $this->getAllowedActionTypesIds()
            );

        return !empty(
            $requestedActionTypesThatAreOutsideOfServiceOfferScope
        );
    }

    private function isActionTypeOutsideServiceOfferScope(string $actionTypeId): bool
    {
        return $this->areActionTypesOutsideServiceOfferScope([$actionTypeId]);
    }

    /**
     * @return array<string>
     */
    private function getAllowedActionTypesIds(): array
    {
        return array_keys($this->interventionServiceOffer->getGmaoConfiguration()->getActionTypesMap());
    }

    /**
     * @return array<string,mixed>
     */
    public static function getGedClientConfigurationVariable(): array
    {
        return [
                'Object.decription' => function (Intervention $intervention, InterventionServiceOffer $interventionServiceOffer): ?string {return $intervention->getDescription(); },
                'Object.title' => function (Intervention $intervention, InterventionServiceOffer $interventionServiceOffer): string {return $intervention->getTitle(); },
                'ServiceOffer.title' => function (Intervention $intervention, InterventionServiceOffer $interventionServiceOffer): string {return $interventionServiceOffer->getTitle(); },
        ];
    }
}
