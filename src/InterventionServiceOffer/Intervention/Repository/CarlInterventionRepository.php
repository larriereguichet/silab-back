<?php

namespace App\InterventionServiceOffer\Intervention\Repository;

use App\Entity\Intervention;
use App\Entity\InterventionStatusHistorised;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Service\JsonApiResource;
use App\Service\UnexpectedCriteriaCheckingTrait;
use App\Service\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Exception\MultipleEmailsFoundForUser;
use App\Shared\Carl\Exception\NoEmailFoundForUser;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlInterventionRepository extends InterventionRepository
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    private CarlConfigurationIntervention $carlConfigurationIntervention;
    private CarlClient $carlClient;

    public function __construct(
        protected InterventionServiceOffer $interventionServiceOffer
    ) {
        $carlConfigurationIntervention = $interventionServiceOffer->getGmaoConfiguration();
        if (is_null($carlConfigurationIntervention)) {
            throw new RuntimeException('Veuillez attacher une configuration GMAO à cette offre de service');
        }

        assert($carlConfigurationIntervention instanceof CarlConfigurationIntervention);
        $this->carlConfigurationIntervention = $carlConfigurationIntervention;
        $this->carlClient = $this->carlConfigurationIntervention->getCarlClient();
        if (is_null($this->carlClient)) {
            throw new RuntimeException('Veuillez configurer un client Carl pour cette offre de service');
        }
    }

    public function update(string $id, Intervention $entity, string $userEmail): Intervention
    {
        $intervention = $entity;
        $originalIntervention = $this->find($id);

        if (!is_null($intervention->getImageB64())) {
            throw new RuntimeException("Carl ne prend pas en charge les fichiers en base64. Il ne doit pas exister d'attribut 'imageB64' dans l'entité Intervention transmise à carl (merci de connecter une GED)");
        }

        $attributes = [];

        // Note: quand nous gèrerons plusieurs images, il faudra modifier l'appel et passer simplement le tableau
        $this->carlClient->updateUrlDocumentLinks(
            newAttachmentsUrls: is_null($intervention->getImageUrl()) ? [] : [$intervention->getImageUrl()],
            objectId: $id,
            entityJavaBean: 'com.carl.xnet.works.backend.bean.WOBean',
            docTypeId: $this->carlConfigurationIntervention->getPhotoAvantInterventionDoctypeId(),
            userEmail: $userEmail);

        $attributes[CarlClient::CARL_WO_MR_IMAGE_FIELD] = $intervention->getImageUrl();

        $attributes['workPriority'] = array_search($intervention->getPriority(), $this->carlClient->getPriorityMapping());
        $attributes['latitude'] = $intervention->getLatitude();
        $attributes['longitude'] = $intervention->getLongitude();
        $attributes['description'] = $intervention->getTitle();

        $relationships = [];

        $hasActionType = !is_null($intervention->getActionType());

        $relationships['actionType']['data'] = $hasActionType
            ? [
                'id' => $intervention->getActionType(),
                'type' => 'actiontype',
            ]
            : null;

        if (!empty($intervention->getDescription())) {
            if (!empty($originalIntervention->getDescription())) {
                $this->carlClient->patchCarlObject(
                    'description',
                    $this->carlClient->getCarlObject('wo', $id, ['longDesc'])
                        ->getRelationship('longDesc')
                        ->getId(),
                    ['description' => $intervention->getDescription()]
                );
            } else {
                $this->carlClient->addDescriptionToObjectRelationshipsArray($relationships, $intervention->getDescription());
            }
        } elseif (!empty($originalIntervention->getDescription())) {
            $this->carlClient->deleteObjectDescription($id, 'wo');
        }

        $this->carlClient->patchCarlObject('wo', $id, $attributes, $relationships);

        return $this->find($id);
    }

    public function find(mixed $id): Intervention
    {
        $sampleInterventionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return array_pop($sampleInterventionArray);
    }

    public function save(Intervention $entity): Intervention
    {
        $intervention = $entity;
        if (!empty($intervention->getImageB64())) {
            throw new RuntimeException("Carl ne prend pas en charge les fichiers en base64. Il ne doit pas exister d'attribut 'imageB64' dans l'entité Intervention transmise à carl (merci de connecter une GED)");
        }

        $userId = $this->carlClient
           ->getUserIdFromEmail($intervention->getUserEmail());

        $interventionArray = [
            'data' => [
                'type' => 'wo',
                'attributes' => [
                    'description' => $intervention->getTitle(),
                    'statusCode' => 'VALIDATE',
                    'latitude' => $intervention->getLatitude(),
                    'longitude' => $intervention->getLongitude(),
                    'workPriority' => array_search($intervention->getPriority(), $this->carlClient->getPriorityMapping()),
                ],
                'relationships' => [
                    'costCenter' => [
                        'data' => [
                            'id' => '15373b7a25f-b231', // todo récupérer dans la conf DAEEP_PROPRETE
                            'type' => 'costcenter',
                        ],
                    ],
                    'actionType' => [
                        'data' => [
                            'id' => $intervention->getActionType(),
                            'type' => 'actiontype',
                        ],
                    ],
                    'createdBy' => [
                        'data' => [
                            'id' => $userId,
                            'type' => 'actor',
                        ],
                    ],
                    'supervisor' => [
                        'data' => [
                            'id' => '180f2f264b3-35ff3', // todo récupérer dans la conf DAEEP_PROPRETE
                            'type' => 'actor',
                        ],
                    ],
                ],
            ],
        ];

        if (!is_null($intervention->getDescription())) {
            $this->carlClient->addDescriptionToObjectRelationshipsArray($interventionArray['data']['relationships'], $intervention->getDescription());
        }

        if (!is_null($intervention->getImageUrl())) {
            $interventionArray['data']['attributes'][CarlClient::CARL_WO_MR_IMAGE_FIELD] = $intervention->getImageUrl();
        }

        $createdInterventionApiResource = $this->carlClient->postCarlObject('wo', json_encode($interventionArray));

        if ($intervention->getImageUrl()) {
            $this->ajouterPhotoAvantIntervention(
                $intervention->getImageUrl(),
                $createdInterventionApiResource->getId(),
                $userId
            );
        }

        return $this->find($createdInterventionApiResource->getId());
    }

    public function getClassName()
    {
        return Intervention::class;
    }

    /**
     * @param array<string, mixed> $criteria :
     *                                       [interventionActionTypes] = Nature sur Carl. Seules les interventions
     *                                       qui ont une de ces natures seront retournée.
     *                                       Si vide, toutes les natures seront incluses.
     *                                       [directions]              = supervisor sur Carl. Seules les interventions
     *                                       dont la direction du demandeur correspond
     *                                       seront retournées.
     *                                       Si vide, toutes les directions seront incluses.
     *                                       [costCenters]             Seules les interventions
     *                                       dont le centre de cout correspond à une des valeur
     *                                       seront retournées.
     *                                       Si vide, tous les centres de coûts seront inclus.
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array {
        $criteria['directions'] = $this->carlConfigurationIntervention->getDirections();
        $criteria['costCenters'] = $this->carlConfigurationIntervention->getCostCenters();

        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }

        if (null !== $orderBy) {
            $this->throwIfUnexpectedOrderByValueCheckingTrait(
                $orderBy,
                [
                    'createdAt',
                    '-createdAt',
                    'updatedAt',
                    '-updatedAt',
                    'priority',
                    '-priority',
                ]
            );

            $orderBy = array_map(
                function ($orderByRaw) {
                    switch ($orderByRaw) {
                        case 'createdAt':
                            $value = 'createDate';
                            break;
                        case '-createdAt':
                            $value = '-createDate';
                            break;
                        case 'updatedAt':
                            $value = 'modifyDate';
                            break;
                        case '-updatedAt':
                            $value = '-modifyDate';
                            break;
                        case 'priority':
                            $value = 'workPriority';
                            break;
                        case '-priority':
                            $value = '-workPriority';
                            break;

                        default:
                            $value = $orderByRaw;
                            break;
                    }

                    return $value;
                },
                $orderBy);
        } else {
            $orderBy = [];
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'statuses',
                'interventionActionTypes',
                'directions',
                'costCenters',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $id = $propertyAccessor->getValue($criteria, '[id]');
        $statuses = $propertyAccessor->getValue($criteria, '[statuses]');
        $interventionActionTypes = $propertyAccessor->getValue($criteria, '[interventionActionTypes]');
        $directions = $propertyAccessor->getValue($criteria, '[directions]');
        $costCenters = $propertyAccessor->getValue($criteria, '[costCenters]');

        $filters = [];

        if (!empty($id)) {
            $filters['id'] = $id;
        }

        if (!empty($statuses)) {
            $filters['statusCode'] = $statuses;
        }

        if (!empty($interventionActionTypes)) {
            $filters['actionType.id'] = $interventionActionTypes;
        }
        if (!empty($directions)) {
            $filters['supervisor.code'] = $directions;
        }
        if (!empty($costCenters)) {
            $filters['costCenter.code'] = $costCenters;
        }

        $includes = [
            'actionType',
            'longDesc',
            'createdBy',
        ];

        $fields = [
            'wo' => [
                'code',
                'xtraTxt03',
                'xtraTxt10',
                CarlClient::CARL_WO_MR_IMAGE_FIELD,
                'xtraNum02',
                'createDate',
                'workPriority',
                'statusCode',
                'description',
                'latitude',
                'longitude',
                ],
            'actiontype' => ['id'],
            'description' => ['rawDescription'],
            'actor' => ['xtraTxt02', 'fullName'],
        ];

        $interventionResources = $this->carlClient->getCarlObjectsRaw(
            'wo',
            $filters,
            $includes,
            $fields,
            $orderBy,
        );

        /** @var array<Intervention> $interventions */
        $interventions = [];
        foreach ($interventionResources['data'] as $interventionResourceData) {
            try {
                $interventionResource = new JsonApiResource($interventionResourceData, $interventionResources['included']);
                $id = $interventionResource->getId();
                $code = $interventionResource->getAttribute('code');
                $creatorName = $interventionResource->getRelationship('createdBy')->getAttribute('fullName');
                $interventionStatusHistory = $this->getInterventionStatusHistory($id);

                $streetNumber = $interventionResource->getAttribute('xtraNum02');
                $streetNumberComplement = $interventionResource->getAttribute('xtraTxt03');
                $streetName = $interventionResource->getAttribute('xtraTxt10');
                $interventionAddress = "$streetNumber$streetNumberComplement $streetName";

                $hasLocalisation = $interventionResource->getAttribute('latitude') && $interventionResource->getAttribute('longitude');

                $intervention = (new Intervention())
                ->setPriority($this->carlClient->getPriorityMapping()[$interventionResource->getAttribute('workPriority')])
                ->setDescription($interventionResource->getRelationship('longDesc')?->getAttribute('rawDescription'))
                ->setId($id)
                ->setUserEmail($this->carlClient->getUserEmailFromId($interventionResource->getRelationship('createdBy')->getId()))
                ->setCreatedAt(new \DateTime($interventionResource->getAttribute('createDate')))
                ->setAddress($interventionAddress)
                ->setActionType($interventionResource->getRelationship('actionType')->getId())
                ->setCode($code)
                ->setCreatorName($creatorName)
                ->setStatusHistory($interventionStatusHistory)
                ->setImageUrl($interventionResource->getAttribute(CarlClient::CARL_WO_MR_IMAGE_FIELD))
                ->setTitle($interventionResource->getAttribute('description') ?? '');

                if ($hasLocalisation) {
                    $intervention
                        ->setLatitude($interventionResource->getAttribute('latitude'))
                        ->setLongitude($interventionResource->getAttribute('longitude'));
                }

                $interventions[] = $intervention;
            } catch (MultipleEmailsFoundForUser|NoEmailFoundForUser $e) {
                // l'intervention n'est pas traitable si l'on est pas capable de retrouver un email, on ne l'ajoute pas et on continue normalement
            }
        }

        foreach ($interventions as $intervention) {
            $imageUrl = $this->recupererPhotoAvantAIntervention($intervention->getId());

            if (!is_null($imageUrl)) {
                $intervention->setImageUrl($imageUrl);
            }
        }

        return $interventions;
    }

    private function ajouterPhotoAvantIntervention(string $imageUrl, string $interventionId, string $userId): void
    {
        $this->carlClient->addDocumentLinkToObject(
            $imageUrl,
            $interventionId,
            'com.carl.xnet.works.backend.bean.WOBean',
            $userId,
            $this->carlConfigurationIntervention->getPhotoAvantInterventionDoctypeId()
        );
    }

    private function recupererPhotoAvantAIntervention(string $interventionId): ?string
    {
        return $this->carlClient->getObjectDocumentUrl(
            $interventionId,
            $this->carlConfigurationIntervention->getPhotoAvantInterventionDoctypeId()
        );
    }

    /**
     * @return array<InterventionStatusHistorised>
     */
    public function getInterventionStatusHistory(
        string $interventionId
    ): array {
        $filters = [
            'origin.id' => [
                $interventionId,
            ],
            ];

        $includes = [
            'statusChangedBy',
        ];

        $fields = [
            'actor' => [
                'fullName',
            ],
            'wohistostatus' => [
                'statusChangedDate',
                'statusCode',
            ],
        ];

        $interventionStatusHistoryFromApi = $this->carlClient->getCarlObjectsRaw(
            'wohistostatus',
            $filters,
            $includes,
            $fields,
        );

        $interventionStatusHistory = [];

        foreach ($interventionStatusHistoryFromApi['data'] as $interventionStatusData) {
            $interventionStatusResource = new JsonApiResource(
                $interventionStatusData,
                $interventionStatusHistoryFromApi['included']
            );

            $interventionStatusFullName = $interventionStatusResource
            ->getRelationship('statusChangedBy')
            ?->getAttribute('fullName');

            $interventionStatusState = $interventionStatusResource
            ->getAttribute('statusCode');

            $interventionStatusChangedDate = $interventionStatusResource
            ->getAttribute('statusChangedDate');

            $interventionStatusHistory[] = new InterventionStatusHistorised(
                $interventionId,
                $interventionStatusState,
                $interventionStatusFullName,
                new \DateTime($interventionStatusChangedDate),
            );
        }

        return $interventionStatusHistory;
    }
}
