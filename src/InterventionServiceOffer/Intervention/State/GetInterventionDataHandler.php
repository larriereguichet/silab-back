<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Intervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

final class GetInterventionDataHandler implements ProviderInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<Intervention>|Intervention
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|Intervention
    {
        assert($operation instanceof Get);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $interventionRepository = new InterventionRepository(
            $interventionServiceOffer,
        );
        $interventionWithInternalImageUrl = $this->replaceObjectExternalImageUrlWithInternalImageUrl(
            $interventionServiceOffer,
            $interventionRepository->find($uriVariables['id'])
        );
        assert($interventionWithInternalImageUrl instanceof Intervention);

        return $interventionWithInternalImageUrl;
    }
}
