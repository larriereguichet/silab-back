<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Intervention;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

final class GetCollectionInterventionDataHandler implements ProviderInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<Intervention>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $interventionRepository = new InterventionRepository(
            $interventionServiceOffer,
        );

        $criteria = $context['filters'] ?? [];

        foreach ($criteria as $key => $value) {
            if (is_string($value)) {
                $criteria[$key] = [$value];
            }
        }

        if (isset($criteria['orderBy'])) {
            $orderBy = $criteria['orderBy'];
            unset($criteria['orderBy']);
        } else {
            $orderBy = null;
        }

        return $this->replaceInterventionsExternalImageUrlWithInternalImageUrl(
            $interventionServiceOffer,
            $interventionRepository->findBy($criteria, $orderBy)
        );
    }

    /**
     * @param array<Intervention> $interventions
     *
     * @return array<Intervention>
     */
    private function replaceInterventionsExternalImageUrlWithInternalImageUrl(InterventionServiceOffer $interventionServiceOffer, array $interventions): array
    {
        return array_map(
            function ($intervention) use ($interventionServiceOffer) {
                $interventionWithInternalImageUrl = $this->replaceObjectExternalImageUrlWithInternalImageUrl($interventionServiceOffer, $intervention);
                assert($interventionWithInternalImageUrl instanceof Intervention);

                return $interventionWithInternalImageUrl;
            },
            $interventions
        );
    }
}
