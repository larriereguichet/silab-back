<?php

namespace App\InterventionServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Intervention;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ObjectWithImage\Trait\ReplaceObjectExternalImageUrlWithInternalImageUrlTrait;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

final class CreateInterventionDataHandler implements ProcessorInterface
{
    use ReplaceObjectExternalImageUrlWithInternalImageUrlTrait
    ;

    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository,
        private Security $security
    ) {
    }

    /**
     * @param Intervention $data
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): Intervention {
        if (!($operation instanceof Post)) {
            throw new RuntimeException('Opération non supportée');
        }

        $currentUser = $this->security->getUser();
        assert($currentUser instanceof User);

        assert($data instanceof Intervention);
        $data->setUserEmail($currentUser->getEmail());

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $interventionRepository = new InterventionRepository(
            $interventionServiceOffer
        );

        $interventionWithInternalImageUrl = $this->replaceObjectExternalImageUrlWithInternalImageUrl(
            $interventionServiceOffer,
            $interventionRepository->save($data)
        );

        assert($interventionWithInternalImageUrl instanceof Intervention);

        return $interventionWithInternalImageUrl;
    }
}
