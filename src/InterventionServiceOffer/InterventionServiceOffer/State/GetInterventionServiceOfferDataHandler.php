<?php

namespace App\InterventionServiceOffer\InterventionServiceOffer\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;

class GetInterventionServiceOfferDataHandler implements ProviderInterface
{
    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): InterventionServiceOffer
    {
        assert($operation instanceof Get);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['id']);

        $equipementRepository = new EquipementRepository($interventionServiceOffer);

        $interventionServiceOffer->setAvailableEquipements(
            array_map(
                function ($equipementId) use ($equipementRepository) {
                    return $equipementRepository->find($equipementId);
                },
                $interventionServiceOffer->getAvailableEquipementsIds())
        );

        return $interventionServiceOffer;
    }
}
