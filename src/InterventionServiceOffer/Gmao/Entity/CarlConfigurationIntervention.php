<?php

namespace App\InterventionServiceOffer\Gmao\Entity;

use App\Entity\Intervention;
use App\InterventionServiceOffer\Gmao\Repository\CarlConfigurationInterventionRepository;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\ValueObject\CarlObject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarlConfigurationInterventionRepository::class)]
class CarlConfigurationIntervention extends GmaoConfigurationIntervention
{
    /** @var array<int,string> */
    #[ORM\Column(nullable: true)]
    private array $costCenters = [];

    /** @var array<int,string> */
    #[ORM\Column(nullable: true)]
    private array $directions = [];

    #[ORM\Column(type: 'json')]
    private mixed $actionTypesMap = [];

    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    private mixed $carlAttributesPresetForDemandeInterventionCreation = [];

    /** @var array<string,CarlObject> */
    #[ORM\Column(type: 'json', options: ['default' => '{}'])]
    private array $carlRelationshipsPresetForDemandeInterventionCreation = [];

    #[ORM\Column]
    private ?string $photoAvantInterventionDoctypeId = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?CarlClient $carlClient = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $woViewUrlSubPath = null;

    /**
     * La fonction de recherche d'enfants sera limitée aux types définis dans cette liste.
     *
     * @var array<int,string>
     */
    #[ORM\Column(nullable: true, options: ['default' => '["building", "zone"]'])]
    private array $includeEquipementsChildrenWithType = [];

    /** @return array<string>|null */
    public function getCostCenters(): ?array
    {
        return $this->costCenters;
    }

    /**
     * @param array<string>|null $costCenters
     */
    public function setCostCenters(?array $costCenters): self
    {
        $this->costCenters = $costCenters;

        return $this;
    }

    /** @return array<string>|null */
    public function getDirections(): ?array
    {
        return $this->directions;
    }

    /**
     * @param array<string>|null $directions
     */
    public function setDirections(?array $directions): self
    {
        $this->directions = $directions;

        return $this;
    }

    /**
     * @return array<string,string>
     */
    public function getActionTypesMap(): array
    {
        return json_decode(json_encode($this->actionTypesMap), true);
    }

    /**
     * @param array<string,string> $actionTypesMap
     */
    public function setActionTypesMap(array $actionTypesMap): self
    {
        $this->actionTypesMap = $actionTypesMap;

        return $this;
    }

    /**
     * @return array<string,string>
     */
    public function getCarlAttributesPresetForDemandeInterventionCreation(): array
    {
        return json_decode(json_encode($this->carlAttributesPresetForDemandeInterventionCreation), true);
    }

    /**
     * @param array<string,string> $carlAttributesPresetForDemandeInterventionCreation
     */
    public function setCarlAttributesPresetForDemandeInterventionCreation(array $carlAttributesPresetForDemandeInterventionCreation): self
    {
        $this->carlAttributesPresetForDemandeInterventionCreation = $carlAttributesPresetForDemandeInterventionCreation;

        return $this;
    }

    /**
     * @return array<string,CarlObject>
     */
    public function getCarlRelationshipsPresetForDemandeInterventionCreation(): array
    {
        // QUESTION_TECHNIQUE Comment faire pour que notre denormalizer soit utilisé automatiquement,  cf : CarlObjectDenormalizer
        return array_map(
            function ($relationshipPresetArray) {
                return CarlObject::fromArray($relationshipPresetArray);
            },
            json_decode(json_encode($this->carlRelationshipsPresetForDemandeInterventionCreation), true)
        );
    }

    /**
     * @return array<string,array<string,array<string,string>>>
     */
    public function getApiFormatedCarlRelationshipsPresetForDemandeInterventionCreation(): array
    {
        return array_map(
            function ($carlObject) {
                return $carlObject->toApiRelationship();
            },
            $this->getCarlRelationshipsPresetForDemandeInterventionCreation()
        );
    }

    /**
     * @param array<string,CarlObject> $carlRelationshipsPresetForDemandeInterventionCreation
     */
    public function setCarlRelationshipsPresetForDemandeInterventionCreation(array $carlRelationshipsPresetForDemandeInterventionCreation): self
    {
        $this->carlRelationshipsPresetForDemandeInterventionCreation = $carlRelationshipsPresetForDemandeInterventionCreation;

        return $this;
    }

    public function getPhotoAvantInterventionDoctypeId(): string
    {
        return $this->photoAvantInterventionDoctypeId;
    }

    public function setPhotoAvantInterventionDoctypeId(string $photoAvantInterventionDoctypeId): self
    {
        $this->photoAvantInterventionDoctypeId = $photoAvantInterventionDoctypeId;

        return $this;
    }

    public function getCarlClient(): ?CarlClient
    {
        return $this->carlClient;
    }

    public function setCarlClient(?CarlClient $carlClient): self
    {
        $this->carlClient = $carlClient;

        return $this;
    }

    public function getWoViewUrlSubPath(): ?string
    {
        return $this->woViewUrlSubPath;
    }

    public function setWoViewUrlSubPath(?string $woViewUrlSubPath): static
    {
        $this->woViewUrlSubPath = $woViewUrlSubPath;

        return $this;
    }

    public function getInstanceTitle(): ?string
    {
        return $this->getCarlClient()?->getTitle();
    }

    public function getInterventionViewUrl(Intervention $intervention): ?string
    {
        return $this->carlClient->getBaseUrl()
                .$this->getWoViewUrlSubPath()
                .'&BEAN_ID='
                .$intervention->getId()
        ;
    }

    /** @return array<string>|null */
    public function getIncludeEquipementsChildrenWithType(): ?array
    {
        return array_values($this->includeEquipementsChildrenWithType);
    }

    /**
     * @param array<string>|null $includedChildrenTypes
     */
    public function setIncludeEquipementsChildrenWithType(?array $includedChildrenTypes): self
    {
        $this->includeEquipementsChildrenWithType = array_values($includedChildrenTypes);

        return $this;
    }
}
