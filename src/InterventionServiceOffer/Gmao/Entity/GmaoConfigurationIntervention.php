<?php

namespace App\InterventionServiceOffer\Gmao\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Entity\Intervention;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\Gmao\Repository\GmaoConfigurationInterventionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get(
    security: "is_granted('ROLE_SERVICEOFFER_CONSULTER_GMAO_CONFIGURATION_INTERVENTIONS')"
)]
#[GetCollection(
    security: "is_granted('ROLE_SERVICEOFFER_CONSULTER_GMAO_CONFIGURATION_INTERVENTIONS')"
)]
#[ORM\Entity(repositoryClass: GmaoConfigurationInterventionRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'carl' => CarlConfigurationIntervention::class,
])]
abstract class GmaoConfigurationIntervention
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ORM\JoinColumn(nullable: false)]
    protected ?string $title = null;

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return array<string,string> un tableau associatif du type "id de la nature"=>"libellé de la nature"
     */
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    abstract public function getActionTypesMap(): array;

    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    abstract public function getInstanceTitle(): ?string;

    /**
     * Si la Gmao employé possède une interface pour visualiser les interventions, il faut surcharger cette méthode dans sont client spécifique.
     */
    public function getInterventionViewUrl(Intervention $intervention): ?string
    {
        return null;
    }
}
