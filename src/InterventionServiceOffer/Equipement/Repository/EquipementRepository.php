<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\Equipement\Entity\Equipement;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;

class EquipementRepository implements EquipementRepositoryInterface
{
    private EquipementRepositoryInterface $specificEquipementRepository;

    public function __construct(
        protected InterventionServiceOffer $interventionServiceOffer
    ) {
        try {
            $this->specificEquipementRepository = match ($interventionServiceOffer->getGmaoConfiguration()::class) {
                CarlConfigurationIntervention::class => new CarlEquipementRepository(
                    $interventionServiceOffer
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$interventionServiceOffer->getGmaoConfiguration()::class);
        }
    }

    public function find($id): Equipement
    {
        return $this->specificEquipementRepository->find($id);
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findOneBy(array $criteria): Equipement
    {
        throw new RuntimeException('methode non implémentée');
    }

    /**
     * @return array<Equipement>
     */
    public function findChildren(string $parentId): array
    {
        $childEquipements = $this->specificEquipementRepository->findChildren($parentId);
        // Extrait les labels des équipements
        $labels = array_map(fn ($equipement) => $equipement->getLabel(), $childEquipements);

        // Trie les labels et maintient l'association avec les équipements
        array_multisort($labels, SORT_ASC | SORT_NATURAL | SORT_FLAG_CASE, $childEquipements);

        return $childEquipements;
    }

    public function getClassName()
    {
        return Equipement::class;
    }
}
