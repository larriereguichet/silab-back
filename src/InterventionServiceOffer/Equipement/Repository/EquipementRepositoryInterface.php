<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use App\InterventionServiceOffer\Equipement\Entity\Equipement;
use Doctrine\Persistence\ObjectRepository;

interface EquipementRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): Equipement;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<Equipement>
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    /**
     * @return array<Equipement>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Equipement;

    /**
     * Retourne les équipement descendants de cet équipement.
     * Note: cette fonction ne se limite pas forcément au enfants directs, ce choix est laissé au connecteur.
     *
     * @return array<Equipement>
     */
    public function findChildren(string $parentId): array;
}
