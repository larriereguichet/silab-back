<?php

namespace App\InterventionServiceOffer\Equipement\Repository;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\Equipement\Entity\Equipement;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Service\JsonApiResource;
use App\Service\UnexpectedCriteriaCheckingTrait;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\ValueObject\CarlEqptIdAndType;
use App\Shared\Location\Entity\Coordinates;

class CarlEquipementRepository implements EquipementRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    private CarlConfigurationIntervention $carlConfigurationIntervention;
    private CarlClient $carlClient;

    public function __construct(
        protected InterventionServiceOffer $interventionServiceOffer
    ) {
        $carlConfigurationIntervention = $interventionServiceOffer->getGmaoConfiguration();
        if (is_null($carlConfigurationIntervention)) {
            throw new RuntimeException('Veuillez attacher une configuration GMAO à cette offre de service');
        }

        assert($carlConfigurationIntervention instanceof CarlConfigurationIntervention);
        $this->carlConfigurationIntervention = $carlConfigurationIntervention;
        $this->carlClient = $this->carlConfigurationIntervention->getCarlClient();
        if (is_null($this->carlClient)) {
            throw new RuntimeException('Veuillez configurer un client Carl pour cette offre de service');
        }
    }

    public function find($id): Equipement
    {
        $carlEquipement = CarlEqptIdAndType::fromJson($id);
        $equipementApiResponse = $this->carlClient->getCarlObject(
            entity: $carlEquipement->getType(),
            id: $carlEquipement->getId(),
            fields: [$carlEquipement->getType() => ['description', 'latitude', 'longitude']]
        );

        return $this->createEquipementFromJsonApiResource($equipementApiResponse);
    }

    /**
     * @return array<Equipement>
     */
    public function findAll(): array
    {
        throw new RuntimeException('methode non implémenté');
    }

    /**
     * @return array<Equipement>
     */
    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        throw new RuntimeException('methode non implémenté');
    }

    public function findOneBy(array $criteria): Equipement
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findChildren(string $parentId): array
    {
        // Note, on ne peut pas filtrer sur le type d'enfants
        $linkEquipementsJsonApiResource = $this->carlClient->getCarlObjectsRaw(
            entity: 'linkequipment',
            filters: ['parent.id' => [CarlEqptIdAndType::fromJson($parentId)->getId()]],// , 'child.type' => ['buildingset', 'building']
            includes: ['child']
        );

        /** @var array<string,Equipement> $children */
        $children = [];
        $grandChildren = [];

        foreach ($linkEquipementsJsonApiResource['data'] as $linkEquipementsJsonApiResourceData) {
            $linkEquipmentJsonApiResource = new JsonApiResource($linkEquipementsJsonApiResourceData, $linkEquipementsJsonApiResource['included']);
            $childEquipmentJsonApiResource = $linkEquipmentJsonApiResource->getRelationship('child');

            if (!is_null($childEquipmentJsonApiResource)) {
                $childEquipementTypeIsIncludedInServiceOffer =
                    in_array(
                        $childEquipmentJsonApiResource->getType(),
                        $this->carlConfigurationIntervention->getIncludeEquipementsChildrenWithType()
                    );
                // si l'équipement actuel est du bon type, il s'ajoute à la liste
                if ($childEquipementTypeIsIncludedInServiceOffer) {
                    // Note, je référence par id pour éviter les doublons
                    $children[$childEquipmentJsonApiResource->getId()] = $this->createEquipementFromJsonApiResource($childEquipmentJsonApiResource);
                }
                // dans tous les cas, on propage la recherche sur les enfants
                $grandChildren = array_merge(
                    $grandChildren,
                    $this->findChildren(
                        (new CarlEqptIdAndType(
                            $childEquipmentJsonApiResource->getId(),
                            $childEquipmentJsonApiResource->getType()
                        ))->getSilabId()
                    )
                );
            }
        }

        return array_merge(array_values($children), $grandChildren);
    }

    public function getClassName(): string
    {
        return Equipement::class;
    }

    private function createEquipementFromJsonApiResource(JsonApiResource $equipmentJsonApiResource): Equipement
    {
        return new Equipement(
            id: (new CarlEqptIdAndType($equipmentJsonApiResource->getId(), $equipmentJsonApiResource->getType()))->getSilabId(),
            label: $equipmentJsonApiResource->getAttribute('description'),
            coordinates: Coordinates::fromUnsafeLatLng(
                $equipmentJsonApiResource->getAttribute('latitude'),
                $equipmentJsonApiResource->getAttribute('longitude')
            )
        );
    }
}
