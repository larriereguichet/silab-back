<?php

namespace App\InterventionServiceOffer\Equipement\Exception;

use App\Exception\RuntimeException;

class MalformedEquipementJsonException extends RuntimeException
{
}
