<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Equipement\Entity\Equipement;
use App\InterventionServiceOffer\Equipement\Entity\EquipementApiRessource;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final class GetAllEquipementChildrenDataHandler implements ProviderInterface
{
    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<EquipementApiRessource>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $equipementRepository = new EquipementRepository($interventionServiceOffer);

        $lOffreDeServicePorteSurDesEquipements = !empty($interventionServiceOffer->getAvailableEquipementsIds());
        if ($lOffreDeServicePorteSurDesEquipements) {
            $LEquipementNEstPasDéfinitDansCetteOds = !in_array(
                $uriVariables['id'],
                $interventionServiceOffer->getAvailableEquipementsIds()
            );
            if ($LEquipementNEstPasDéfinitDansCetteOds) {
                throw new AccessDeniedException("L'équipement dont vous voulez récupérer les enfants ne concerne pas votre offre de service.");
            }
        }

        return Equipement::toApiResources($equipementRepository->findChildren($uriVariables['id']));
    }
}
