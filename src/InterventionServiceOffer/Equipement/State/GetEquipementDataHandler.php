<?php

namespace App\InterventionServiceOffer\Equipement\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\InterventionServiceOffer\Equipement\Entity\EquipementApiRessource;
use App\InterventionServiceOffer\Equipement\Repository\EquipementRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;

final class GetEquipementDataHandler implements ProviderInterface
{
    public function __construct(
        private InterventionServiceOfferRepository $interventionServiceOfferRepository
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): EquipementApiRessource
    {
        assert($operation instanceof Get);

        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $equipementRepository = new EquipementRepository($interventionServiceOffer);

        return $equipementRepository->find($uriVariables['id'])->toApiRessource();
    }
}
