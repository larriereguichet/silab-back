<?php

namespace App\InterventionServiceOffer\Equipement\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\Equipement\Exception\MalformedEquipementJsonException;
use App\InterventionServiceOffer\Equipement\State\GetAllEquipementChildrenDataHandler;
use App\InterventionServiceOffer\Equipement\State\GetEquipementDataHandler;
use App\Shared\Location\Entity\Coordinates;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get()]
#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/equipements/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CONSULTER_EQUIPEMENT')",
    provider: GetEquipementDataHandler::class,
    normalizationContext: ['groups' => [EquipementApiRessource::GROUP_AFFICHER_EQUIPEMENT]],
)]
#[GetCollection(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/equipements/{id}/children',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CONSULTER_EQUIPEMENT')",
    provider: GetAllEquipementChildrenDataHandler::class,
    normalizationContext: ['groups' => [EquipementApiRessource::GROUP_AFFICHER_EQUIPEMENT]]
)]
class EquipementApiRessource
{
    public const GROUP_AFFICHER_EQUIPEMENT = 'Afficher détails demande d\'équipement';

    public function __construct(
        #[Groups([EquipementApiRessource::GROUP_AFFICHER_EQUIPEMENT, DemandeIntervention::GROUP_CREER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_MODIFIER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private ?string $id = null,
        #[Groups([EquipementApiRessource::GROUP_AFFICHER_EQUIPEMENT, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private ?string $label = null,
        #[Groups([EquipementApiRessource::GROUP_AFFICHER_EQUIPEMENT, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
        private ?Coordinates $coordinates = null
    ) {
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public static function fromJson(string $equipementJson): self
    {
        $equipementArray = json_decode($equipementJson, true);

        if (
            is_null($equipementArray)
        ) {
            throw new MalformedEquipementJsonException('Le json envoyé n\'est pas décodable. Attendu : {"id":"id de l\'objet", "label":"nom de l\'equipement"}. Chaîne reçue : '.$equipementJson);
        }

        return self::fromArray($equipementArray);
    }

    /**
     * @param array<string,string> $equipementArray
     */
    public static function fromArray(array $equipementArray): self
    {
        if (
            !array_key_exists('id', $equipementArray)
            || !array_key_exists('label', $equipementArray)
        ) {
            throw new MalformedEquipementJsonException('Le tableau envoyé n\'est pas convertible en instance de Equipement.');
        }

        return new EquipementApiRessource($equipementArray['id'], $equipementArray['label']);
    }

    public function __toString()
    {
        return $this->getLabel();
    }
}
