<?php

namespace App\InterventionServiceOffer\Equipement\Entity;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\Equipement\Exception\MalformedEquipementJsonException;
use App\Shared\Location\Entity\Coordinates;
use Symfony\Component\Serializer\Annotation\Groups;

class Equipement
{
    public function __construct(
        #[Groups([DemandeIntervention::GROUP_CREER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_MODIFIER_DEMANDE_INTERVENTION, DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private ?string $id = null,
        #[Groups([DemandeIntervention::GROUP_AFFICHER_DETAILS_DEMANDE_INTERVENTION, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, DemandeIntervention::GROUP_LISTER_DEMANDES_INTERVENTIONS])]
        private ?string $label = null,
        #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
        private ?Coordinates $coordinates = null
    ) {
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public static function fromJson(string $equipementJson): self
    {
        $equipementArray = json_decode($equipementJson, true);

        if (
            is_null($equipementArray)
        ) {
            throw new MalformedEquipementJsonException('Le json envoyé n\'est pas décodable. Attendu : {"id":"id de l\'objet", "label":"nom de l\'equipement"}. Chaîne reçue : '.$equipementJson);
        }

        return self::fromArray($equipementArray);
    }

    /**
     * @param array<string,string> $equipementArray
     */
    public static function fromArray(array $equipementArray): self
    {
        if (
            !array_key_exists('id', $equipementArray)
            || !array_key_exists('label', $equipementArray)
        ) {
            throw new MalformedEquipementJsonException('Le tableau envoyé n\'est pas convertible en instance de Equipement.');
        }

        return new Equipement($equipementArray['id'], $equipementArray['label']);
    }

    public function __toString()
    {
        return $this->getLabel();
    }

    public function toApiRessource(): EquipementApiRessource
    {
        return new EquipementApiRessource(
            id: $this->getId(),
            label: $this->getLabel(),
            coordinates: $this->getCoordinates()
        );
    }

    /**
     * @param array<Equipement> $equipements
     *
     * @return array<EquipementApiRessource>
     */
    public static function toApiResources(array $equipements): array
    {
        return array_map(
            function ($equipement) {
                return new EquipementApiRessource(
                    id: $equipement->getId(),
                    label: $equipement->getLabel(),
                    coordinates: $equipement->getCoordinates()
                );
            },
            $equipements
        );
    }
}
