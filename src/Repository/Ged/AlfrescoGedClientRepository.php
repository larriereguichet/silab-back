<?php

namespace App\Repository\Ged;

use App\Entity\Ged\AlfrescoGedClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AlfrescoGedClient>
 *
 * @method AlfrescoGedClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlfrescoGedClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlfrescoGedClient[]    findAll()
 * @method AlfrescoGedClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlfrescoGedClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlfrescoGedClient::class);
    }

    public function save(AlfrescoGedClient $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AlfrescoGedClient $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return AlfrescoGedClient[] Returns an array of AlfrescoGedClient objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?AlfrescoGedClient
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
