<?php

namespace App\Repository\Intervention;

use App\Entity\Intervention;
use Doctrine\Persistence\ObjectRepository;

interface InterventionRepositoryInterface extends ObjectRepository
{
    /**
     * @param mixed $id On attend un InterventionId
     *                  QUESTION_TECHNIQUE: les repos doctrine prefère renvoyer du nullable plutot que throw une exception, est-ce une pratique à suivre ?
     */
    public function find(mixed $id): Intervention;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<Intervention>
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    /**
     * @return array<Intervention>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Intervention;

    public function save(Intervention $entity): Intervention;

    public function update(string $id, Intervention $entity, string $userEmail): Intervention;
}
