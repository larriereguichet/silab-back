<?php

namespace App\Repository\InterventionStatus;

use App\Entity\InterventionStatus;

interface InterventionStatusRepositoryInterface
{
    public function save(InterventionStatus $entity): void;

    public function remove(InterventionStatus $entity): void;
}
