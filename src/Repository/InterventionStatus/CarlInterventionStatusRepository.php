<?php

namespace App\Repository\InterventionStatus;

use App\Entity\InterventionStatus;
use App\Shared\Carl\Entity\CarlClient;

final class CarlInterventionStatusRepository implements InterventionStatusRepositoryInterface
{
    public function __construct(
        private CarlClient $carlClient,
    ) {
    }

    public function save(InterventionStatus $entity): void
    {
        $this->carlClient->transitionToStatus('wo', $entity->getInterventionId(), $entity->getLabel(), CarlClient::INTERVENTION_STATUS_WORKFLOW);
    }

    /**
     * Cette méthode n'est pas implémenté.
     */
    public function remove(InterventionStatus $entity): void
    {
    }
}
