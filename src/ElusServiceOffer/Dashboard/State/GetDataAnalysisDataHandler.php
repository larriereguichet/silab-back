<?php

namespace App\ElusServiceOffer\Dashboard\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\ElusServiceOffer\Dashboard\Entity\DataAnalysis;
use App\ElusServiceOffer\Dashboard\Repository\DataAnalysisRepository;
use App\ElusServiceOffer\ElusServiceOffer\Repository\ElusServiceOfferRepository;
use App\Service\MetabaseClient;

final class GetDataAnalysisDataHandler implements ProviderInterface
{
    public function __construct(
        private ElusServiceOfferRepository $elusServiceOfferRepository,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): DataAnalysis
    {
        assert($operation instanceof Get);

        $metabaseClient = new MetabaseClient($this->elusServiceOfferRepository->find($uriVariables['serviceOfferId']));

        $dataAnalysisRepository = new DataAnalysisRepository($metabaseClient);

        return $dataAnalysisRepository->find($uriVariables['id']);
    }
}
