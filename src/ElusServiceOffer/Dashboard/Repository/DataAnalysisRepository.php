<?php

namespace App\ElusServiceOffer\Dashboard\Repository;

use App\ElusServiceOffer\Dashboard\Entity\DataAnalysis;
use App\Service\MetabaseClient;

class DataAnalysisRepository
{
    public function __construct(private MetabaseClient $metabaseClient)
    {
    }

    public function find(int $id): DataAnalysis
    {
        $url = $this->metabaseClient->getDashboardIframeUrl($id, expirationMinutes: 48 * 60);

        return new DataAnalysis($id, $url);
    }
}
