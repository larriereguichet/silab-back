<?php

namespace App\ElusServiceOffer\Intervention\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\GetCollection;
use App\ElusServiceOffer\Intervention\State\GetCollectionInterventionDataHandler;
use App\Filter\SearchFilter;
use App\Shared\Location\Entity\Coordinates;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    uriTemplate: '/elus-service-offers/{serviceOfferId}/interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_ELUS_CONSULTER_INTERVENTIONS')",
    provider: GetCollectionInterventionDataHandler::class,
    normalizationContext: ['groups' => [self::GROUP_LISTER_INTERVENTIONS]]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'beginStatusChangedAt' => 'exact',
        'beginCreatededAt' => 'exact',
        'endStatusChangedAt' => 'exact',
        'endCreatededAt' => 'exact',
        'statuses' => 'exact',
        'regroupment' => 'exact',
    ]
)]
class Intervention
{
    public const GROUP_LISTER_INTERVENTIONS = 'Lister les interventions';

    public function __construct(
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $id = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $code = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $title = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $currentStatus = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $regroupment = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?Coordinates $coordinates = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?string $address = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?\DateTime $createdAt = null,
        #[Groups([self::GROUP_LISTER_INTERVENTIONS])]
        private ?\DateTime $statusChangedAt = null
    ) {
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setcode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCurrentStatus(): ?string
    {
        return $this->currentStatus;
    }

    public function setCurrentStatus(string $currentStatus): self
    {
        $this->currentStatus = $currentStatus;

        return $this;
    }

    public function getRegroupment(): ?string
    {
        return $this->regroupment;
    }

    public function setRegroupment(string $regroupment): self
    {
        $this->regroupment = $regroupment;

        return $this;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public function setCoordinates(?Coordinates $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setStatusChangedAt(\DateTime $statusChangedAt): self
    {
        $this->statusChangedAt = $statusChangedAt;

        return $this;
    }

    public function getStatusChangedAt(): ?\DateTime
    {
        return $this->statusChangedAt;
    }
}
