<?php

namespace App\ElusServiceOffer\Intervention\Repository;

use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use App\ElusServiceOffer\Intervention\Entity\Intervention;
use App\ElusServiceOffer\Shared\Oracle\OraclePDOFactory;
use App\Exception\RuntimeException;
use App\Shared\Location\Entity\Coordinates;
use Symfony\Component\PropertyAccess\PropertyAccess;

class InterventionRepository
{
    private \PDO $datamartPDO;
    private string $oracleSchema;

    private const MAX_NB_INTERVENTIONS_FETCHED = 5000;

    public function __construct(
        private ElusServiceOffer $elusServiceOffer
    ) {
        $this->datamartPDO = OraclePDOFactory::fromElusServiceOffer($elusServiceOffer);
        $this->oracleSchema = $elusServiceOffer->getConfiguration()['database']['tables']['intervention']['schema'];
    }

    /**
     * @param array<string,mixed> $criteria
     *
     * @return array<Intervention>
     */
    public function findAll(array $criteria): array
    {
        $conditions = [];
        $conditionsParameters = [];

        $conditions[] = "TITLE IS NOT NULL AND STATUS IS NOT NULL AND IS_CURRENT_STATUS = 'TRUE'";
        $conditions[] = 'LATITUDE IS NOT NULL AND LONGITUDE IS NOT NULL';

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $beginCreatedAt = $propertyAccessor->getValue($criteria, '[beginCreatedAt]');
        $endCreatedAt = $propertyAccessor->getValue($criteria, '[endCreatedAt]');
        $beginStatusChangedAt = $propertyAccessor->getValue($criteria, '[beginStatusChangedAt]');
        $endStatusChangedAt = $propertyAccessor->getValue($criteria, '[endStatusChangedAt]');
        $statuses = $propertyAccessor->getValue($criteria, '[statuses]');
        $regroupment = $propertyAccessor->getValue($criteria, '[regroupment]');
        $cities = $propertyAccessor->getValue($criteria, '[city]');

        if (!empty($beginCreatedAt)) {
            $conditionsParameters['beginCreatedAt'] = $beginCreatedAt;
            $conditions[] = "CREATED_AT > TO_DATE(:beginCreatedAt,'yy-mm-dd')";
        }

        if (!empty($endCreatedAt)) {
            $conditionsParameters['endCreatedAt'] = $endCreatedAt;
            $conditions[] = "CREATED_AT < TO_DATE(:endCreatedAt,'yy-mm-dd')";
        }

        if (!empty($beginStatusChangedAt)) {
            $conditionsParameters['beginStatusChangedAt'] = $beginStatusChangedAt;
            $conditions[] = "STATUS_CHANGED_AT > TO_DATE(:beginStatusChangedAt,'yy-mm-dd')";
        }

        if (!empty($endStatusChangedAt)) {
            $conditionsParameters['endStatusChangedAt'] = $endStatusChangedAt;
            $conditions[] = "STATUS_CHANGED_AT < TO_DATE(:endStatusChangedAt,'yy-mm-dd')";
        }

        if (!empty($statuses)) {
            $statusesClausesNbInValues = 0;
            $inValues = [];
            foreach ($statuses as $status) {
                $conditionsParameters["status$statusesClausesNbInValues"] = $status;
                $inValues[] = ":status$statusesClausesNbInValues";
                ++$statusesClausesNbInValues;
            }
            $inValuesSql = implode(',', $inValues);
            $conditions[] = "STATUS IN ($inValuesSql)";
        }

        if (!empty($regroupment)) {
            $conditionsParameters['regroupment'] = $regroupment;
            $conditions[] = 'REGROUPMENT = :regroupment';
        }

        if (!empty($cities)) {
            $cityConditions = [];
            foreach ($cities as $cityIndex => $city) {
                $cityParamName = "city$cityIndex";
                $conditionsParameters[$cityParamName] = $city;
                $cityConditions[] = "CITY = :$cityParamName";
            }
            $conditions[] = '('.implode(' OR ', $cityConditions).')';
        }

        $interventionTableName = $this->oracleSchema.'.'.$this->elusServiceOffer->getConfiguration()['database']['tables']['intervention']['name'];

        $whereConditions = ' WHERE '.implode(' AND ', $conditions);

        $request = "SELECT ID_CARL_WO, CODE, TITLE, STATUS, REGROUPMENT, TO_CHAR(CREATED_AT, 'yyyy-mm-dd\"T\"hh24:mi:ss\"Z\"') as CREATED_AT, TO_CHAR(STATUS_CHANGED_AT, 'yyyy-mm-dd\"T\"hh24:mi:ss\"Z\"') as STATUS_CHANGED_AT, LATITUDE, LONGITUDE, CITY FROM $interventionTableName $whereConditions ORDER BY created_at DESC OFFSET 0 ROWS FETCH NEXT ".self::MAX_NB_INTERVENTIONS_FETCHED.' ROWS ONLY';

        $request = $this->datamartPDO->prepare($request);
        $request->execute($conditionsParameters);
        $interventionDatas = $request->fetchAll(\PDO::FETCH_ASSOC);
        $interventions = [];
        foreach ($interventionDatas as $interventionData) {
            $interventions[] = $this->responseDataToIntervention($interventionData);
        }

        return $interventions;
    }

    /**
     * @return array<Intervention>
     */
    public function findBy(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    /**
     * Undocumented function.
     *
     * @param array<string,string> $interventionData
     */
    private function responseDataToIntervention(array $interventionData): Intervention
    {
        return (new Intervention())
            ->setId($interventionData['ID_CARL_WO'])
            ->setCode($interventionData['CODE'])
            ->setTitle($interventionData['TITLE'])
            ->setCurrentStatus($interventionData['STATUS'])
            ->setRegroupment($interventionData['REGROUPMENT'])
            ->setCreatedAt(\DateTime::createFromFormat(\DateTimeInterface::ATOM, $interventionData['CREATED_AT']))
            ->setStatusChangedAt(\DateTime::createFromFormat(\DateTimeInterface::ATOM, $interventionData['STATUS_CHANGED_AT']))
            ->setCoordinates(
                Coordinates::fromUnsafeLatLng(
                    $this->stringToFloat($interventionData['LATITUDE']),
                    $this->stringToFloat($interventionData['LONGITUDE'])
                )
            )
            ->setAddress($interventionData['CITY'])
        ;
    }

    private function stringToFloat(?string $string, string $stringDecimalSeparator = ','): ?float
    {
        if (is_null($string)) {
            return null;
        }

        if ('.' !== $stringDecimalSeparator) {
            $string = str_replace(
                $stringDecimalSeparator,
                '.',
                $string
            );
        }

        return floatval($string);
    }

    /**
     * @return array<string>
     */
    public function getAvailableCities(): array
    {
        $interventionTableName = $this->oracleSchema.'.'.$this->elusServiceOffer->getConfiguration()['database']['tables']['intervention']['name'];
        $request = "select distinct(city) FROM $interventionTableName";

        $request = $this->datamartPDO->prepare($request);
        $request->execute();

        return $request->fetchAll(\PDO::FETCH_COLUMN);
    }
}
