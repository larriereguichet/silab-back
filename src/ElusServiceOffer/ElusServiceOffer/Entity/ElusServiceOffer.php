<?php

namespace App\ElusServiceOffer\ElusServiceOffer\Entity;

use App\ElusServiceOffer\ElusServiceOffer\Repository\ElusServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ElusServiceOfferRepository::class)]
class ElusServiceOffer extends ServiceOffer
{
    /** @var array<string,mixed>|null $configuration */
    #[ORM\Column(nullable: true)]
    private ?array $configuration = null;

    /**
     * @return array<string,mixed>|null
     */
    public function getConfiguration(): ?array
    {
        return $this->configuration;
    }

    /**
     * @param array<string,mixed>|null $configuration
     */
    public function setConfiguration(?array $configuration): self
    {
        $this->configuration = $configuration;

        return $this;
    }

    public function getTemplate(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array<string,string>
     */
    public function getAvailableRoles(): array
    {
        $availableRoles = [
            'SERVICEOFFER_'.$this->getId().'_ROLE_ELUS_ELU' => 'Elu',
        ];

        // On propose dynamiquement un rôle pour chaque territoire disponible
        foreach ($this->getTerritoires() as $territoire) {
            $availableRoles['SERVICEOFFER_'.$this->getId().'_ROLE_ELUS_SUPERVISER_TERRITOIRE_'.$territoire] = "Territoire : $territoire";
        }

        return array_merge(parent::getAvailableRoles(), $availableRoles);
    }

    /**
     * @return array<string>
     */
    public function getTerritoires(): array
    {
        return $this->getConfiguration()['territoires'];
    }
}
