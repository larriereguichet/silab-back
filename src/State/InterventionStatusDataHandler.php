<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\InterventionStatus;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\Repository\InterventionStatus\CarlInterventionStatusRepository;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\Shared\Exception\InsufficientRolesException;

class InterventionStatusDataHandler implements ProcessorInterface
{
    public function __construct(private InterventionServiceOfferRepository $interventionServiceOfferRepository)
    {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): InterventionStatus {
        if (!($operation instanceof Post)) {
            throw new RuntimeException('Opération non supportée');
        }

        $serviceOfferId = $uriVariables['serviceOfferId'];

        // On vérifie que l'utilisateur peut bien récupérer l'intervention visée via l'ODS indiqué.
        $interventionServiceOffer = $this->interventionServiceOfferRepository->find($serviceOfferId);
        $interventionRepository = new InterventionRepository(
            $interventionServiceOffer,
        );

        assert($data instanceof InterventionStatus);
        $data->setInterventionId($uriVariables['interventionId']);
        try {
            $interventionRepository->find($data->getInterventionId());
        } catch (RuntimeException) {
            throw new InsufficientRolesException("Vous n'avez pas les droits suffisants pour changer le statut de cette intervention");
        }

        $gmaoConfiguration = $interventionServiceOffer->getGmaoConfiguration();
        assert($gmaoConfiguration instanceof CarlConfigurationIntervention);

        $interventionStatusRepository = new CarlInterventionStatusRepository(
            $gmaoConfiguration->getCarlClient(),
        );
        $interventionStatusRepository->save($data);

        $newCurrentStatusHistorised = $interventionRepository->find($data->getInterventionId())->getCurrentStatus();

        return (new InterventionStatus($newCurrentStatusHistorised->getLabel()))
            ->setCreatedAt($newCurrentStatusHistorised->getCreatedAt())
            ->setCreatedBy($newCurrentStatusHistorised->getCreatedBy())
            ->setInterventionId($newCurrentStatusHistorised->getInterventionId());
    }
}
