<?php

namespace App\Form\Type;

use EasyCorp\Bundle\EasyAdminBundle\Form\Type\CodeEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;

// voir : https://stackoverflow.com/a/48412951 et https://symfony.com/doc/current/form/create_custom_field_type.html
class JsonEditorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->addModelTransformer(new CallbackTransformer(
            static fn ($object) => json_encode($object, \JSON_PRETTY_PRINT),
            static fn ($json) => json_decode($json, true)
        ));
    }

    public function getParent(): string
    {
        return CodeEditorType::class;
    }
}
