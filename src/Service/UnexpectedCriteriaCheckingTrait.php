<?php

namespace App\Service;

trait UnexpectedCriteriaCheckingTrait
{
    /**
     * @param array<string> $actualCriteria
     * @param array<string> $expectedCriteriaKeys
     *
     * @throws \UnexpectedValueException
     */
    protected function throwIfUnexpectedCriteria(array $actualCriteria, array $expectedCriteriaKeys): void
    {
        $unexpectedCriterias = array_diff(array_keys($actualCriteria), $expectedCriteriaKeys);

        if (!empty($unexpectedCriterias)) {
            throw new \UnexpectedValueException('Le(s) critère(s) '.implode(', ', $unexpectedCriterias).' ne sont pas supportés. Voici la liste des critères acceptés : '.implode(', ', $expectedCriteriaKeys).'.');
        }
    }
}
