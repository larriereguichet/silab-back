<?php

namespace App\Service;

trait UnexpectedOrderByValueCheckingTrait
{
    /**
     * @param array<string> $actualSortValues
     * @param array<string> $expectedSortValues
     *
     * @throws \UnexpectedValueException
     */
    protected function throwIfUnexpectedOrderByValueCheckingTrait(array $actualSortValues, array $expectedSortValues): void
    {
        $unexpectedSortValues = array_diff($actualSortValues, $expectedSortValues);

        if (!empty($unexpectedSortValues)) {
            throw new \UnexpectedValueException('Le tri par '.implode(', ', $unexpectedSortValues).' ne sont pas supportés. Voici la liste de tri accepté : '.implode(', ', $expectedSortValues).'.');
        }
    }
}
