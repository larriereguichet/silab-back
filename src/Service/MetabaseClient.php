<?php

namespace App\Service;

use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use Firebase\JWT\JWT;

class MetabaseClient
{
    private string $metabaseSecretKey;
    private string $metabaseUrl;

    public function __construct(ElusServiceOffer $elusServiceOffer)
    {
        $this->metabaseSecretKey = $elusServiceOffer->getConfiguration()['metabase']['secretKey'];
        $this->metabaseUrl = $elusServiceOffer->getConfiguration()['metabase']['url'];
    }

    /**
     * @param mixed[] $lockedParameters
     */
    private function generateDashboardToken(int $dashboardId, int $expirationMinutes = 10, array $lockedParameters = []): string
    {
        $payload = [
            'resource' => ['dashboard' => $dashboardId],
            'params' => empty($lockedParameters) ? new \stdClass() : $lockedParameters,
            'exp' => time() + ($expirationMinutes * 60),
            'iat' => time(),
        ];

        // Actuellement, le seul algorithme pris en charge par Metabase est HS256, documentation : https://www.metabase.com/docs/latest/people-and-groups/authenticating-with-jwt
        return JWT::encode($payload, $this->metabaseSecretKey, 'HS256');
    }

    /**
     * @param mixed[] $lockedParameters
     * @param mixed[] $hiddenParameters
     * @param mixed[] $prefilledParameters
     */
    private function generateDashboardIframeUrl(int $dashboardId, int $expirationMinutes = 10, array $lockedParameters = [], array $hiddenParameters = [], array $prefilledParameters = [], int $autoRefresh = null): string
    {
        $token = $this->generateDashboardToken($dashboardId, $expirationMinutes, $lockedParameters);

        $hiddenParametersString = empty($hiddenParameters) ? '' : '&hide_parameters='.implode(',', $hiddenParameters);

        $prefilledParametersWithImplodedValues = [];
        foreach ($prefilledParameters as $prefilledParameter => $values) {
            $prefilledParametersWithImplodedValues[] = "$prefilledParameter=".implode("&$prefilledParameter=", $values);
        }
        $prefilledParametersString = empty($prefilledParametersWithImplodedValues) ? '' : '?'.implode('&', $prefilledParametersWithImplodedValues);

        $autoRefreshString = empty($autoRefresh) ? '' : "&refresh=$autoRefresh";

        $iframeUrl = "{$this->metabaseUrl}/embed/dashboard/$token$prefilledParametersString#bordered=false&titled=false$hiddenParametersString$autoRefreshString";

        return $iframeUrl;
    }

    /**
     * @param mixed[] $lockedParameters
     * @param mixed[] $hiddenParameters
     * @param mixed[] $prefilledParameters
     */
    public function getDashboardIframeUrl(int $dashboardId, int $expirationMinutes = 10, array $lockedParameters = [], array $hiddenParameters = [], array $prefilledParameters = [], int $autoRefresh = null): string
    {
        return $this->generateDashboardIframeUrl($dashboardId, $expirationMinutes, $lockedParameters, $hiddenParameters, $prefilledParameters, $autoRefresh);
    }
}
