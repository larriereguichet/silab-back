<?php

namespace App\Service;

use App\Exception\RuntimeException;

abstract class Utils
{
    /**
     * @param string               $stringWithKeys Exemple : "Description : {{Object.decription}} By Silab."
     * @param array<string,string> $data           Exemple : ["Object.decription" => "Je suis une description."]
     *
     * @return string Exemple : "Description : Je suis une description. By Silab."
     *
     * @throws RuntimeException si une des variables de $stringWithKeys n'a pas de clef correspondante dans $data
     */
    public static function replaceKeysInStringFromArray(string $stringWithKeys, array $data): string
    {
        return preg_replace_callback(
            '/{{(.+)}}/mU',
            function ($captureGroups) use ($data) {
                $stringKey = $captureGroups[1];
                if (!array_key_exists($stringKey, $data)) {
                    throw new RuntimeException("La variable '$stringKey' demandée n'est pas disponible, voici la liste des variables disponibles : ".implode(', ', array_keys($data)).'.');
                }

                return $data[$stringKey];
            },
            $stringWithKeys
        );
    }
}
