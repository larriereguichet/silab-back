<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Exception;

use Symfony\Component\HttpFoundation\Exception\BadRequestException as SymfonyBadRequestException;

class BadRequestException extends SymfonyBadRequestException
{
    public function __construct(string $message, \Throwable $previous = null)
    {
        parent::__construct(message: $message, previous: $previous);
    }
}
