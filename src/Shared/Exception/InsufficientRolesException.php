<?php

// Fonctionnement des Exceptions copiée de celle d'ApiPlatform.
// cf : vendor/api-platform/core/src/Exception

namespace App\Shared\Exception;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class InsufficientRolesException extends AccessDeniedException
{
    public function __construct(string $message = 'Vous n\'avez pas les droits suffisants pour accéder à cette ressource.', \Throwable $previous = null)
    {
        parent::__construct($message, $previous);
    }
}
