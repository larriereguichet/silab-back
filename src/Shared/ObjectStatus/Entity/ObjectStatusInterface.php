<?php

namespace App\Shared\ObjectStatus\Entity;

interface ObjectStatusInterface
{
    public const GROUP_AJOUTER_NOUVEAU_STATUT = 'Changer le statut de l\'objet vers un nouveau statut';
    public const GROUP_AFFICHER_STATUT = 'Affiche un statut';

    public function getCode(): string;

    public function getLabel(): string;

    public function getCreatedBy(): string;

    public function getCreatedAt(): \DateTimeInterface;
}
