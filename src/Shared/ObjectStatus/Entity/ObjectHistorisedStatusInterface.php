<?php

namespace App\Shared\ObjectStatus\Entity;

interface ObjectHistorisedStatusInterface extends ObjectStatusInterface
{
    public const GROUP_AFFICHER_HISTORIQUE_STATUTS = 'Affiche l\'historuqe d\'un statut';

    public function getComment(): string;
}
