<?php

namespace App\Shared\ObjectWithImage\Entity;

interface ObjectWithImageInterface
{
    public function getId(): ?string;

    public function setImageUrl(?string $imageUrl): self;

    public function getImageUrl(): ?string;
}
