<?php

namespace App\Shared\User\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Put;
use ApiPlatform\State\ProcessorInterface;
use App\Shared\User\Entity\User;
use App\Shared\User\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PutUserByServiceOfferDataHandler implements ProcessorInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param User         $data
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): User {
        assert($operation instanceof Put, 'Opération non supportée');

        assert($data instanceof User);

        $existingUser = $this->userRepository->find($uriVariables['id']);

        if (!$existingUser) {
            throw new NotFoundHttpException('Utilisateur non trouvé.');
        }

        $newRolesForThisServicoffer = $data->getRoles();
        $allExistingRoles = $existingUser->getRoles();
        $existingRolesWithThisServiceOfferRolesRemoved = array_filter(
            $allExistingRoles,
            fn ($role) => !str_starts_with($role, 'SERVICEOFFER_'.$uriVariables['serviceOfferId'])
        );

        $updatedRoles = array_unique(array_merge($existingRolesWithThisServiceOfferRolesRemoved, $newRolesForThisServicoffer));

        $existingUser->setRoles($updatedRoles);

        $this->entityManager->flush();

        return $this->userRepository->getUserCloneWithOnlyServiceOfferScopedRoles($existingUser, intval($uriVariables['serviceOfferId']));
    }
}
