<?php

namespace App\Shared\User\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Shared\Security\Role\ScopedRoleHierarchyInterface;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class GetCurrentUserDataHandler implements ProviderInterface
{
    public function __construct(private Security $security, private ScopedRoleHierarchyInterface $scopedRoleHierarchy)
    {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): User
    {
        assert(!$operation instanceof CollectionOperationInterface);

        $user = $this->security->getUser();

        if (!$user instanceof User) {
            throw new \RuntimeException('Utilisateur non trouvé');
        }

        $user->setReachableRoles($this->scopedRoleHierarchy->getReachableRoleNames($user->getRoles()));

        return $user;
    }
}
