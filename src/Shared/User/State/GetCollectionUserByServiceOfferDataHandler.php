<?php

namespace App\Shared\User\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Shared\User\Entity\User;
use App\Shared\User\Repository\UserRepository;

class GetCollectionUserByServiceOfferDataHandler implements ProviderInterface
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<User>|User
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|User
    {
        assert($operation instanceof CollectionOperationInterface);

        return $this->userRepository->findScopedClonedUsersByServiceOffer(
            intval($uriVariables['serviceOfferId']),
            orderBy: $operation->getOrder());
    }
}
