<?php

namespace App\Shared\User\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\Shared\User\Entity\User;
use App\Shared\User\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class PostUserByServiceOfferDataHandler implements ProcessorInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param User         $data
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(
        mixed $data,
        Operation $operation,
        array $uriVariables = [],
        array $context = []
    ): User {
        assert($operation instanceof Post, 'Opération non supportée');
        assert($data instanceof User);

        $email = $data->getEmail();

        $existingUser = $this->userRepository->findOneByEmail($email);

        if (null !== $existingUser) {
            $currentRoles = $existingUser->getRoles();

            $serviceOfferId = $uriVariables['serviceOfferId'];

            $leRoleAppartienALODSregex = '/^SERVICEOFFER_'.$serviceOfferId.'_ROLE_(?<serviceOfferType>[^_]+)_.*/';

            $userAlreadyExistsInCurrentServiceOffer = false;
            foreach ($currentRoles as $role) {
                if (preg_match($leRoleAppartienALODSregex, $role)) {
                    $userAlreadyExistsInCurrentServiceOffer = true;
                    break;
                }
            }

            if ($userAlreadyExistsInCurrentServiceOffer) {
                throw new ConflictHttpException('Cet utilisateur existe déjà dans cette offre de service.');
            }

            $newRoles = $data->getRoles();
            $mergedRoles = array_unique(array_merge($currentRoles, $newRoles));

            $existingUser->setRoles($mergedRoles);
            $this->entityManager->flush();

            return $existingUser;
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}
