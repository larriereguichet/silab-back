<?php

namespace App\Shared;

class Helpers
{
    // from https://stackoverflow.com/a/5147704
    /**
     * @param array<mixed> $array
     * @param array<mixed> $excludeKeys
     *
     * @return array<mixed>
     */
    public static function arrayWithoutKeys(array $array, array $excludeKeys): array
    {
        foreach ($excludeKeys as $key) {
            unset($array[$key]);
        }

        return $array;
    }

    /**
     * @param string|int   $key
     * @param array<mixed> $array
     */
    public static function initArrayValueIfAbsent(array &$array, mixed $key, mixed $value): void
    {
        if (array_key_exists($key, $array)) {
            return;
        }

        $array[$key] = $value;
    }
}
