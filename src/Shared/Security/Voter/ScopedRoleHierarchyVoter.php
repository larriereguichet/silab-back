<?php

namespace App\Shared\Security\Voter;

use App\Shared\Security\Role\ScopedRoleHierarchyInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;

class ScopedRoleHierarchyVoter extends RoleVoter
{
    protected bool $requestIsOnUnscopedRoute;

    public function __construct(private ScopedRoleHierarchyInterface $scopedRoleHierarchy)
    {
        parent::__construct('SERVICEOFFER_');
    }

    /**
     * @return array<string>
     */
    protected function extractRoles(TokenInterface $token): array
    {
        return $this->scopedRoleHierarchy->getReachableRoleNames($token->getRoleNames());
    }
}
