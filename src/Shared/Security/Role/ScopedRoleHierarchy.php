<?php

namespace App\Shared\Security\Role;

use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class ScopedRoleHierarchy extends RoleHierarchy implements ScopedRoleHierarchyInterface
{
    public const SCOPED_ROLE_DECOMPOSITION_REGEX = '/^SERVICEOFFER_(?<serviceOfferId>[^_]+)_ROLE_(?<serviceOfferType>[^_]+)_(?<profileOrActionOnEntity>.+)$/';

    /**
     * @param array<string,list<string>> $unscopedHierarchy
     */
    public function __construct(
        #[Autowire(service: ServiceOfferRepository::class)] ServiceOfferRepository $serviceOfferRepository,
        #[Autowire('%security.role_hierarchy.roles%')] array $unscopedHierarchy
    ) {
        $serviceOfferIds =
            array_map(
                function ($serviceOffer) {return $serviceOffer->getId(); },
                $serviceOfferRepository->findAll()
            );

        $hierarchyWithScopes = [];
        foreach ($serviceOfferIds as $serviceOfferId) {
            assert(!is_null($serviceOfferId));
            $hierarchyWithScopes = array_merge_recursive(
                $hierarchyWithScopes,
                $this->scopeHierarchy($unscopedHierarchy, $serviceOfferId)
            );
        }

        parent::__construct($hierarchyWithScopes);
    }

    /**
     * @param array<string,list<string>> $unscopedHierarchy
     *
     * @return array<string,list<string>>
     */
    private function scopeHierarchy(array $unscopedHierarchy, int $serviceOfferId): array
    {
        $scopedHierarchy = [];
        foreach ($unscopedHierarchy as $unscopedRootRole => $unscopedSubroles) {
            $scopedHierarchy[$this->scopeRole($unscopedRootRole, $serviceOfferId)] = $this->scopeRoles($unscopedSubroles, $serviceOfferId);
        }

        return $scopedHierarchy;
    }

    /**
     * Insère l'id de l'ods correspondant à la requête actuelle dans le rôle, si celui-ci est prévue pour (scopé).
     */
    private function scopeRole(string $unscopedRole, int $serviceOfferId): string
    {
        return preg_replace(self::SCOPED_ROLE_DECOMPOSITION_REGEX, 'SERVICEOFFER_'.$serviceOfferId.'_ROLE_${2}_${3}', $unscopedRole);
    }

    /**
     * @param array<string> $unscopedRoles
     *
     * @return array<string>
     */
    private function scopeRoles(array $unscopedRoles, int $serviceOfferId): array
    {
        return array_map(function (string $unscopedRole) use ($serviceOfferId) {
            return $this->scopeRole($unscopedRole, $serviceOfferId);
        }, $unscopedRoles);
    }
}
