<?php

namespace App\Shared\Security\Role;

use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

interface ScopedRoleHierarchyInterface extends RoleHierarchyInterface
{
}
