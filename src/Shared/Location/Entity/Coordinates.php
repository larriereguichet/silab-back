<?php

namespace App\Shared\Location\Entity;

use App\ElusServiceOffer\Intervention\Entity\Intervention;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\InterventionServiceOffer\Equipement\Entity\EquipementApiRessource;
use Symfony\Component\Serializer\Annotation\Groups;

class Coordinates
{
    public function __construct(
        #[Groups([EquipementApiRessource::GROUP_AFFICHER_EQUIPEMENT, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, Intervention::GROUP_LISTER_INTERVENTIONS])]
        public float $lat,
        #[Groups([EquipementApiRessource::GROUP_AFFICHER_EQUIPEMENT, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, Intervention::GROUP_LISTER_INTERVENTIONS])]
        public float $lng
    ) {
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }

    public static function fromUnsafeLatLng(float|null $latitude, float|null $longitude): Coordinates|null
    {
        $hasCorrectCoordinates = !is_null($latitude) && !is_null($longitude);

        return $hasCorrectCoordinates ? new Coordinates(
            $latitude,
            $longitude
        ) : null;
    }
}
