<?php

namespace App\Shared\Carl\ValueObject;

class CarlLinkedUrlDocument
{
    public function __construct(
        private string $documentLinkId,
        private string $documentId,
        private string $documentUrl
    ) {
    }

    public function getDocumentlinkId(): string
    {
        return $this->documentLinkId;
    }

    public function getDocumentId(): string
    {
        return $this->documentId;
    }

    // liens de l'image
    public function getAttachmentUrl(): string
    {
        return $this->documentUrl;
    }
}
