<?php

namespace App\Shared\Carl\Exception;

use App\Exception\RuntimeException;

class NoCarlObjectsFoundWithGivenFilters extends RuntimeException
{
}
