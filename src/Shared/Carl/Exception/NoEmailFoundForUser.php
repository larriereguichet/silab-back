<?php

namespace App\Shared\Carl\Exception;

use App\Exception\RuntimeException;

class NoEmailFoundForUser extends RuntimeException
{
    public function __construct(string $userId, \Throwable $previous = null)
    {
        parent::__construct(message: "Aucun email trouvé pour l'acteur carl avec l'id $userId.", previous: $previous);
    }
}
