<?php

namespace App\Shared\Carl\Exception;

use App\Exception\RuntimeException;

class MultipleCarlObjectsFoundWithGivenFilters extends RuntimeException
{
}
