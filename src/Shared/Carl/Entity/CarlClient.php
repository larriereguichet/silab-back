<?php

namespace App\Shared\Carl\Entity;

use App\Exception\RuntimeException;
use App\Repository\Carl\CarlClientRepository;
use App\Service\JsonApiResource;
use App\Shared\Carl\Exception\MultipleCarlObjectsFoundWithGivenFilters;
use App\Shared\Carl\Exception\MultipleEmailsFoundForUser;
use App\Shared\Carl\Exception\NoCarlObjectsFoundWithGivenFilters;
use App\Shared\Carl\Exception\NoEmailFoundForUser;
use App\Shared\Carl\ValueObject\CarlLinkedUrlDocument;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PropertyAccess\Exception\NoSuchIndexException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

// QUESTION_TECHNIQUE : comment injecter le logger ici ?
#[ORM\Entity(repositoryClass: CarlClientRepository::class)]
class CarlClient
{
    public const CARL_WO_MR_IMAGE_FIELD = 'xtraTxt09';
    private const CARL_API_ORIGIN = 'silab';
    private const CARL_API_ENTITY_ENDPOINT = 'entities/v1';
    private const CARL_API_AUTH_ENDPOINT = 'auth/v1';
    private const CARL_API_SUB_PATH = 'api/';
    private const CARL_ERROR_500_MAX_TRIES = 2;

    public const SILAB_TO_CARL_DEMANDEINTERVENTION_STATUS_MAPPING = [
        'awaiting_validation' => ['REQUEST'],
        'awaiting_information' => ['WAITINFO'],
        'accepted' => ['AWAITINGREAL', 'INPROGRESS'],
        'closed' => ['FINISHED', 'CLOSED'],
    ];

    public const CARL_TO_SILAB_DEMANDEINTERVENTION_STATUS_MAPPING = [
        'REQUEST' => ['awaiting_validation'],
    ];

    public const INTERVENTION_STATUS_WORKFLOW = [
        'REQUEST' => 'VALIDATE',
        'VALIDATE' => 'AWAITINGREAL',
        'AWAITINGREAL' => 'CLOSED',
        'INPROGRESS' => 'CLOSED',
    ];

    public const DEMANDEINTERVENTION_STATUS_WORKFLOW = [
        'WAITINFO' => 'REQUEST',
    ];

    public const CARL_ALLOWED_LOCATION_EQPT_TYPES = ['box', 'buildingset', 'building', 'zone'];

    private ?string $token = null;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ORM\JoinColumn(nullable: false)]
    protected ?string $title = null;

    #[ORM\Column(length: 255, options: ['default' => 'à changer'])]
    private ?string $login = null;

    #[ORM\Column(length: 255, options: ['default' => 'à changer'])]
    private ?string $password = null;

    #[ORM\Column(length: 255, options: ['default' => 'à changer'])]
    private ?string $baseUrl = null;

    #[ORM\Column(options: ['default' => 100])]
    private ?int $pageLimit = null;

    /** @var array<string,string> $priorityMapping la liste de valeur doit être ordonnée par ordre décroissant. */
    #[ORM\Column(options: ['default' => '[]'])]
    private array $priorityMapping = [];

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBaseUrl(): ?string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): self
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    public function getApiUrl(): ?string
    {
        if (is_null($this->getBaseUrl())) {
            return null;
        }

        return $this->getBaseUrl().self::CARL_API_SUB_PATH;
    }

    public function getPageLimit(): ?int
    {
        return $this->pageLimit;
    }

    public function setPageLimit(int $pageLimit): self
    {
        $this->pageLimit = $pageLimit;

        return $this;
    }

    // Note: créer un client http à chaque fois peut empêcher de faire des requêtes en parrallèle
    private function getCarlHttpClient(): HttpClientInterface
    {
        return HttpClient::createForBaseUri(
            $this->getApiUrl(),
        );
    }

    /**
     * Retourne le token d'accès si éxistant ou en génère un nouveau si besoin.
     */
    private function getToken(): string
    {
        return $this->token ?? $this->token = $this->generateToken();
    }

    /**
     * Génére un token grâce aux identifiants Carl.
     *
     * @return string un nouveau token
     */
    private function generateToken(): string
    {
        $response = $this->getCarlHttpClient()->request(
            'POST',
            self::CARL_API_AUTH_ENDPOINT.'/authenticate',
            [
                'body' => [
                    'login' => $this->getLogin(),
                    'password' => $this->getPassword(),
                    'origin' => self::CARL_API_ORIGIN,
                ],
            ]
        );

        $body = $response->toArray();

        if (!array_key_exists('X-CS-Access-Token', $body)) {
            $message = 'Token non trouvé dans la réponse carl';
            throw new RuntimeException($message);
        }

        return $body['X-CS-Access-Token'];
    }

    /**
     * appel simple à l'API carl.
     *
     * @param string|null  $body        inséré dans le body de la requête
     * @param array<mixed> $options     option du client http
     *                                  voir: https://symfony.com/doc/current/http_client.html#making-requests
     * @param bool         $hasResponse false si l'appel attend une reponse vide, par exemple pour un DELETE
     *
     * @return array<mixed> le retour de carl
     */
    public function call(
        string $method,
        string $absoluteUrl,
        mixed $body = null,
        array $options = [],
        bool $hasResponse = true
    ): array {
        $options = array_replace_recursive(
            [
                'headers' => [
                    'X-CS-Access-Token' => $this->getToken(),
                    'Accept' => 'application/vnd.api+json',
                ],
                'query' => [
                    'page[limit]' => $this->getPageLimit(),
                ],
                'body' => $body,
            ],
            $options
        );

        // inspiré par https://stackoverflow.com/questions/25002164/php-try-catch-and-retry
        $nbTries = 0;
        $responseArray = []; // Note: si on ne met pas ça, l'analyse statique pense que : Variable $responseArray might not be defined.
        while (true) {
            try {
                $response = $this->getCarlHttpClient()->request(
                    $method,
                    $absoluteUrl,
                    $options
                );
                $responseArray = $hasResponse ?
                    $response->toArray()
                    : [];
            } catch (HttpExceptionInterface $clientException) { // voir https://symfony.com/doc/current/http_client.html#handling-exceptions
                // on essaye de récupérer les jolies erreurs carl en json si c'est possible
                ++$nbTries;

                if ($nbTries >= self::CARL_ERROR_500_MAX_TRIES) {
                    try {
                        $errorMessages = array_reduce(
                            $clientException->getResponse()->toArray(false)['errors'],
                            function ($carlErrorMessages, $carlError) {
                                $carlErrorMessages[] = $carlError['title'];

                                return $carlErrorMessages;
                            },
                            []
                        );
                        $carlErrorMessage = implode(', ', $errorMessages);
                    } catch (\Exception $e) {
                        $carlErrorMessage = $clientException->getResponse()->getContent(false);
                    }
                    throw new RuntimeException("Erreurs Carl rencontrées : $carlErrorMessage");
                }
                continue;
            }
            break;
        }

        return $responseArray;
    }

    /**
     * La méthode getCarlObjects est plus aisée à manipuler mais consomme potentiellement plus de RAM.
     * Vous pouvez utiliser cette méthode pour des besoin d'optimisation (impact réèl à mesurer).
     *
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     *
     * @return array<mixed>
     */
    public function getCarlObjectsRaw(
        string $entity,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): array {
        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity);

        return $this->getCarlObjectsByAbsoluteUrl($absoluteUrl, $filters, $includes, $fields, $sort);
    }

    /**
     * Cette méthode tranforme pour nous les élément en JsonApiResource.
     *
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     *
     * @return array<JsonApiResource>
     */
    public function getCarlObjects(
        string $entity,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): array {
        $objectsApiResponse = $this->getCarlObjectsRaw($entity, $filters, $includes, $fields, $sort);

        $objectApiResources = [];
        foreach ($objectsApiResponse['data'] as $objectApiResponseData) {
            $objectApiResources[] = new JsonApiResource($objectApiResponseData, $objectsApiResponse['included']);
        }

        return $objectApiResources;
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     *
     * @throws MultipleCarlObjectsFoundWithGivenFilters
     * @throws NoCarlObjectsFoundWithGivenFilters
     */
    public function getOneCarlObject(
        string $entity,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): JsonApiResource {
        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity);

        $carlObjects = $this->getCarlObjectsByAbsoluteUrl($absoluteUrl, $filters, $includes, $fields, $sort);

        if (sizeof($carlObjects['data']) > 1) {
            throw new MultipleCarlObjectsFoundWithGivenFilters("Plusieurs objets $entity trouvés pour les filtres donnés");
        }

        if (sizeof($carlObjects['data']) < 1) {
            throw new NoCarlObjectsFoundWithGivenFilters("Aucun objet $entity trouvé pour les filtres donnés");
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        return new JsonApiResource(
            $carlObjects['data'][0],
            $propertyAccessor->getValue($carlObjects, '[included]')
        );
    }

    /**
     * Limitation : La donnée retournée ne contien pas la section relative à la pagination 'links' (self, first, next ...).
     *
     * @param array<string>               $orderOfValues par exemple ['urgent','important','normal']
     * @param array<string,array<string>> $filters       voir doc json-api :
     *                                                   https://jsonapi.org/format/#fetching-filtering
     * @param array<string>               $includes      Attention, une relation doit être inclue
     *                                                   dans les displayedMembers
     *                                                   (si displayedMembers n'est pas vide)
     * @param array<string,array<string>> $fields        champs à inclure pour chaque type voir doc json-api :
     *                                                   https://jsonapi.org/format/#fetching-includes, ex:
     *                                                   ['type1'
     *                                                   =>
     *                                                   ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>|null          $secondarySort
     *
     * @return array<mixed>
     */
    private function getCarlObjectsByAbsoluteUrlWithProvidedOrderOfValues(
        string $absoluteUrl,
        string $sortAttribut,
        array $orderOfValues,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array|null $secondarySort = ['createDate'],
    ): array {
        $missingObjects = $this->getPageLimit();

        $allObjects = [
            'data' => [],
            'included' => [],
        ];

        if (!empty($filters[$sortAttribut])) {
            $orderOfValues = array_intersect($orderOfValues, $filters[$sortAttribut]);
        }

        foreach ($orderOfValues as $targetValue) {
            $filters[$sortAttribut] = [$targetValue];
            $options = [
                'query' => $this->buildJsonApiQuery($filters, $includes, $fields, $secondarySort),
            ];
            $options['query']['page[limit]'] = $missingObjects;

            $newObjects = $this->call('GET', $absoluteUrl, null, $options);

            $newObjectsCount = count($newObjects['data']);

            if (0 === $newObjectsCount) {
                continue;
            }

            $newIncludedRelationships = [];

            foreach ($newObjects['included'] as $relationship) {
                if (in_array($relationship, $allObjects['included'])) {
                    continue;
                }
                $newIncludedRelationships[] = $relationship;
            }

            $allObjects = array_merge_recursive($allObjects, ['data' => $newObjects['data'], 'included' => $newIncludedRelationships]);

            $missingObjects -= $newObjectsCount;

            if (0 === $missingObjects) {
                break;
            }
        }

        return $allObjects;
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     *
     * @return array<mixed>
     */
    public function getCarlObjectsByAbsoluteUrl(
        string $absoluteUrl,
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): array {
        // $sortedAttributesThatNeedsOrderOfValuesProvision contient tout les atributs qui pour un tri necessitent de fournir l'ordre des valeurs souhaités. Par exemple pour 'workPriority' on peut vouloir ['faible','normale','importante'].
        $sortedAttributesThatNeedsOrderOfValuesProvision = [
            'workPriority',
            '-workPriority',
        ];

        $sortHasAttributesThatNeedsOrderOfValuesProvision = !empty(array_intersect($sort, $sortedAttributesThatNeedsOrderOfValuesProvision));

        // Ce bloc de code géré le cas ou l'on tri avec un ordre de valeur custom.
        if (!empty($sortHasAttributesThatNeedsOrderOfValuesProvision)) {
            $sortAttribut = array_intersect($sort, $sortedAttributesThatNeedsOrderOfValuesProvision);

            // On verifie qu'il n'y ait pas plus de deux tri, et que le premier soit celui qui necessite la fourniture d'une liste d'ordre de valeur.
            assert(2 <= count($sort));
            assert(1 === count($sortAttribut));
            assert($sortAttribut[0] === $sort[0]);

            $sortAttribut = array_shift($sort);

            $orderOfValues =
                match ($sortAttribut) {
                    '-workPriority' => array_keys($this->getPriorityMapping()),
                    'workPriority' => array_keys(array_reverse($this->getPriorityMapping())),
                    default => []
                };
            assert(!empty($orderOfValues));

            return $this->getCarlObjectsByAbsoluteUrlWithProvidedOrderOfValues(
                $absoluteUrl,
                trim($sortAttribut, '-'),
                $orderOfValues,
                $filters,
                $includes,
                $fields,
                $sort
            );
        }

        $options = [
            'query' => $this->buildJsonApiQuery($filters, $includes, $fields, $sort),
        ];

        return $this->call('GET', $absoluteUrl, null, $options);
    }

    /**
     * @param array<string>               $sort     les tris à appliquer dans l'ordre, voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-sorting
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param string                      $entity   le code de l'entité requêtée, ex: wo
     */
    public function getCarlObject(
        string $entity,
        string $id,
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): JsonApiResource {
        $options = [
            'query' => $this->buildJsonApiQuery([], $includes, $fields, $sort),
        ];
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity).'/'.urlencode($id);
        $resourceResponseArray = $this->call('GET', $absoluteUrl, null, $options);

        return new JsonApiResource(
            $resourceResponseArray['data'],
            $propertyAccessor->getValue($resourceResponseArray, '[included]')
        );
    }

    /**
     * @param array<string,array<string>> $filters  voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-filtering
     * @param array<string,array<string>> $fields   champs à inclure pour chaque type voir doc json-api :
     *                                              https://jsonapi.org/format/#fetching-includes, ex:
     *                                              ['type1'
     *                                              =>
     *                                              ['attribut_à_inclure_1', 'attribut_à_inclure_2']]
     * @param array<string>               $includes Attention, une relation doit être inclue
     *                                              dans les displayedMembers
     *                                              (si displayedMembers n'ets pas vide)
     * @param array<string>               $sort
     *
     * @return array<mixed> le tableau query pour le httpclient
     */
    private function buildJsonApiQuery(
        array $filters = [],
        array $includes = [],
        array $fields = [],
        array $sort = []
    ): array {
        $query = [];

        foreach ($filters as $attributeKey => $allowedAttributeValues) {
            if (is_array($allowedAttributeValues) && array_key_exists('LIKE', $allowedAttributeValues)) {
                $query["filter[$attributeKey][LIKE]"] = $allowedAttributeValues['LIKE'];
                continue;
            }
            $query["filter[$attributeKey]"] = join(',', $allowedAttributeValues);
        }

        if (!empty($includes)) {
            $query['include'] = join(',', $includes);
        }

        foreach ($fields as $includedEntityType => $includedAttributeValues) {
            $query["fields[$includedEntityType]"] = join(',', $includedAttributeValues);
        }

        if (!empty($sort)) {
            $query['sort'] = join(',', $sort);
        }

        return $query;
    }

    /**
     * @param string $entity         le code de l'entité requêtée, ex: wo
     * @param string $postedResource le JSON de l'entity postée
     */
    public function postCarlObject(string $entity, string $postedResource): JsonApiResource
    {
        $absoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity);
        $options = ['headers' => ['Content-Type' => 'application/vnd.api+json']];

        $resourceResponseArray = $this->call('POST', $absoluteUrl, $postedResource, $options);

        return new JsonApiResource($resourceResponseArray['data'], $resourceResponseArray['included'] ?? null);
    }

    /**
     * Amélioration à faire : pouvoir mettre à jour les relationships.
     *
     * @param array<string,string|number|boolean|null> $attributes
     * @param array<string,mixed>                      $relationships
     *
     * @return array<mixed> l'object carl dans son nouvel état
     */
    public function patchCarlObject(string $entity, string $objectId, array $attributes = [], array $relationships = []): array
    {
        $objectData = ['data' => []];
        if (!empty($attributes)) {
            $objectData['data']['attributes'] = $attributes;
        }

        if (!empty($relationships)) {
            $objectData['data']['relationships'] = $relationships;
        }

        $options = ['headers' => ['Content-Type' => 'application/vnd.api+json']];

        $entityAbsoluteUrl = $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.'/'.urlencode($entity).'/'.$objectId;

        return $this->call(
            'PATCH',
            $entityAbsoluteUrl,
            json_encode($objectData),
            $options
        );
    }

    public function getUserIdFromEmail(string $userEmail): string
    {
        $filters = [
            'type' => ['EMAIL'],
            'phoneNum' => [$userEmail],
            ];

        $includes = [
            'actor',
        ];

        $fields = [
            'actor' => ['id'],
            'phone' => ['actor'],
        ];

        $usersFound = $this->getCarlObjectsRaw(
            'phone',
            $filters,
            $includes,
            $fields,
        );

        if (!array_key_exists('included', $usersFound) || sizeof($usersFound['included']) < 1) {
            throw new RuntimeException("Aucun utilisateur correspondant à l'email $userEmail");
        }
        if (sizeof($usersFound['included']) > 1) {
            throw new RuntimeException("Plusieurs utilisateurs correspondant à l'email $userEmail");
        }

        $includedActor = new JsonApiResource(
            $usersFound['included'][0]
        );

        return $includedActor->getId();
    }

    public function getUserEmailFromId(string $userId): string
    {
        $filters = [
            'type' => ['EMAIL'],
            'actor.id' => [$userId],
        ];

        $fields = [
            'phone' => ['phoneNum'],
        ];

        try {
            $emailResource = $this->getOneCarlObject(
                'phone',
                $filters,
                [],
                $fields,
            );
        } catch (NoCarlObjectsFoundWithGivenFilters $e) {
            throw new NoEmailFoundForUser($userId, $e);
        } catch (MultipleCarlObjectsFoundWithGivenFilters $e) {
            throw new MultipleEmailsFoundForUser($userId, $e);
        }

        return $emailResource->getAttribute('phoneNum');
    }

    public function getUserEmailFromIdSafe(string $userId): string
    {
        try {
            return $this->getUserEmailFromId($userId);
        } catch (NoEmailFoundForUser $e) {
            return "Aucun email renseigné pour l'acteur carl avec l'id $userId";
        } catch (MultipleEmailsFoundForUser $e) {
            return "Plusieurs emails renseignés pour l'acteur carl avec l'id $userId";
        }
    }

    /** @return array<string,string>  les valeurs retournées sont ordonnées par ordre décroissant*/
    public function getPriorityMapping(): array
    {
        return $this->priorityMapping;
    }

    /** @param array<string,string> $priorityMapping */
    public function setPriorityMapping(array $priorityMapping): self
    {
        $this->priorityMapping = $priorityMapping;

        return $this;
    }

    public function deleteObjectByAbsoluteUrl(string $absoluteUrl, string $bodyJson = null): void
    {
        $this->call(
            'DELETE',
            $absoluteUrl,
            $bodyJson,
            hasResponse: false
        );
    }

    public function deleteObject(string $entity, string $id): void
    {
        $this->deleteObjectByAbsoluteUrl(
            $this->getApiUrl().self::CARL_API_ENTITY_ENDPOINT.
            '/'.urlencode($entity).
            '/'.$id
        );
    }

    public function addDocumentLinkToObject(string $documentUrl, string $objectId, string $objectJavaClass, string $userId, string $docTypeId): void
    {
        $attachementId = $this->createUrlAttachement($documentUrl, $userId);
        $documentId = $this->createDocument(
            $attachementId,
            $docTypeId,
        );
        $this->linkDocumentToObject($documentId, $objectId, $objectJavaClass, $userId);
    }

    /**
     * @return string id de l'url d'attachement
     */
    private function createUrlAttachement(string $attachmentUrl, string $userId): string
    {
        $dataImageUrl = $this->extractDataFromUrl($attachmentUrl);
        $urlToEncode = [
            'data' => [
                'type' => 'url',
                'attributes' => [
                    'path' => $dataImageUrl['path'],
                    'fileName' => $dataImageUrl['name'],
                ],
                'relationships' => [
                    'publishedBy' => [
                        'data' => [
                            'id' => $userId,
                            'type' => 'actor',
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->postCarlObject('url', json_encode($urlToEncode));

        return $response->getId();
    }

    /**
     * @return array<string,string> un tableau représentant les données du l'url. Il est constitué des clés 'path' et 'name'.
     */
    private function extractDataFromUrl(string $url): array
    {
        $separatorPosition = strrpos($url, '/');

        return [
                'path' => substr($url, 0, $separatorPosition + 1),
                'name' => substr($url, $separatorPosition + 1),
        ];
    }

    /**
     * @param array<string> $newAttachmentsUrls
     * @param string        $entityJavaBean     Attention dans certaines situation il faut utiliser le code de la classe abstraite. Par exemple pour les WO on utilise directement le code WO alors que pour les MR il faut utiliser le code ABSTRACTMR.
     */
    public function updateUrlDocumentLinks(array $newAttachmentsUrls, string $objectId, string $entityJavaBean, string $docTypeId, string $userEmail): void
    {
        // récupère tous les documents avec le doctypeId attachés à l'inter
        $oldUrlDocuments = $this->getObjectUrlDocuments($objectId, $docTypeId);

        // on retire de carl tous ceux qui ont un url qui n'est pas dans $documentsUrl
        foreach ($oldUrlDocuments as $urlDocument) {
            if (in_array($urlDocument->getAttachmentUrl(), $newAttachmentsUrls)) {
                continue;
            }

            // Note supprimer un documentlink supprime également le document associé
            $this->deleteObject('documentlink', $urlDocument->getDocumentlinkId());
        }

        // on ajoute dans carl un document pour chaque $documentsUrl qui n'est pas présent dans carl
        $oldAttachmentsUrls = array_map(
            function ($urlDocument) {
                return $urlDocument->getAttachmentUrl();
            },
            $oldUrlDocuments
        );

        foreach ($newAttachmentsUrls as $newAttachmentUrl) {
            if (in_array($newAttachmentUrl, $oldAttachmentsUrls)) {
                continue;
            }
            $this->addDocumentLinkToObject($newAttachmentUrl, $objectId, $entityJavaBean, $this->getUserIdFromEmail($userEmail), $docTypeId);
        }
    }

    /**
     * @return string id du document
     */
    private function createDocument(string $attachementId, string $docTypeId): string
    {
        $documentToEncode = [
            'data' => [
                'type' => 'document',
                'relationships' => [
                    'type' => [
                        'data' => [
                            'id' => $docTypeId,
                            'type' => 'doctype',
                        ],
                    ],
                    'attachment' => [
                        'data' => [
                            'id' => $attachementId,
                            'type' => 'url',
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->postCarlObject('document', json_encode($documentToEncode));

        return $response->getId();
    }

    private function linkDocumentToObject(string $documentId, string $objectId, string $objectJavaClass, string $userId): void
    {
        $linkToEncode = [
            'data' => [
                'type' => 'documentlink',
                'attributes' => [
                    'entityClass' => $objectJavaClass,
                    'entityId' => $objectId,
                ],
                'relationships' => [
                    'document' => [
                        'data' => [
                            'type' => 'document',
                            'id' => $documentId,
                        ],
                    ],
                    'attachedBy' => [
                        'data' => [
                            'id' => $userId,
                            'type' => 'actor',
                        ],
                    ],
                ],
            ],
        ];

        $this->postCarlObject('documentlink', json_encode($linkToEncode));
    }

    /**
     * @return array<CarlLinkedUrlDocument>
     */
    public function getObjectUrlDocuments(string $objectId, string $doctypeId): array
    {
        $linkedDocumentsResponseArray = $this->getLinkedDocuments($objectId, ['document.type.id' => [$doctypeId]]);

        return array_map(
            function ($documentLinkResponseArray) use ($linkedDocumentsResponseArray) {
                $documentLinkApiResource = new JsonApiResource(
                    $documentLinkResponseArray,
                    $linkedDocumentsResponseArray['included']
                );

                $imagePath = $documentLinkApiResource->getRelationship('document')->getRelationship('attachment')->getAttribute('path');
                $imageFileName = $documentLinkApiResource->getRelationship('document')->getRelationship('attachment')->getAttribute('fileName');

                return new CarlLinkedUrlDocument(
                    $documentLinkApiResource->getId(),
                    $documentLinkApiResource->getRelationship('document')->getId(),
                    $imagePath.$imageFileName
                );
            },
            $linkedDocumentsResponseArray['data'],
        );
    }

    public function getObjectDocumentUrl(string $objectId, string $doctypeId): ?string
    {
        try {
            $docTypeIdFilter = ['document.type.id' => [$doctypeId]];
            $linkedDocuments = $this->getLinkedDocuments($objectId, $docTypeIdFilter);
            foreach ($linkedDocuments['data'] as $linkedDocument) {
                $linkedDocumentRessource = new JsonApiResource(
                    $linkedDocument,
                    $linkedDocuments['included']
                );

                $documentUrl = $linkedDocumentRessource->getRelationshipRelatedLink('document');

                $document = $this->getCarlObjectsByAbsoluteUrl($documentUrl);
                $documentRessource = new JsonApiResource(
                    $document['data'],
                    $document['included']
                );
                $imagePath = $documentRessource->getRelationship('attachment')->getAttribute('path');
                $imageName = $documentRessource->getRelationship('attachment')->getAttribute('fileName');

                return $imagePath.$imageName;
            }
        } catch (NoSuchIndexException $_exception) {
            return null;
        }

        return null;
    }

    /**
     * @param array<string,array<string>> $filters voir doc json-api :
     *                                             https://jsonapi.org/format/#fetching-filtering
     *
     * @return array<mixed>
     */
    private function getLinkedDocuments(string $entityId, array $filters = []): array
    {
        $filters['entityId'] = [$entityId];

        $includes = ['document', 'document.attachment'];

        return $this->getCarlObjectsRaw('documentlink', $filters, $includes);
    }

    private function createDescriptionObject(string $description): JsonApiResource
    {
        $descriptionJson = json_encode([
                        'data' => [
                            'type' => 'description',
                            'attributes' => [
                                'description' => $description,
                            ],
                        ],
                    ]);

        return $this->postCarlObject('description', $descriptionJson);
    }

    /**
     * @param array<mixed> $objectRelationShipsArray
     */
    public function addDescriptionToObjectRelationshipsArray(array &$objectRelationShipsArray, string $description): void
    {
        $objectRelationShipsArray['longDesc'] = [
            'data' => [
                'id' => $this->createDescriptionObject($description)->getId(),
                'type' => 'description',
            ],
        ];
    }

    /**
     * Supprime la relation ainsi que l'objet longDesc.
     */
    public function deleteObjectDescription(string $objectId, string $entity): void
    {
        $objectRessource = $this->getCarlObject($entity, $objectId, ['longDesc']);
        $relationshipLink = $objectRessource->getRelationshipSelfLink('longDesc');
        $descriptionId = $objectRessource->getRelationship('longDesc')->getId();

        $this->deleteObjectByAbsoluteUrl($relationshipLink, json_encode(['data' => null]));
        $this->deleteObject('description', $descriptionId);
    }

    /**
     * @param array<string,string> $statusCodeWorkflow
     */
    public function transitionToStatus(string $entityType, string $objectId, string $targetCarlStatusCode, array $statusCodeWorkflow): void
    {
        do {
            $object = $this->getCarlObject(
                $entityType,
                $objectId
            );

            $objectStatusCode = $object->getAttribute('statusCode');
            if (!array_key_exists($objectStatusCode, $statusCodeWorkflow)) {
                throw new RuntimeException('Etat '.$objectStatusCode.' non pris en charge. Liste des status pris en charge : '.implode(', ', array_keys($statusCodeWorkflow)).'.');
            }

            $newStatusCode = $this->doStatusTransition(
                $object,
                $statusCodeWorkflow[$objectStatusCode]);
        } while ($targetCarlStatusCode !== $newStatusCode);
    }

    /**
     * @return string le nouveau status
     */
    private function doStatusTransition(JsonApiResource $objectApiResource, string $nextStatus): string
    {
        //  Générer l'url
        $workflowTransitionUrl = $objectApiResource->getLink('workflow-transitions');
        //  Générer le body
        $filters = [
            'nextStepCode' => [$nextStatus],
            'id' => ['LIKE' => ':com.carl.xnet.system.status.TransitionParameters'],
        ];

        $body = json_encode(
            $this->getCarlObjectsByAbsoluteUrl(
                $workflowTransitionUrl,
                $filters
            )
        );
        //  Faire le post
        $options = ['headers' => ['Content-Type' => 'application/vnd.api+json']];
        $response = $this->call('POST', $workflowTransitionUrl, $body, $options);

        return $response['data']['attributes']['nextStepCode'];
    }

    public function getStatusDescriptionFromCodeAndFlowCode(string $statusCode, string $statusFlowCode): string
    {
        $filters = [
            'code' => [$statusCode],
            'statusFlow.code' => [$statusFlowCode],
        ];

        $fields = [
            'simplestep' => ['description'],
        ];

        return $this->getOneCarlObject(
            entity: 'simplestep',
            filters: $filters,
            fields: $fields
        )->getAttribute('description');
    }

    public function getUnitDescriptionFromCode(string $unitCode): string
    {
        $unitJsonApiRessource = $this->getOneCarlObject(
            entity: 'unit',
            filters: ['code' => [$unitCode]],
            fields: ['unit' => ['description']]
        );

        return $unitJsonApiRessource->getAttribute('description');
    }
}
