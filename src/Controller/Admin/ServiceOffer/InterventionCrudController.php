<?php

namespace App\Controller\Admin\ServiceOffer;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Form\Type\JsonEditorType;
use App\InterventionServiceOffer\Gmao\Repository\GmaoConfigurationInterventionRepository;
use App\Repository\Ged\GedClientRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class InterventionCrudController extends AbstractCrudController
{
    public function __construct(private GmaoConfigurationInterventionRepository $gmaoConfigurationInterventionRepository, private GedClientRepository $gedClientRepository)
    {
    }

    public static function getEntityFqcn(): string
    {
        return InterventionServiceOffer::class;
    }

    public function configureFields(string $pageName): iterable
    {
        // configurations gmao disponibles
        $gmaoConfigurationInterventions = $this->gmaoConfigurationInterventionRepository->findAll();
        $gmaoConfigurationInterventionsChoice = [];
        foreach ($gmaoConfigurationInterventions as $gmaoConfigurationIntervention) {
            $gmaoConfigurationInterventionsChoice[$gmaoConfigurationIntervention->getTitle()] = $gmaoConfigurationIntervention;
        }

        // clients GED disponibles
        $gedClients = $this->gedClientRepository->findAll();
        $gedClientsChoice = ['aucun' => null];
        foreach ($gedClients as $gedClient) {
            $gedClientsChoice[$gedClient->getTitle()] = $gedClient;
        }

        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            TextField::new('link'),
            TextField::new('image'),
            ChoiceField::new('gmaoConfiguration')
                ->allowMultipleChoices(false)
                ->setChoices($gmaoConfigurationInterventionsChoice)
                ->hideOnDetail()
                ->hideOnIndex(),
            TextField::new('gmaoConfiguration')->hideOnForm(),
            ArrayField::new('availableEquipementsIds')
                ->hideOnDetail()
                ->hideOnIndex(),
            ArrayField::new('availableEquipements')
                ->hideOnForm(),
            ChoiceField::new('gedClient')
                ->allowMultipleChoices(false)
                ->setChoices($gedClientsChoice)
                ->hideOnDetail()
                ->hideOnIndex(),
            TextField::new('gedClient')->hideOnForm(),
            CodeEditorField::new('gedClientConfigurationDocumentation')
                ->setDisabled()
                ->setLabel("Documentation de le configuration du client GED (enregistrez d'abord un client GED)")
                ->hideOnDetail()
                ->hideOnIndex(),
            CodeEditorField::new('gedClientConfiguration')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex() // Note: ça plante sur l'index, jsp pk
                ->setHelp('Variables diponibles : '.implode(', ', InterventionServiceOffer::getGedClientConfigurationVariableKeys())),
        ];
    }
}
