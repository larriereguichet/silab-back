<?php

namespace App\Controller\Admin\ServiceOffer;

use App\Form\Type\JsonEditorType;
use App\Shared\Carl\Entity\CarlClient;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class CarlClientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CarlClient::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            TextField::new('baseUrl'),
            TextField::new('login'),
            TextField::new('password')->setFormType(PasswordType::class)
                ->hideOnIndex()
                ->hideOnDetail(),
            IntegerField::new('pagelimit'),
            CodeEditorField::new('priorityMapping')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex(), // Note: ça plante sur l'index, jsp pk
        ];
    }
}
