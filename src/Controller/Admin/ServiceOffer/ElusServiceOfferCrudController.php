<?php

namespace App\Controller\Admin\ServiceOffer;

use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use App\Form\Type\JsonEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ElusServiceOfferCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ElusServiceOffer::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            TextField::new('link'),
            TextField::new('image'),
            CodeEditorField::new('configuration')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex(), // Note: ça plante sur l'index
        ];
    }
}
