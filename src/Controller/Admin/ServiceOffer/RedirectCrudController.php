<?php

namespace App\Controller\Admin\ServiceOffer;

use App\Entity\ServiceOffer\Template\RedirectServiceOffer;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RedirectCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RedirectServiceOffer::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
