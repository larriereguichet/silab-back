<?php

namespace App\Controller\Admin\ServiceOffer;

use App\Form\Type\JsonEditorType;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\Repository\Carl\CarlClientRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarlConfigurationInterventionCrudController extends AbstractCrudController
{
    public function __construct(private CarlClientRepository $carlClientRepository)
    {
    }

    public static function getEntityFqcn(): string
    {
        return CarlConfigurationIntervention::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $availableCarlClients = $this->carlClientRepository->findAll();
        $carlClientsChoices = [];
        foreach ($availableCarlClients as $carlClient) {
            $carlClientsChoices[$carlClient->getTitle()] = $carlClient;
        }

        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            ArrayField::new('costCenters'),
            ArrayField::new('directions'),
            // voir https://github.com/EasyCorp/EasyAdminBundle/issues/4388
            CodeEditorField::new('actionTypesMap')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex(), // Note: ça plante sur l'index, jsp pk
            ArrayField::new('actionTypesMap')
                ->hideOnDetail()
                ->hideOnForm(),
            CodeEditorField::new('carlAttributesPresetForDemandeInterventionCreation')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex(),
            ArrayField::new('carlAttributesPresetForDemandeInterventionCreation')
                ->hideOnDetail()
                ->hideOnForm(),
            CodeEditorField::new('carlRelationshipsPresetForDemandeInterventionCreation')
                ->setFormType(JsonEditorType::class)
                ->hideOnIndex(),
            ArrayField::new('carlRelationshipsPresetForDemandeInterventionCreation')
                ->hideOnDetail()
                ->hideOnForm(),
            ChoiceField::new('carlClient')
                ->allowMultipleChoices(false)
                ->setChoices($carlClientsChoices)
                ->hideOnDetail()->hideOnIndex(),
            TextField::new('photoAvantInterventionDoctypeId'),
            TextField::new('woViewUrlSubPath'),
            ArrayField::new('includeEquipementsChildrenWithType'),
        ];
    }
}
