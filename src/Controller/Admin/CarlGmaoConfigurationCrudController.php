<?php

namespace App\Controller\Admin;

use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\Repository\Carl\CarlClientRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarlGmaoConfigurationCrudController extends AbstractCrudController
{
    public function __construct(private CarlClientRepository $carlClientRepository)
    {
    }

    public static function getEntityFqcn(): string
    {
        return CarlGmaoConfiguration::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $availableCarlClients = $this->carlClientRepository->findAll();
        $carlClientsChoices = [];
        foreach ($availableCarlClients as $carlClient) {
            $carlClientsChoices[$carlClient->getTitle()] = $carlClient;
        }

        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('title'),
            ChoiceField::new('carlClient')
                ->allowMultipleChoices(false)
                ->setChoices($carlClientsChoices)
                ->hideOnDetail()->hideOnIndex(),
        ];
    }
}
