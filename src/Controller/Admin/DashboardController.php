<?php

namespace App\Controller\Admin;

use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use App\Entity\Ged\AlfrescoGedClient;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Entity\ServiceOffer\Template\RedirectServiceOffer;
use App\InterventionServiceOffer\Gmao\Entity\CarlConfigurationIntervention;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration as LogisticCarlGmaoConfiguration;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\User\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle("Panneau d'administration");
    }

    public function configureMenuItems(): iterable
    {
        $listIcon = 'fas fa-list';
        yield MenuItem::linkToCrud('Utilisateurs', $listIcon, User::class);
        yield MenuItem::section('Intervention');
        yield MenuItem::linkToCrud('ODS Intervention', $listIcon, InterventionServiceOffer::class);
        yield MenuItem::linkToCrud('Configuration Carl', $listIcon, CarlConfigurationIntervention::class);
        yield MenuItem::section('Logistique');
        yield MenuItem::linkToCrud('ODS Logistique', $listIcon, LogisticServiceOffer::class);
        yield MenuItem::linkToCrud('Configuration Carl', $listIcon, LogisticCarlGmaoConfiguration::class);
        yield MenuItem::section('Silab V1');
        yield MenuItem::linkToCrud('ODS Redirection', $listIcon, RedirectServiceOffer::class);
        yield MenuItem::section('POC');
        yield MenuItem::linkToCrud('ODS Elus', $listIcon, ElusServiceOffer::class);
        yield MenuItem::section('Services');
        yield MenuItem::linkToCrud('Client - Carl', $listIcon, CarlClient::class);
        yield MenuItem::linkToCrud('Client - Ged Alfresco', $listIcon, AlfrescoGedClient::class);
    }
}
