<?php

namespace App\LogisticServiceOffer\Emplacement\Entity;

use App\LogisticServiceOffer\Article\Entity\ArticleApiResource;
use App\LogisticServiceOffer\Mouvement\Entity\MouvementApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

class Emplacement
{
    public function __construct(
        #[Groups([ArticleApiResource::GROUP_AFFICHER_ARTICLE, MouvementApiResource::GROUP_CREER_MOUVEMENT, MouvementApiResource::GROUP_AFFICHER_MOUVEMENT])]
        private string $id,
        #[Groups([ArticleApiResource::GROUP_AFFICHER_ARTICLE])]
        private ?string $code = null
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }
}
