<?php

namespace App\LogisticServiceOffer\Intervention\Repository;

use App\Entity\Intervention;
use App\Entity\InterventionStatusHistorised;
use App\Exception\RuntimeException;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\Service\JsonApiResource;
use App\Service\UnexpectedCriteriaCheckingTrait;
use App\Service\UnexpectedOrderByValueCheckingTrait;
use App\Shared\Carl\Entity\CarlClient;
use App\Shared\Carl\Exception\MultipleEmailsFoundForUser;
use App\Shared\Carl\Exception\NoEmailFoundForUser;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CarlInterventionRepository implements InterventionRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;
    use UnexpectedOrderByValueCheckingTrait;

    public function __construct(
        private CarlGmaoConfiguration $carlGmaoConfiguration
    ) {
    }

    public function update(string $id, Intervention $entity, string $userEmail): Intervention
    {
        throw new RuntimeException('Méthode non implémenté');
    }

    public function find(mixed $id): Intervention
    {
        $sampleInterventionArray = $this->findBy(['id' => [$id]]);

        if (count($sampleInterventionArray) > 1) {
            throw new RuntimeException("Plus d'une intervention trouvée pour l'id : $id");
        }

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException("Aucune intervention trouvée pour l'id : $id");
        }

        return array_pop($sampleInterventionArray);
    }

    public function save(Intervention $entity): Intervention
    {
        throw new RuntimeException("Cette méthode n'est pas implémentée");
    }

    public function getClassName()
    {
        return Intervention::class;
    }

    public function findAll(): array
    {
        return $this->findBy([]);
    }

    public function findOneBy(array $criteria): Intervention
    {
        $sampleInterventionArray = $this->findBy($criteria);

        if (0 === count($sampleInterventionArray)) {
            throw new RuntimeException('Aucune intervention trouvée avec les critères donnés');
        }

        return array_values($sampleInterventionArray)[0];
    }

    /**
     * @param array<string, mixed> $criteria :
     *                                       [interventionActionTypes] = Nature sur Carl. Seules les interventions
     *                                       qui ont une de ces natures seront retournée.
     *                                       Si vide, toutes les natures seront incluses.
     *                                       [directions]              = supervisor sur Carl. Seules les interventions
     *                                       dont la direction du demandeur correspond
     *                                       seront retournées.
     *                                       Si vide, toutes les directions seront incluses.
     *                                       [costCenters]             Seules les interventions
     *                                       dont le centre de cout correspond à une des valeur
     *                                       seront retournées.
     *                                       Si vide, tous les centres de coûts seront inclus.
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }

        if (null !== $orderBy) {
            $this->throwIfUnexpectedOrderByValueCheckingTrait(
                $orderBy,
                [
                    'createdAt',
                    '-createdAt',
                    'updatedAt',
                    '-updatedAt',
                    'priority',
                    '-priority',
                ]
            );

            $orderBy = array_map(
                function ($orderByRaw) {
                    switch ($orderByRaw) {
                        case 'createdAt':
                            $value = 'createDate';
                            break;
                        case '-createdAt':
                            $value = '-createDate';
                            break;
                        case 'updatedAt':
                            $value = 'modifyDate';
                            break;
                        case '-updatedAt':
                            $value = '-modifyDate';
                            break;
                        case 'priority':
                            $value = 'workPriority';
                            break;
                        case '-priority':
                            $value = '-workPriority';
                            break;

                        default:
                            $value = $orderByRaw;
                            break;
                    }

                    return $value;
                },
                $orderBy);
        } else {
            $orderBy = [];
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'id',
                'statuses',
                'interventionActionTypes',
                'directions',
                'costCenters',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $id = $propertyAccessor->getValue($criteria, '[id]');
        $statuses = $propertyAccessor->getValue($criteria, '[statuses]');
        $interventionActionTypes = $propertyAccessor->getValue($criteria, '[interventionActionTypes]');
        $directions = $propertyAccessor->getValue($criteria, '[directions]');
        $costCenters = $propertyAccessor->getValue($criteria, '[costCenters]');

        $filters = [];

        if (!empty($id)) {
            $filters['id'] = $id;
        }

        if (!empty($statuses)) {
            $filters['statusCode'] = $statuses;
        }

        if (!empty($interventionActionTypes)) {
            $filters['actionType.id'] = $interventionActionTypes;
        }
        if (!empty($directions)) {
            $filters['supervisor.code'] = $directions;
        }
        if (!empty($costCenters)) {
            $filters['costCenter.code'] = $costCenters;
        }

        $includes = [
            'actionType',
            'longDesc',
            'createdBy',
        ];

        $fields = [
            'wo' => [
                'code',
                'xtraTxt03',
                'xtraTxt10',
                CarlClient::CARL_WO_MR_IMAGE_FIELD,
                'xtraNum02',
                'createDate',
                'workPriority',
                'statusCode',
                'description',
                'latitude',
                'longitude',
                ],
            'actiontype' => ['id'],
            'description' => ['rawDescription'],
            'actor' => ['xtraTxt02', 'fullName'],
        ];

        $interventionResources = $this->carlGmaoConfiguration->getCarlClient()->getCarlObjectsRaw(
            'wo',
            $filters,
            $includes,
            $fields,
            $orderBy,
        );

        /** @var array<Intervention> $interventions */
        $interventions = [];
        foreach ($interventionResources['data'] as $interventionResourceData) {
            try {
                $interventionResource = new JsonApiResource($interventionResourceData, $interventionResources['included']);
                $id = $interventionResource->getId();
                $code = $interventionResource->getAttribute('code');
                $creatorName = $interventionResource->getRelationship('createdBy')->getAttribute('fullName');
                $interventionStatusHistory = $this->getInterventionStatusHistory($id);

                $streetNumber = $interventionResource->getAttribute('xtraNum02');
                $streetNumberComplement = $interventionResource->getAttribute('xtraTxt03');
                $streetName = $interventionResource->getAttribute('xtraTxt10');
                $interventionAddress = "$streetNumber$streetNumberComplement $streetName";

                $hasLocalisation = $interventionResource->getAttribute('latitude') && $interventionResource->getAttribute('longitude');

                $intervention = (new Intervention())
                ->setPriority($this->carlGmaoConfiguration->getCarlClient()->getPriorityMapping()[$interventionResource->getAttribute('workPriority')])
                ->setDescription($interventionResource->getRelationship('longDesc')?->getAttribute('rawDescription'))
                ->setId($id)
                ->setUserEmail($this->carlGmaoConfiguration->getCarlClient()->getUserEmailFromId($interventionResource->getRelationship('createdBy')->getId()))
                ->setCreatedAt(new \DateTime($interventionResource->getAttribute('createDate')))
                ->setAddress($interventionAddress)
                ->setActionType($interventionResource->getRelationship('actionType')->getId())
                ->setCode($code)
                ->setCreatorName($creatorName)
                ->setStatusHistory($interventionStatusHistory)
                ->setImageUrl($interventionResource->getAttribute(CarlClient::CARL_WO_MR_IMAGE_FIELD))
                ->setTitle($interventionResource->getAttribute('description') ?? '');

                if ($hasLocalisation) {
                    $intervention
                        ->setLatitude($interventionResource->getAttribute('latitude'))
                        ->setLongitude($interventionResource->getAttribute('longitude'));
                }

                $interventions[] = $intervention;
            } catch (MultipleEmailsFoundForUser|NoEmailFoundForUser $e) {
                // l'intervention n'est pas traitable si l'on est pas capable de retrouver un email, on ne l'ajoute pas et on continue normalement
            }
        }

        return $interventions;
    }

    /**
     * @return array<InterventionStatusHistorised>
     */
    public function getInterventionStatusHistory(
        string $interventionId
    ): array {
        $filters = [
            'origin.id' => [
                $interventionId,
            ],
            ];

        $includes = [
            'statusChangedBy',
        ];

        $fields = [
            'actor' => [
                'fullName',
            ],
            'wohistostatus' => [
                'statusChangedDate',
                'statusCode',
            ],
        ];

        $interventionStatusHistoryFromApi = $this->carlGmaoConfiguration->getCarlClient()->getCarlObjectsRaw(
            'wohistostatus',
            $filters,
            $includes,
            $fields,
        );

        $interventionStatusHistory = [];

        foreach ($interventionStatusHistoryFromApi['data'] as $interventionStatusData) {
            $interventionStatusResource = new JsonApiResource(
                $interventionStatusData,
                $interventionStatusHistoryFromApi['included']
            );

            $interventionStatusFullName = $interventionStatusResource
            ->getRelationship('statusChangedBy')
            ?->getAttribute('fullName');

            $interventionStatusState = $interventionStatusResource
            ->getAttribute('statusCode');

            $interventionStatusChangedDate = $interventionStatusResource
            ->getAttribute('statusChangedDate');

            $interventionStatusHistory[] = new InterventionStatusHistorised(
                $interventionId,
                $interventionStatusState,
                $interventionStatusFullName,
                new \DateTime($interventionStatusChangedDate),
            );
        }

        return $interventionStatusHistory;
    }
}
