<?php

namespace App\LogisticServiceOffer\Intervention\Repository;

use App\Entity\Intervention;
use App\Exception\RuntimeException;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfiguration;

class InterventionRepository implements InterventionRepositoryInterface
{
    private InterventionRepositoryInterface $specificInterventionRepository;

    public function __construct(
        protected GmaoConfiguration $gmaoConfiguration
    ) {
        try {
            $this->specificInterventionRepository = match ($gmaoConfiguration::class) {
                CarlGmaoConfiguration::class => new CarlInterventionRepository(
                    $gmaoConfiguration
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfiguration::class);
        }
    }

    public function find($id): Intervention
    {
        return $this->specificInterventionRepository->find($id);
    }

    public function findAll(): array
    {
        return $this->specificInterventionRepository->findAll();
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        return $this->specificInterventionRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): Intervention
    {
        return $this->specificInterventionRepository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return Intervention::class;
    }

    public function update(string $id, Intervention $intervention, string $userEmail): Intervention
    {
        throw new RuntimeException('Méthode non implémenté');
    }

    public function save(Intervention $intervention): Intervention
    {
        throw new RuntimeException("Cette méthode n'est pas implémentée");
    }

    public function getInterventionStatusHistory(
        string $interventionId
    ): array {
        return $this->specificInterventionRepository->getInterventionStatusHistory($interventionId);
    }
}
