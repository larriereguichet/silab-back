<?php

namespace App\LogisticServiceOffer\Intervention\Repository;

use App\Entity\Intervention;
use App\Entity\InterventionStatusHistorised;
use Doctrine\Persistence\ObjectRepository;

interface InterventionRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): Intervention;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<Intervention>
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    /**
     * @return array<Intervention>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Intervention;

    public function update(string $id, Intervention $intervention, string $userEmail): Intervention;

    public function save(Intervention $intervention): Intervention;

    /**
     * @return array<InterventionStatusHistorised>
     */
    public function getInterventionStatusHistory(string $interventionId): array;
}
