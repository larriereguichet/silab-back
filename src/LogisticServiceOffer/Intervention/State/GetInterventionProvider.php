<?php

namespace App\LogisticServiceOffer\Intervention\State;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Intervention;
use App\LogisticServiceOffer\Intervention\Repository\InterventionRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;

final class GetInterventionProvider implements ProviderInterface
{
    public function __construct(
        private LogisticServiceOfferRepository $logisticServiceOfferRepository
    ) {
    }

    /**
     * Fournit au endpoint getAllIntervention les datas provenants du connecteur Carl.
     *
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): Intervention
    {
        assert($operation instanceof Get);

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $interventionRepository = new InterventionRepository(
            $logisticServiceOffer->getGmaoConfiguration()
        );

        return $interventionRepository->find($uriVariables['id']);
    }
}
