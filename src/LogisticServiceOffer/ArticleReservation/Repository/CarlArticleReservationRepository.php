<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\Exception\RuntimeException;
use App\Exception\WarehouseIdIsMissingException;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\ArticleReservation\Entity\CarlArticleReservationId;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Intervention\Repository\InterventionRepositoryInterface;
use App\Service\JsonApiResource;
use App\Service\UnexpectedCriteriaCheckingTrait;
use Symfony\Component\PropertyAccess\PropertyAccess;

final class CarlArticleReservationRepository implements ArticleReservationRepositoryInterface
{
    use UnexpectedCriteriaCheckingTrait;

    private const API_REQUEST_PARAMETERS_INCLUDES = [
        'WO',
        'actor',
        'actor.supervisor',
        'item',
        'item.stockBatches',
        'item.stockBatches.warehouse',
        'item.stockBatches.location',
        'item.itemLocations.location',
        'item.itemLocations.location.warehouse',
        'itemWarehouse.warehouse',
    ];

    private const API_REQUEST_PARAMETERS_FIELDS = [
        'reserve' => ['reserveDate', 'quantity', 'WO', 'actor', 'item', 'itemWarehouse'],
        'wo' => ['code', 'description'],
        'actor' => ['fullName', 'supervisor'],
        'item' => ['unit', 'code', 'description', 'stockBatches', 'itemLocations'],
        'batch' => ['quantity', 'warehouse', 'location'],
        'warehouse' => ['code'],
        'location' => ['code', 'warehouse'],
    ];

    public function __construct(
        private CarlGmaoConfiguration $carlGmaoConfiguration,
        private InterventionRepositoryInterface $interventionRepository
    ) {
    }

    /**
     * @param mixed $id un id au format généré par la méthode CarlArticleReservationId->toString()
     */
    public function find(mixed $id): ?ArticleReservation
    {
        $carlArticleReservationId = CarlArticleReservationId::fromString($id);

        $criterias = [
            'articleId' => $carlArticleReservationId->articleId,
            'interventionId' => $carlArticleReservationId->interventionId,
            'warehouseId' => $carlArticleReservationId->warehouseId,
            'storageLocationId' => $carlArticleReservationId->storageLocationId,
        ];

        return $this->findOneBy($criterias);
    }

    public function findAll()
    {
        return $this->findBy([]);
    }

    public function findOneBy(array $criteria): ArticleReservation
    {
        $articleReservations = $this->findBy($criteria);

        if (0 === count($articleReservations)) {
            throw new RuntimeException('Aucune réservation trouvée avec les critères donnés');
        }

        return array_values($articleReservations)[0];
    }

    public function getClassName()
    {
        return self::class;
    }

    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array {
        if (null !== $limit) {
            throw new RuntimeException('Le paramètre limit n\'est pas encore implémenté');
        }
        if (null !== $offset) {
            throw new RuntimeException('Le paramètre offset n\'est pas encore implémenté');
        }
        $this->throwIfUnexpectedCriteria(
            $criteria,
            [
                'warehouseCode',
                'warehouseId',
                'interventionId',
                'articleId',
                'storageLocationId',
            ]
        );

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $warehouseCode = $propertyAccessor->getValue($criteria, '[warehouseCode]');
        if (is_array($warehouseCode)) {
            throw new RuntimeException('Cette methode ne supporte pas plusieurs warehouseCode');
        }
        $warehouseId = $propertyAccessor->getValue($criteria, '[warehouseId]');
        $interventionId = $propertyAccessor->getValue($criteria, '[interventionId]');
        $articleId = $propertyAccessor->getValue($criteria, '[articleId]');
        $storageLocationId = $propertyAccessor->getValue($criteria, '[storageLocationId]');
        // Note: le filtrage sur le storageLocationId se fera à posteriori à cause des limitation de crnk
        // ( filtrage sur collection provoque des illegal attempt to dereference collection)

        /** @var array<string, array<string>> $filters */
        $filters = [];

        if (!is_null($warehouseCode)) {
            $filters['itemWarehouse.warehouse.code'] = [$warehouseCode];
        }

        if (!is_null($warehouseId)) {
            $filters['itemWarehouse.warehouse.id'] = [$warehouseId];
        }

        if (!is_null($interventionId)) {
            $filters['WO.id'] = [$interventionId];
        }

        if (!is_null($articleId)) {
            $filters['item.id'] = [$articleId];
        }

        $resources = $this->carlGmaoConfiguration->getCarlClient()->getCarlObjectsRaw(
            'reserve',
            $filters,
            self::API_REQUEST_PARAMETERS_INCLUDES,
            self::API_REQUEST_PARAMETERS_FIELDS,
            ['-reserveDate']
        );

        // Convertion des données reçues, reservation au format "carl" (non groupées)
        // on vérifie également certains critères d'acceptation
        /** @var array<ArticleReservation> $carlArticleReservations */
        $carlArticleReservations = [];
        foreach ($resources['data'] as $resourceData) {
            try {
                $articleReservationResource = new JsonApiResource($resourceData, $resources['included']);
                $carlArticleReservations[] = $this->buildCarlArticleReservationFromApiResource($articleReservationResource);
            } catch (WarehouseIdIsMissingException $exception) {
                continue;
            }
        }

        // Regroupement des réservation selon id générique (voir ArticleReservationId)
        $groupedArticleReservations = $this->groupArticleReservationsById($carlArticleReservations);

        // retrait des locationId indésirables (impossible à faire en rest)
        if (!is_null($storageLocationId)) {
            $groupedArticleReservations = array_filter(
                $groupedArticleReservations,
                function (ArticleReservation $articleReservation) use ($storageLocationId) {
                    return $storageLocationId === $articleReservation->getStorageLocationId();
                }
            );
        }

        return $groupedArticleReservations;
    }

    /**
     * Regroupe ensemble toutes les réservations qui ont le même articleReservationId
     * en additionant les quantités demandées, pour coller au modèle générique.
     *
     * @param array<ArticleReservation> $articleReservations
     *
     * @return array<ArticleReservation>
     */
    private function groupArticleReservationsById(array $articleReservations): array
    {
        /** @var array<string,ArticleReservation> $groupedArticleReservations */
        $groupedArticleReservations = [];
        foreach ($articleReservations as $articleReservation) {
            $carlArticleReservationId = new CarlArticleReservationId(
                $articleReservation->getArticleId(),
                $articleReservation->getInterventionId(),
                $articleReservation->getWarehouseId(),
                $articleReservation->getStorageLocationId()
            );
            // on utlise la version string de l'ArticleReservationId pour pouvoir l'utiliser en clef de tableau
            $articleReservationIdString = $carlArticleReservationId->__toString();

            if (array_key_exists($articleReservationIdString, $groupedArticleReservations)) {
                // si une réservation avec le même "id" et déja présente, on cumule la quantité demandée
                $groupedArticleReservations[$articleReservationIdString]
                    ->increaseQuantity($articleReservation->getQuantity());
            } else {
                // sinon on l'ajoute simplement
                $articleReservation->setId($carlArticleReservationId->__toString());
                $groupedArticleReservations[$articleReservationIdString] = $articleReservation;
            }
        }

        return array_values($groupedArticleReservations);
    }

    private function buildCarlArticleReservationFromApiResource(JsonApiResource $articleReservationApiResource): ArticleReservation
    {
        // // on ignore toutes les réservations qui n'on pas de warehouseId
        if (
            null
            ===
            $articleReservationApiResource->getRelationship('itemWarehouse')?->getRelationship('warehouse')->getId()
        ) {
            throw new WarehouseIdIsMissingException();
        }

        /**
         * retourne 0 si aucun batch trouvé pour cette location et ce warehouse.
         */
        $getItemQuantityForLocationAndWarehouse = function (array $stockBatchesApiResource, ?string $locationId, ?string $warehouseId) {
            foreach ($stockBatchesApiResource as $stockBatchApiResource) {
                if (
                    $stockBatchApiResource->getRelationship('location')?->getRelationship('location')->getId() === $locationId
                    && $stockBatchApiResource->getRelationship('warehouse')->getId() === $warehouseId
                ) {
                    return $stockBatchApiResource->getAttribute('quantity');
                }
            }

            return 0.0;
        };

        // il n'y a pas de batch, lorsqu' qu'il n'y a pas de stock dans le magasin souhaité
        // on va alors regarder plutôt dans les locations de l'item, pour obtenir les emplacements
        // et récupérer après les stocks correspondants s'il y en a
        $warehouseId = $articleReservationApiResource
            ->getRelationship('itemWarehouse')
            ->getRelationship('warehouse')
            ->getId();

        $itemLocationsApiResource = $articleReservationApiResource
            ->getRelationship('item')
            ->getRelationshipCollection('itemLocations');

        $itemLocationsApiResourceInSelectedWarehouse = array_filter(
            $itemLocationsApiResource,
            function ($itemLocationApiResource) use ($warehouseId) {
                return $itemLocationApiResource
                    ->getRelationship('location')
                    ->getRelationship('warehouse')
                    ->getId() === $warehouseId;
            }
        );

        if (empty($itemLocationsApiResourceInSelectedWarehouse)) {
            $locationId = null;
            $locationCode = null;
        } else {
            /** @var JsonApiResource $arbitraryItemLocation */
            $arbitraryItemLocation = reset($itemLocationsApiResourceInSelectedWarehouse)->getRelationship('location');
            $locationId = $arbitraryItemLocation->getId();
            $locationCode = $arbitraryItemLocation->getAttribute('code');
        }

        $stockBatchesApiResource = $articleReservationApiResource->getRelationship('item')->getRelationshipCollection('stockBatches');

        $interventionId = $articleReservationApiResource->getRelationship('WO')->getId();

        return new ArticleReservation(
            $articleReservationApiResource->getId(),// c'est l'id carl, il sera écrasé quand les réservations seront groupées
            $articleReservationApiResource->getRelationship('item')->getId(),
            $articleReservationApiResource->getRelationship('item')->getAttribute('code'),
            $articleReservationApiResource->getRelationship('item')->getAttribute('description'),
            $articleReservationApiResource->getAttribute('quantity'),
            $getItemQuantityForLocationAndWarehouse($stockBatchesApiResource, $locationId, $warehouseId),
            $this->carlGmaoConfiguration->getCarlClient()->getUnitDescriptionFromCode($articleReservationApiResource->getRelationship('item')->getAttribute('unit')),
            $warehouseId,
            $articleReservationApiResource->getRelationship('itemWarehouse')->getRelationship('warehouse')->getAttribute('code'),
            $locationId,
            $locationCode,
            $articleReservationApiResource->getRelationship('actor')->getAttribute('fullName'),
            $articleReservationApiResource->getRelationship('actor')->getRelationship('supervisor')?->getAttribute('fullName'),
            new \DateTime($articleReservationApiResource->getAttribute('reserveDate')),
            $articleReservationApiResource->getRelationship('WO')->getId(),
            $articleReservationApiResource->getRelationship('WO')->getAttribute('code'),
            $articleReservationApiResource->getRelationship('WO')->getAttribute('description'),
            $this->interventionRepository->getInterventionStatusHistory($interventionId),
        );
    }
}
