<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use Doctrine\Persistence\ObjectRepository;

interface ArticleReservationRepositoryInterface extends ObjectRepository
{
    /**
     * @param array<string,mixed>       $criteria
     * @param array<string,string>|null $orderBy
     *
     * @return array<ArticleReservation>
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    public function findOneBy(array $criteria): ArticleReservation;

    /**
     * @param mixed $id On attend un ArticleReservationId
     */
    public function find(mixed $id): ArticleReservation|null;
}
