<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\Exception\RuntimeException;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfiguration;
use App\LogisticServiceOffer\Intervention\Repository\InterventionRepositoryInterface;

final class ArticleReservationRepository implements ArticleReservationRepositoryInterface
{
    private ArticleReservationRepositoryInterface $specificArticleReservationRepository;

    public function __construct(
        protected GmaoConfiguration $gmaoConfiguration,
        InterventionRepositoryInterface $interventionRepository
    ) {
        try {
            $this->specificArticleReservationRepository = match ($gmaoConfiguration::class) {
                CarlGmaoConfiguration::class => new CarlArticleReservationRepository(
                    $gmaoConfiguration,
                    $interventionRepository
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfiguration::class);
        }
    }

    public function find(mixed $id): ?ArticleReservation
    {
        return $this->specificArticleReservationRepository->find($id);
    }

    public function findAll()
    {
        return $this->specificArticleReservationRepository->findAll();
    }

    public function findOneBy(array $criteria): ArticleReservation
    {
        return $this->specificArticleReservationRepository->findOneBy($criteria);
    }

    public function getClassName()
    {
        return self::class;
    }

    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array {
        return $this->specificArticleReservationRepository->findBy(
            $criteria,
            $orderBy,
            $limit,
            $offset
        );
    }
}
