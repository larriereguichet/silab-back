<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\Exception\RuntimeException;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservationIssue;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;

final class CarlArticleReservationIssueRepository implements ArticleReservationIssueRepositoryInterface
{
    public function __construct(private CarlGmaoConfiguration $carlGmaoConfiguration)
    {
    }

    public function save(ArticleReservationIssue $entity): void
    {
        $articleReservationIssue = $entity;

        $this->enforceInterventionEndDateIsAtLeastToday($articleReservationIssue->getInterventionId());

        $stockIssueJson = $this->generateIssueApiData(
            $articleReservationIssue
        );

        $this->carlGmaoConfiguration->getCarlClient()->postCarlObject('issue', $stockIssueJson);
    }

    /**
     * S'assure que la date de fin de l'intervention donnée est a minima la date d'aujourd'hui, en
     * modifiant l'intervention au besoin.
     */
    private function enforceInterventionEndDateIsAtLeastToday(string $interventionId): void
    {
        $minimumDateTime = new \DateTime();

        $fieldsOnlyWoEndDate = [
            'reserve' => ['WO'],
            'wo' => ['WOEnd'],
        ];

        $reserveApiResource = $this->carlGmaoConfiguration->getCarlClient()->getCarlObject(
            'wo',
            $interventionId,
            [],
            $fieldsOnlyWoEndDate
        );

        $woEndDateTime = new \DateTime($reserveApiResource->getAttribute('WOEnd'));

        if ($woEndDateTime < $minimumDateTime) {
            $this->carlGmaoConfiguration->getCarlClient()->patchCarlObject(
                'wo',
                $interventionId,
                ['WOEnd' => $minimumDateTime->format(\DateTimeInterface::ATOM)]
            );
        }
    }

    public function remove(ArticleReservationIssue $entity): void
    {
        throw new RuntimeException("Cette méthode n'est pas encore implémentée");
    }

    private function generateIssueApiData(
        ArticleReservationIssue $articleReservationIssue
    ): string {
        $createdById = $this
            ->carlGmaoConfiguration
            ->getCarlClient()
            ->getUserIdFromEmail($articleReservationIssue->getWarehouseClerkEmail());

        $stockIssue =
        ['data' => [
            'type' => 'issue',
            'attributes' => [
                'movementQuantity' => -$articleReservationIssue->getQuantity(),
            ],
            'relationships' => [
                'item' => [
                    'data' => [
                        'id' => $articleReservationIssue->getArticleId(),
                        'type' => 'item',
                        ],
                ],
                'WO' => [
                    'data' => [
                        'id' => $articleReservationIssue->getInterventionId(),
                        'type' => 'wo',
                        ],
                ],
                'movementType' => [
                    'data' => [
                        'id' => 'ISSUE',
                        'type' => 'movementtype',
                        ],
                ],
                'warehouse' => [
                    'data' => [
                        'id' => $articleReservationIssue->getWarehouseId(),
                        'type' => 'warehouse',
                        ],
                ],
                'createdBy' => [
                    'data' => [
                        'id' => $createdById,
                        'type' => 'actor',
                    ],
                ],
            ],
        ]];

        // si l'article à une location, on la précise
        if (!is_null($articleReservationIssue->getStorageLocationId())) {
            $stockIssue['data']['relationships']['location'] = [
                    'data' => [
                        'id' => $articleReservationIssue->getStorageLocationId(),
                        'type' => 'location',
                    ],
                ];
        }

        return json_encode($stockIssue);
    }
}
