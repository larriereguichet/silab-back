<?php

namespace App\LogisticServiceOffer\ArticleReservation\Repository;

use App\Exception\RuntimeException;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservationIssue;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfiguration;

final class ArticleReservationIssueRepository implements ArticleReservationIssueRepositoryInterface
{
    private ArticleReservationIssueRepositoryInterface $specificArticleReservationIssueRepository;

    public function __construct(
        protected GmaoConfiguration $gmaoConfiguration
    ) {
        try {
            $this->specificArticleReservationIssueRepository = match ($gmaoConfiguration::class) {
                CarlGmaoConfiguration::class => new CarlArticleReservationIssueRepository(
                    $gmaoConfiguration
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfiguration::class);
        }
    }

    public function save(ArticleReservationIssue $entity): void
    {
        $this->specificArticleReservationIssueRepository->save($entity);
    }

    public function remove(ArticleReservationIssue $entity): void
    {
        throw new RuntimeException("Cette méthode n'est pas encore implémentée");
    }
}
