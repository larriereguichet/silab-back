<?php

namespace App\LogisticServiceOffer\ArticleReservation\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\LogisticServiceOffer\ArticleReservation\Entity\ArticleReservation;
use App\LogisticServiceOffer\ArticleReservation\Repository\ArticleReservationRepository;
use App\LogisticServiceOffer\Intervention\Repository\InterventionRepository;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;

final class GetCollectionArticleReservationProvider implements ProviderInterface
{
    public function __construct(private LogisticServiceOfferRepository $logisticServiceOfferRepository)
    {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<ArticleReservation>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        assert($operation instanceof CollectionOperationInterface);

        $criteria = $context['filters'] ?? [];

        $logisticServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);

        $criteria['warehouseId'] = $logisticServiceOffer->getWarehouseId();

        if (isset($uriVariables['interventionId'])) {
            $criteria['interventionId'] = $uriVariables['interventionId'];
        }

        $interventionRepository = new InterventionRepository($logisticServiceOffer->getGmaoConfiguration());

        $articleReservationRepository = new ArticleReservationRepository(
            $logisticServiceOffer->getGmaoConfiguration(),
            $interventionRepository
        );

        return $articleReservationRepository->findBy($criteria);
    }
}
