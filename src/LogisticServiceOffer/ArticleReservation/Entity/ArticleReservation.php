<?php

namespace App\LogisticServiceOffer\ArticleReservation\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\GetCollection;
use App\Entity\InterventionStatusHistorised;
use App\Filter\SearchFilter;
use App\LogisticServiceOffer\ArticleReservation\State\GetCollectionArticleReservationProvider;

#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/interventions/{interventionId}/article-reservations',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'interventionId' => 'interventionId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_LOGISTIQUE_CONSULTER_RESERVATIONS')",
    provider: GetCollectionArticleReservationProvider::class,
)]
#[GetCollection(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/article-reservations',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_LOGISTIQUE_CONSULTER_RESERVATIONS')",
    provider: GetCollectionArticleReservationProvider::class,
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'warehouseId' => 'exact',
        'articleId' => 'exact',
        'storageLocationId' => 'exact',
        'interventionId' => 'exact',
        ]
)]
class ArticleReservation
{
    public function __construct(
        private string $id,
        private string $articleId,
        private string $articleCode,
        private string $articleDescription,
        private float $quantity,
        private float $availableQuantity,
        private string $unit,
        private string $warehouseId,
        private ?string $warehouseCode = null,
        private ?string $storageLocationId = null,
        private ?string $storageLocationCode = null,
        private ?string $creatorName = null,
        private ?string $creatorDirectionName = null,
        private ?\DateTimeInterface $expectedReceiptDate = null,
        private ?string $interventionId = null,
        private ?string $interventionCode = null,
        private ?string $interventionTitle = null,
        /** @var array<InterventionStatusHistorised>|null $interventionStatusHistory */
        private ?array $interventionStatusHistory = null
    ) {
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getArticleId(): string
    {
        return $this->articleId;
    }

    public function getArticleCode(): string
    {
        return $this->articleCode;
    }

    public function getArticleDescription(): string
    {
        return $this->articleDescription;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    // Note: attention, on ne peut pas préfixer le nom de cette méthode par "add"
    // car cela impacte le schéma généré par api-platform (quantity est alors considéré comme un array)
    public function increaseQuantity(float $quantity): void
    {
        $this->quantity += $quantity;
    }

    public function getAvailableQuantity(): float
    {
        return $this->availableQuantity;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    public function getWarehouseCode(): ?string
    {
        return $this->warehouseCode;
    }

    public function getStorageLocationId(): ?string
    {
        return $this->storageLocationId;
    }

    public function getStorageLocationCode(): ?string
    {
        return $this->storageLocationCode;
    }

    public function getCreatorName(): ?string
    {
        return $this->creatorName;
    }

    public function getCreatorDirectionName(): ?string
    {
        return $this->creatorDirectionName;
    }

    public function getExpectedReceiptDate(): ?\DateTimeInterface
    {
        return $this->expectedReceiptDate;
    }

    public function getInterventionId(): ?string
    {
        return $this->interventionId;
    }

    public function getInterventionCode(): ?string
    {
        return $this->interventionCode;
    }

    public function getInterventionTitle(): ?string
    {
        return $this->interventionTitle;
    }

    /**
     * @return array<InterventionStatusHistorised>|null
     */
    public function getInterventionStatusHistory(): ?array
    {
        return $this->interventionStatusHistory;
    }
}
