<?php

namespace App\LogisticServiceOffer\ArticleReservation\Entity;

/**
 * Cette classe représente la clef primaire d'une ArticleReservation.
 */
class CarlArticleReservationId implements \Stringable
{
    public function __construct(
        public string $articleId,
        public string $interventionId,
        public string $warehouseId,
        public ?string $storageLocationId,
    ) {
    }

    public static function fromString(string $idString): CarlArticleReservationId
    {
        $carlArticleReservationIdData = json_decode($idString);

        return new CarlArticleReservationId(
            $carlArticleReservationIdData->articleId,
            $carlArticleReservationIdData->interventionId,
            $carlArticleReservationIdData->warehouseId,
            $carlArticleReservationIdData->storageLocationId,
        );
    }

    public function __toString(): string
    {
        return json_encode($this);
    }
}
