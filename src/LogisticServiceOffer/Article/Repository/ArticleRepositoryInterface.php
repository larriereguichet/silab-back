<?php

namespace App\LogisticServiceOffer\Article\Repository;

use App\LogisticServiceOffer\Article\Entity\Article;
use Doctrine\Persistence\ObjectRepository;

interface ArticleRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): Article;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<Article>
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    /**
     * @return array<Article>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Article;
}
