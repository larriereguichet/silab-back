<?php

namespace App\LogisticServiceOffer\Article\Repository;

use App\Exception\RuntimeException;
use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfiguration;

class ArticleRepository implements ArticleRepositoryInterface
{
    private ArticleRepositoryInterface $specificArticleRepository;

    public function __construct(
        protected GmaoConfiguration $gmaoConfiguration
    ) {
        try {
            $this->specificArticleRepository = match ($gmaoConfiguration::class) {
                CarlGmaoConfiguration::class => new CarlArticleRepository(
                    $gmaoConfiguration
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfiguration::class);
        }
    }

    public function find($id): Article
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        return $this->specificArticleRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria): Article
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return Article::class;
    }
}
