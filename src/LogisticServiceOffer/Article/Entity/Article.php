<?php

namespace App\LogisticServiceOffer\Article\Entity;

use App\LogisticServiceOffer\Mouvement\Entity\MouvementApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

class Article
{
    public function __construct(
        #[Groups([MouvementApiResource::GROUP_CREER_MOUVEMENT, MouvementApiResource::GROUP_AFFICHER_MOUVEMENT])]
        private string $id,
        /** @var array<Lot> */
        private ?array $lots = null,
        private ?string $code = null,
        private ?string $titre = null,
        private ?string $unité = null
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array<Lot>
     */
    public function getLots(): ?array
    {
        return $this->lots;
    }

    /**
     * @param array<Lot> $lots
     */
    public function setLots(array $lots): self
    {
        $this->lots = $lots;

        return $this;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getUnité(): ?string
    {
        return $this->unité;
    }

    public function setUnité(?string $unité): self
    {
        $this->unité = $unité;

        return $this;
    }

    public function toApiRessource(): ArticleApiResource
    {
        return new ArticleApiResource(
            id: $this->id,
            code: $this->code,
            titre: $this->titre,
            unité: $this->unité,
            lots: $this->lots
        );
    }
}
