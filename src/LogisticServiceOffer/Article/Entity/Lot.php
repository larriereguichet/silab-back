<?php

namespace App\LogisticServiceOffer\Article\Entity;

use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;
use Symfony\Component\Serializer\Annotation\Groups;

class Lot
{
    public function __construct(
        #[Groups([ArticleApiResource::GROUP_AFFICHER_ARTICLE])]
        private float $quantité,
        #[Groups([ArticleApiResource::GROUP_AFFICHER_ARTICLE])]
        private string $magasinId,
        #[Groups([ArticleApiResource::GROUP_AFFICHER_ARTICLE])]
        private ?Emplacement $emplacement = null
    ) {
    }

    public function setQuantité(float $quantité): self
    {
        $this->quantité = $quantité;

        return $this;
    }

    public function getQuantité(): float
    {
        return $this->quantité;
    }

    public function addQuantité(float $quantitéToAdd): self
    {
        $this->quantité += $quantitéToAdd;

        return $this;
    }

    public function setmagasinId(string $magasinId): self
    {
        $this->magasinId = $magasinId;

        return $this;
    }

    public function getmagasinId(): string
    {
        return $this->magasinId;
    }

    public function setEmplacement(?Emplacement $emplacement): self
    {
        $this->emplacement = $emplacement;

        return $this;
    }

    public function getEmplacement(): ?Emplacement
    {
        return $this->emplacement;
    }
}
