<?php

namespace App\LogisticServiceOffer\Mouvement\Repository;

use App\LogisticServiceOffer\Mouvement\Entity\CreateMouvement;
use App\LogisticServiceOffer\Mouvement\Entity\Mouvement;
use Doctrine\Persistence\ObjectRepository;

interface MouvementRepositoryInterface extends ObjectRepository
{
    public function find(mixed $id): Mouvement;

    /**
     * @param array<string,mixed>    $criteria
     * @param array<int,string>|null $orderBy
     *
     * @return array<Mouvement>
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    /**
     * @return array<Mouvement>
     */
    public function findAll(): array;

    public function findOneBy(array $criteria): Mouvement;

    public function save(CreateMouvement $createMouvement): Mouvement;
}
