<?php

namespace App\LogisticServiceOffer\Mouvement\Repository;

use App\Exception\RuntimeException;
use App\LogisticServiceOffer\Gmao\Entity\CarlGmaoConfiguration;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfiguration;
use App\LogisticServiceOffer\Mouvement\Entity\CreateMouvement;
use App\LogisticServiceOffer\Mouvement\Entity\Mouvement;

class MouvementRepository implements MouvementRepositoryInterface
{
    private MouvementRepositoryInterface $specificMouvementRepository;

    public function __construct(
        protected GmaoConfiguration $gmaoConfiguration
    ) {
        try {
            $this->specificMouvementRepository = match ($gmaoConfiguration::class) {
                CarlGmaoConfiguration::class => new CarlMouvementRepository(
                    $gmaoConfiguration
                ),
            };
        } catch (\UnhandledMatchError $e) {
            throw new RuntimeException('Aucun repository correspondant à la configuration de type '.$gmaoConfiguration::class);
        }
    }

    public function find($id): Mouvement
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findAll(): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function findOneBy(array $criteria): Mouvement
    {
        throw new RuntimeException('methode non implémentée');
    }

    public function getClassName()
    {
        return Mouvement::class;
    }

    public function save(CreateMouvement $createMouvement): Mouvement
    {
        return $this->specificMouvementRepository->save($createMouvement);
    }
}
