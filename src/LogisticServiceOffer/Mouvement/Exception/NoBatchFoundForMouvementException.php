<?php

namespace App\LogisticServiceOffer\Mouvement\Exception;

use App\Exception\RuntimeException;

class NoBatchFoundForMouvementException extends RuntimeException
{
}
