<?php

namespace App\LogisticServiceOffer\Mouvement\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\LogisticServiceOffer\Mouvement\Entity\CreateMouvement;
use App\LogisticServiceOffer\Mouvement\Entity\MouvementApiResource;
use App\LogisticServiceOffer\Mouvement\Repository\MouvementRepository;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

final class CreateMouvementProcessor implements ProcessorInterface
{
    public function __construct(
        private LogisticServiceOfferRepository $logisticServiceOfferRepository,
        private Security $security
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): MouvementApiResource
    {
        assert($operation instanceof Post);
        assert($data instanceof MouvementApiResource);

        $logistiqueServiceOffer = $this->logisticServiceOfferRepository->find($uriVariables['serviceOfferId']);
        $mouvementRepository = new MouvementRepository($logistiqueServiceOffer->getGmaoConfiguration());

        $currentUser = $this->security->getUser();
        assert($currentUser instanceof User);

        $data->setCreatedBy($currentUser->getEmail());

        $createMouvement = CreateMouvement::fromApiResourceInMagasin($data, $logistiqueServiceOffer->getWarehouseId());

        return $mouvementRepository->save($createMouvement)->toApiRessource();
    }
}
