<?php

namespace App\LogisticServiceOffer\Mouvement\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class TypeDeMouvement
{
    public function __construct(
        #[Groups([MouvementApiResource::GROUP_CREER_MOUVEMENT, MouvementApiResource::GROUP_AFFICHER_MOUVEMENT])]
        private string $id,
        private ?string $libelé = null
    ) {
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setLibelé(string $libelé): self
    {
        $this->libelé = $libelé;

        return $this;
    }

    public function getLibelé(): ?string
    {
        return $this->libelé;
    }
}
