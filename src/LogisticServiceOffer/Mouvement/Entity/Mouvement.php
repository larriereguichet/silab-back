<?php

namespace App\LogisticServiceOffer\Mouvement\Entity;

use App\LogisticServiceOffer\Article\Entity\Article;
use App\LogisticServiceOffer\Emplacement\Entity\Emplacement;

/**
 * refacto : cette classe devrait être splittée en deux entitées :.
 *
 * - Mouvement -> cette classe/interface ferait partie d'un "shared context" et serait utilisée par le contexte inventory et articleReservation
 * - MouvementInventaire -> celle-ci en gros
 */
class Mouvement extends CreateMouvement
{
    public function __construct(
        private string $id,
        float $quantité,
        Article $article,
        string $magasinId,
        TypeDeMouvement $typeDeMouvement,
        string $createdBy,
        private \DateTime $createdAt,
        Emplacement $emplacement = null,
    ) {
        $this->id = $id;
        $this->createdAt = $createdAt;

        parent::__construct(
            quantité: $quantité,
            article: $article,
            emplacement: $emplacement,
            magasinId: $magasinId,
            typeDeMouvement: $typeDeMouvement,
            createdBy: $createdBy
        );
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function toApiRessource(): MouvementApiResource
    {
        return new MouvementApiResource(
            id: $this->getId(),
            quantité: $this->getQuantité(),
            article: $this->getArticle(),
            emplacement: $this->getEmplacement(),
            typeDeMouvement: $this->getTypeDeMouvement(),
            createdBy: $this->getCreatedBy(),
            createdAt: $this->getCreatedAt()
        );
    }
}
