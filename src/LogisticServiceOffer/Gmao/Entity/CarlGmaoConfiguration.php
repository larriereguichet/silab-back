<?php

namespace App\LogisticServiceOffer\Gmao\Entity;

use App\Exception\RuntimeException;
use App\LogisticServiceOffer\Gmao\Repository\CarlGmaoConfigurationRepository;
use App\Shared\Carl\Entity\CarlClient;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarlGmaoConfigurationRepository::class)]
#[ORM\Table('logisticserviceoffer_carl_gmao_configuration')]
class CarlGmaoConfiguration extends GmaoConfiguration
{
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?CarlClient $carlClient = null;

    public function getCarlClient(): CarlClient
    {
        if (is_null($this->carlClient)) {
            throw new RuntimeException('Veuillez configurer un client Carl');
        }

        return $this->carlClient;
    }

    public function setCarlClient(CarlClient $carlClient): self
    {
        $this->carlClient = $carlClient;

        return $this;
    }
}
