<?php

namespace App\LogisticServiceOffer\Gmao\Repository;

use App\LogisticServiceOffer\Gmao\Entity\GmaoConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<GmaoConfiguration>
 *
 * @method GmaoConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method GmaoConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method GmaoConfiguration[]    findAll()
 * @method GmaoConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GmaoConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GmaoConfiguration::class);
    }

    public function save(GmaoConfiguration $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(GmaoConfiguration $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return GmaoConfiguration[] Returns an array of GmaoConfiguration objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('g')
    //            ->andWhere('g.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('g.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?GmaoConfiguration
    //    {
    //        return $this->createQueryBuilder('g')
    //            ->andWhere('g.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
