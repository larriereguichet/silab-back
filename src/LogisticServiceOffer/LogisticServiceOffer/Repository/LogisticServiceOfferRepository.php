<?php

namespace App\LogisticServiceOffer\LogisticServiceOffer\Repository;

use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LogisticServiceOffer>
 *
 * @method LogisticServiceOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogisticServiceOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogisticServiceOffer[]    findAll()
 * @method LogisticServiceOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogisticServiceOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogisticServiceOffer::class);
    }

    public function save(LogisticServiceOffer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LogisticServiceOffer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return Logistic[] Returns an array of Logistic objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('i.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Logistic
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
