<?php

namespace App\LogisticServiceOffer\LogisticServiceOffer\Entity;

use App\Exception\RuntimeException;
use App\LogisticServiceOffer\Gmao\Entity\GmaoConfiguration;
use App\LogisticServiceOffer\LogisticServiceOffer\Repository\LogisticServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LogisticServiceOfferRepository::class)]
class LogisticServiceOffer extends ServiceOffer
{
    #[ORM\Column(length: 255)]
    private ?string $warehouseId = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?GmaoConfiguration $gmaoConfiguration;

    /**
     * @return array<string,string>
     */
    public function getAvailableRoles(): array
    {
        $availableRoles = [
            'SERVICEOFFER_'.$this->getId().'_ROLE_LOGISTIQUE_SUPERVISEUR' => 'Superviseur',
            'SERVICEOFFER_'.$this->getId().'_ROLE_LOGISTIQUE_MAGASINIER' => 'Magasinier',
            'SERVICEOFFER_'.$this->getId().'_ROLE_LOGISTIQUE_INVENTORISTE' => 'Inventoriste',
        ];

        return array_merge(parent::getAvailableRoles(), $availableRoles);
    }

    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    public function setWarehouseId(string $warehouseId): self
    {
        $this->warehouseId = $warehouseId;

        return $this;
    }

    public function getTemplate(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function getGmaoConfiguration(): GmaoConfiguration
    {
        if (is_null($this->gmaoConfiguration)) {
            throw new RuntimeException('Veuillez attacher une configuration GMAO à cette offre de service');
        }

        return $this->gmaoConfiguration;
    }

    public function setGmaoConfiguration(?GmaoConfiguration $gmaoConfiguration): self
    {
        $this->gmaoConfiguration = $gmaoConfiguration;

        return $this;
    }
}
