<?php

namespace App\Entity;

use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Repository\TestRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TestRepository::class)]
class Test
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?InterventionServiceOffer $relationtest = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getRelationtest(): ?InterventionServiceOffer
    {
        return $this->relationtest;
    }

    public function setRelationtest(?InterventionServiceOffer $relationtest): static
    {
        $this->relationtest = $relationtest;

        return $this;
    }
}
