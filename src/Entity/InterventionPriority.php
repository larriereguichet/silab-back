<?php

namespace App\Entity;

enum InterventionPriority: string
{
    case Urgent = 'urgent';
    case Important = 'important';
    case Normal = 'normal';
    case Low = 'bas';
}
