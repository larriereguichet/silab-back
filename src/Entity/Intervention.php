<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Exception\AddressNotFoundException;
use App\Filter\SearchFilter;
use App\InterventionServiceOffer\Intervention\State\CreateInterventionDataHandler as InterventionServiceOfferCreateInterventionDataHandler;
use App\InterventionServiceOffer\Intervention\State\GetCollectionInterventionDataHandler as InterventionServiceOfferGetCollectionInterventionDataHandler;
use App\InterventionServiceOffer\Intervention\State\GetInterventionDataHandler as InterventionServiceOfferGetInterventionDataHandler;
use App\InterventionServiceOffer\Intervention\State\UpdateInterventionDataHandler as InterventionServiceOfferUpdateInterventionDataHandler;
use App\LogisticServiceOffer\Intervention\State\GetInterventionProvider as LogisticServiceOfferGetInterventionProvider;
use App\Service\ReverseGeocoding;
use App\Shared\ObjectWithImage\Entity\ObjectWithImageInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS')",
    provider: InterventionServiceOfferGetCollectionInterventionDataHandler::class,
    normalizationContext: ['groups' => ['Lister interventions']]
)]
#[Get()] // QUESTION_TECHNIQUE: nous mettons un get sans subresource pour palier à des problèmes de génération d'IRI, comment faire autrement ?
#[Get(
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CONSULTER_INTERVENTIONS')",
    provider: InterventionServiceOfferGetInterventionDataHandler::class,
    normalizationContext: ['groups' => ['Afficher détails intervention']]
)]
#[Get(
    uriTemplate: '/logistic-service-offers/{serviceOfferId}/interventions/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_LOGISTIQUE_CONSULTER_INTERVENTIONS')",
    provider: LogisticServiceOfferGetInterventionProvider::class,
    normalizationContext: ['groups' => ['Afficher détails intervention']]
)]
#[Post(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_CREER_INTERVENTIONS')",
    processor: InterventionServiceOfferCreateInterventionDataHandler::class,
    normalizationContext: ['groups' => ['Afficher détails intervention']],
    denormalizationContext: ['groups' => ['Créer intervention']],
)]
#[Put(
    read: false,
    uriTemplate: '/intervention-service-offers/{serviceOfferId}/interventions/{id}',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'id' => 'id',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_MODIFIER_INTERVENTIONS')",
    processor: InterventionServiceOfferUpdateInterventionDataHandler::class,
    normalizationContext: ['groups' => ['Afficher détails intervention']],
    denormalizationContext: ['groups' => ['Modifier intervention']],
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'statuses' => 'exact',
        'orderBy' => 'exact',
        ]
)]
class Intervention implements ObjectWithImageInterface
{
    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?string $id = null;

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private string $title;

    #[Groups(['Créer intervention', 'Modifier intervention', 'Lister interventions', 'Afficher détails intervention'])]
    private ?InterventionPriority $priority = null;

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?\DateTime $createdAt = null;

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?string $address = null;

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?string $creatorName;

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?string $code;

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?string $imageUrl = null;

    /** @var array<InterventionStatusHistorised>|null $statusHistory */
    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?array $statusHistory = null;

    #[Groups(['Créer intervention', 'Modifier intervention', 'Lister interventions', 'Afficher détails intervention'])]
    private ?string $description = null;

    #[Groups(['Créer intervention', 'Modifier intervention'])]
    private ?string $imageB64 = null;

    #[Groups(['Créer intervention', 'Modifier intervention', 'Lister interventions', 'Afficher détails intervention'])]
    private ?string $actionType = null;

    #[Groups(['Créer intervention', 'Modifier intervention', 'Lister interventions', 'Afficher détails intervention'])]
    private ?float $latitude = null;

    #[Groups(['Créer intervention', 'Modifier intervention', 'Lister interventions', 'Afficher détails intervention'])]
    private ?float $longitude = null;

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    private ?string $userEmail = null;

    #[Groups(['Afficher détails intervention'])]
    private ?string $gmaoObjectViewUrl = null;

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setPriority(string $priority): self
    {
        $this->priority = InterventionPriority::from($priority);

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority?->value;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatorName(?string $creatorName): self
    {
        $this->creatorName = $creatorName;

        return $this;
    }

    public function getCreatorName(): ?string
    {
        return $this->creatorName;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageB64(?string $imageB64): self
    {
        $this->imageB64 = $imageB64;

        return $this;
    }

    public function getImageB64(): ?string
    {
        return $this->imageB64;
    }

    public function setActionType(string $actionType): self
    {
        $this->actionType = $actionType;

        return $this;
    }

    public function getActionType(): ?string
    {
        return $this->actionType;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setUserEmail(string $userEmail): self
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setGmaoObjectViewUrl(?string $gmaoObjectViewUrl): self
    {
        $this->gmaoObjectViewUrl = $gmaoObjectViewUrl;

        return $this;
    }

    public function getGmaoObjectViewUrl(): ?string
    {
        return $this->gmaoObjectViewUrl;
    }

    /**
     * @param array<InterventionStatusHistorised> $interventionStatusHistory
     */
    public function setStatusHistory(array $interventionStatusHistory): self
    {
        $this->statusHistory = $interventionStatusHistory;

        return $this;
    }

    /**
     * @return array<InterventionStatusHistorised>|null getStatusHistory
     */
    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    public function getStatusHistory(): ?array
    {
        return $this->statusHistory;
    }

    #[Groups(['Lister interventions', 'Afficher détails intervention'])]
    public function getCurrentStatus(): InterventionStatusHistorised
    {
        return is_null($this->statusHistory)
        ?
        null
        :
        array_reduce(
            $this->statusHistory,
            function (InterventionStatusHistorised $newestStatus, InterventionStatusHistorised $currentStatus) {
                return
                    $currentStatus->getCreatedAt()->format('Uu') > $newestStatus->getCreatedAt()->format('Uu')
                    ? $currentStatus : $newestStatus;
            },
            $this->statusHistory[0]
        );
    }

    /**
     * @param array<string,string> $actionTypesMap
     */
    public function setTitleFromActionTypeAndCoordinates(array $actionTypesMap): self
    {
        try {
            $reverseGeocoding = new ReverseGeocoding();
            $addressName = $reverseGeocoding->getAddressFromCoordinates(
                $this->getLatitude(),
                $this->getLongitude()
            );
        } catch (AddressNotFoundException $e) {
            $addressName = '';
        }

        $this->setTitle(
            $actionTypesMap[$this->getActionType()]
            .', '.
            $addressName
        );

        return $this;
    }
}
