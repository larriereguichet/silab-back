<?php

namespace App\Entity;

interface InterventionStatusInterface
{
    public function getLabel(): string;

    public function getInterventionId(): ?string;

    public function getCreatedBy(): ?string;

    public function getCreatedAt(): ?\DateTimeInterface;
}
