<?php

namespace App\Entity\ServiceOffer\Template;

use App\Repository\ServiceOffer\RedirectServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RedirectServiceOfferRepository::class)]
class RedirectServiceOffer extends ServiceOffer
{
    #[ORM\Column(length: 255)]
    private ?string $redirectUrl;

    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function getTemplate(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function setRedirectUrl(?string $redirectUrl): self
    {
        $this->redirectUrl = $redirectUrl;
        $this->setLink($redirectUrl);

        return $this;
    }
}
