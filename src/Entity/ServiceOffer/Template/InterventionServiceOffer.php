<?php

namespace App\Entity\ServiceOffer\Template;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use App\Entity\Ged\GedClient;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\Equipement\Entity\Equipement;
use App\InterventionServiceOffer\Gmao\Entity\GmaoConfigurationIntervention;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\InterventionServiceOffer\InterventionServiceOffer\State\GetInterventionServiceOfferDataHandler;
use App\Repository\ServiceOffer\InterventionServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\Entity\GedUserServiceOfferInterface;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get(
    uriTemplate: 'intervention-service-offers/{id}',
    uriVariables: ['id' => 'id'],
    security: "is_granted('SERVICEOFFER_' ~ object.getId() ~ '_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE')",
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS]],
    provider: GetInterventionServiceOfferDataHandler::class,
)]
#[Get()]
#[Post(
    security: "is_granted('ROLE_SERVICEOFFER_CREER_OFFRE_DE_SERVICE')",
    denormalizationContext: ['groups' => [ServiceOffer::GROUP_CREER_ODS, InterventionServiceOffer::GROUP_CREER_ODS]],
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
)]
#[ORM\Entity(repositoryClass: InterventionServiceOfferRepository::class)]
class InterventionServiceOffer extends ServiceOffer implements GedUserServiceOfferInterface
{
    public const GROUP_AFFICHER_DETAILS_ODS = "Afficher les détails d'un offre de service intervention";
    public const GROUP_CREER_ODS = 'Créer une offre de service intervention';

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_CREER_ODS])]
    private ?GmaoConfigurationIntervention $gmaoConfiguration;

    #[ORM\ManyToOne]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS, InterventionServiceOffer::GROUP_CREER_ODS])]
    private ?GedClient $gedClient = null;

    /** @var array<string,mixed>|null $gedClientConfiguration */
    #[ORM\Column(nullable: true)]
    #[Groups([InterventionServiceOffer::GROUP_CREER_ODS])]
    private ?array $gedClientConfiguration = [];

    /** @var array<string> $availableEquipementsIds */
    #[ORM\Column(type: 'json', options: ['default' => '[]'])]
    #[Groups([InterventionServiceOffer::GROUP_CREER_ODS])]
    private array $availableEquipementsIds = [];

    /** @var array<Equipement> $availableEquipements */
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    private array $availableEquipements = [];

    public function getTemplate(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array<string,string>
     */
    public function getAvailableRoles(): array
    {
        $availableRoles = [
            'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_SUPERVISEUR' => 'Superviseur',
            'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_DECLARANT' => 'Déclarant',
            'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_INTERVENANT' => 'Intervenant',
            'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_DEMANDEUR' => 'Demandeur',
            'SERVICEOFFER_'.$this->getId().'_ROLE_INTERVENTION_OBSERVATEUR' => 'Observateur',
        ];

        return array_merge(parent::getAvailableRoles(), $availableRoles);
    }

    public function getGmaoConfiguration(): ?GmaoConfigurationIntervention
    {
        return $this->gmaoConfiguration;
    }

    public function setGmaoConfiguration(?GmaoConfigurationIntervention $gmaoConfiguration): self
    {
        $this->gmaoConfiguration = $gmaoConfiguration;

        return $this;
    }

    /**
     * @return array<string,string> un tableau associatif du type "libellé de la nature"=>"id de la nature"
     */
    public function getActionTypesMap(): array
    {
        if (is_null($this->gmaoConfiguration)) {
            throw new RuntimeException('Veuillez renseigner une configuration GMAO.');
        }

        return $this->gmaoConfiguration->getActionTypesMap();
    }

    public function getGedClient(): ?GedClient
    {
        return $this->gedClient;
    }

    public function setGedClient(?GedClient $gedClient): self
    {
        $this->gedClient = $gedClient;

        return $this;
    }

    /**
     * @return array<string,mixed>|null
     */
    public function getGedClientConfiguration(): ?array
    {
        return json_decode(json_encode($this->gedClientConfiguration), true);
    }

    /**
     * @param array<string,mixed>|null $gedClientConfiguration
     */
    public function setGedClientConfiguration(?array $gedClientConfiguration): self
    {
        $this->gedClientConfiguration = $gedClientConfiguration;

        return $this;
    }

    public function getGedClientConfigurationDocumentation(): ?string
    {
        return $this->gedClient?->getConfigurationDocumentation();
    }

    /**
     * @return array<string>
     */
    public function getAvailableEquipementsIds(): array
    {
        return $this->availableEquipementsIds;
    }

    /**
     * @param array<string> $equipements
     */
    public function setAvailableEquipementsIds(array $equipements): self
    {
        $this->availableEquipementsIds = array_values($equipements);

        return $this;
    }

    /**
     * @return array<Equipement>
     */
    public function getAvailableEquipements(): array
    {
        $equipements = $this->availableEquipements;

        // Extrait les labels des équipements
        $labels = array_map(fn ($equipement) => $equipement->getLabel(), $equipements);

        // Trie les labels et maintient l'association avec les équipements
        array_multisort($labels, SORT_ASC | SORT_NATURAL | SORT_FLAG_CASE, $equipements);

        return $equipements;
    }

    /**
     * @param array<Equipement> $equipements
     */
    public function setAvailableEquipements(array $equipements): self
    {
        $this->availableEquipements = $equipements;

        return $this;
    }

    /**
     * @return array<string>
     */
    public static function getGedClientConfigurationVariableKeys(): array
    {
        return array_keys(InterventionRepository::getGedClientConfigurationVariable());
    }

    public function utiliseUneGED(): bool
    {
        return !is_null($this->gedClient);
    }
}
