<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class InterventionStatusHistorised implements InterventionStatusInterface
{
    public function __construct(
        #[Groups(['Lister interventions', 'Afficher détails intervention'])]
        private string $interventionId,
        #[Groups(['Lister interventions', 'Afficher détails intervention'])]
        private string $label,
        #[Groups(['Lister interventions', 'Afficher détails intervention'])]
        private ?string $createdBy,
        #[Groups(['Lister interventions', 'Afficher détails intervention'])]
        private ?\DateTimeInterface $createdAt
    ) {
    }

    public function getInterventionId(): string
    {
        return $this->interventionId;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
}
