<?php

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use App\State\InterventionStatusDataHandler;
use Symfony\Component\Serializer\Annotation\Groups;

#[Post(
    uriTemplate: 'intervention-service-offers/{serviceOfferId}/interventions/{interventionId}/statuses',
    uriVariables: [
        'serviceOfferId' => 'serviceOfferId',
        'interventionId' => 'interventionId',
    ],
    security: "is_granted('SERVICEOFFER_' ~ serviceOfferId ~ '_ROLE_INTERVENTION_SOLDER_INTERVENTIONS')",
    denormalizationContext: ['groups' => ['post:InterventionStatus']],
    read: false,
    processor: InterventionStatusDataHandler::class,
)]
class InterventionStatus implements InterventionStatusInterface
{
    private ?string $interventionId = null;
    private ?string $createdBy = null;
    private ?\DateTimeInterface $createdAt = null;

    public function __construct(
        #[Groups(['post:InterventionStatus'])]
        private string $label,
    ) {
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getInterventionId(): ?string
    {
        return $this->interventionId;
    }

    public function setInterventionId(string $interventionId): self
    {
        $this->interventionId = $interventionId;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
