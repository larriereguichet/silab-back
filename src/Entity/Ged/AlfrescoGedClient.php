<?php

namespace App\Entity\Ged;

use App\Exception\RuntimeException;
use App\Repository\Ged\AlfrescoGedClientRepository;
use App\Service\Utils;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[ORM\Entity(repositoryClass: AlfrescoGedClientRepository::class)]
class AlfrescoGedClient extends GedClient
{
    //  QUESTION_TECHNIQUE comment autowire httpClientInterface ici?

    #[ORM\Column(length: 255)]
    private ?string $username = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    private ?string $instanceUrl = null;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getInstanceUrl(): ?string
    {
        return $this->instanceUrl;
    }

    public function setInstanceUrl(string $instanceUrl): self
    {
        $this->instanceUrl = $instanceUrl;

        return $this;
    }

    private function getRepositoryUrl(): string
    {
        return $this->instanceUrl.'/alfresco/api/-default-/public/cmis/versions/1.1/browser';
    }

    private function getImageVisualizationUrl(string $objectId): string
    {
        return $this->instanceUrl."/share/proxy/alfresco/slingshot/node/content/workspace/SpacesStore/$objectId";
    }

    private function extractObjectIdFromFileUrl(string $url): string
    {
        // Note nous acceptons deux formats d'url, principalement pour des raison de rétro-compatibilité :
        // auparavant nous fonctionnnions avec des url de type $repositoryUrlRegex,
        // dorénavent nous utilisons des url du type $sharedUrlRegex
        $repositoryUrlRegex = '/(?<instanceUrl>.*)\/alfresco\/api\/-default-\/public\/cmis\/versions\/1\.1\/browser\/root\?objectId=(?<objectId>.*)$/U';
        $sharedUrlRegex = '/(?<instanceUrl>.*)\/share\/proxy\/alfresco\/slingshot\/node\/content\/workspace\/SpacesStore\/(?<objectId>.*)$/U';

        $match = null;
        if (preg_match($repositoryUrlRegex, $url, $match)) {
            return $match['objectId'];
        } elseif (preg_match($sharedUrlRegex, $url, $match)) {
            return $match['objectId'];
        }

        throw new RuntimeException("Format d'url Alfresco inconnu ($url), impossible de déduire l'objectId");
    }

    private function getObjectRootUrl(string $objectId): string
    {
        return $this->getRepositoryUrl()."/root?objectId=$objectId";
    }

    private function getObjectRootUrlFromFileUrl(string $url): string
    {
        return $this->getObjectRootUrl($this->extractObjectIdFromFileUrl($url));
    }

    private function getAlfrescoHttpClient(): HttpClientInterface
    {
        return HttpClient::createForBaseUri(
            $this->getInstanceUrl(),
            [
                'auth_basic' => [
                    $this->getUsername(),
                    $this->getPassword(),
                ],
            ]
        );
    }

    public function persistFile(string $fileB64, string $name, array $configuration = [], array $documentData = []): string
    {
        if (empty($configuration['folderPath'])) {
            throw new RuntimeException("La variable 'folderPath' n'est pas défini dans la configuration fournit.");
        }

        $fileData = base64_decode($fileB64);

        $fileExtension = explode('/', finfo_buffer(finfo_open(), $fileData, FILEINFO_EXTENSION))[0];
        $uniqueName = $name.Uuid::v4().'.'.$fileExtension;
        $fileMimeType = finfo_buffer(finfo_open(), $fileData, FILEINFO_MIME);

        $formFields = [
            'cmisaction' => 'createDocument',
            'propertyId' => [
                'cmis:objectTypeId',
                'cmis:name',
                ],
            'propertyValue' => [
                'cmis:document',
                $uniqueName,
                ],
        ];

        // propriétés natives
        $ilYADesProrietesNativesARenseigner = array_key_exists('objectTypeProperties', $configuration);
        if ($ilYADesProrietesNativesARenseigner) {
            foreach ($configuration['objectTypeProperties'] as $propertyId => $propertyValue) {
                $formFields['propertyId'][] = $propertyId;
                $formFields['propertyValue'][] = Utils::replaceKeysInStringFromArray($propertyValue, $documentData);
            }
        }

        // propriétés secondaires
        $ilYADesProrietesSecondairesARenseigner = array_key_exists('secondaryObjectTypes', $configuration);
        if ($ilYADesProrietesSecondairesARenseigner) {
            // en cmis, il faut déclarer au préalable les types secondaires dans un tableau
            $formFields['propertyId'][] = 'cmis:secondaryObjectTypeIds';
            $formFields['propertyValue'][] = array_keys($configuration['secondaryObjectTypes']);

            foreach ($configuration['secondaryObjectTypes'] as $secondaryObjectTypeProperties) {
                foreach ($secondaryObjectTypeProperties as $propertyId => $propertyValue) {
                    $formFields['propertyId'][] = $propertyId;
                    $formFields['propertyValue'][] = Utils::replaceKeysInStringFromArray($propertyValue, $documentData);
                }
            }
        }

        $formFields['file'] = new DataPart(
            $fileData,
            $uniqueName,
            $fileMimeType
        );

        $formData = new FormDataPart($formFields);

        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable(),
        ];

        $versionedObjectId = $this->getAlfrescoHttpClient()->request('POST', $configuration['folderPath'], $options)
                        ->toArray()['properties']['cmis:objectId']['value'];

        // échappe la version du fichier afin de toujours pointer sur la dernière version, c'est lecomportement par défaut quand auucune version n'est precisée.
        $objectId = substr(
            $versionedObjectId,
            0,
            strrpos($versionedObjectId, ';')
        );

        return $this->getImageVisualizationUrl($objectId);
    }

    public function updateFile(string $fileUrl, string $newFileB64, ?string $filename): void
    {
        $this->checkIn(
            $this->checkOut($this->getObjectRootUrlFromFileUrl($fileUrl)),
            $newFileB64,
            $filename
        );
    }

    private function checkOut(string $fileUrl): string
    {
        $formData = new FormDataPart(['cmisaction' => 'checkOut']);
        $options = [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable(),
        ];

        return $this->getAlfrescoHttpClient()->request('POST', $fileUrl, $options)
        ->toArray()['properties']['cmis:objectId']['value'];
    }

    private function checkIn(string $pwcId, string $newFileB64, ?string $filename = ''): void
    {
        $fileData = base64_decode($newFileB64);
        $fileExtension = explode('/', finfo_buffer(finfo_open(), $fileData, FILEINFO_EXTENSION))[0];
        $uniqueName = $filename.Uuid::v4().'.'.$fileExtension;
        $fileMimeType = finfo_buffer(finfo_open(), $fileData, FILEINFO_MIME);

        $formFields = [
            'cmisaction' => 'checkIn',
            'file' => new DataPart(
                $fileData,
                $uniqueName,
                $fileMimeType
            ),
            ];

        $formData = new FormDataPart($formFields);

        $options = [
             'headers' => $formData->getPreparedHeaders()->toArray(),
             'body' => $formData->bodyToIterable(),
        ];

        $this->getAlfrescoHttpClient()->request(
            'POST',
            $this->getObjectRootUrl($pwcId),
            $options
        );
    }

    public function getFile(string $fileUrl): string
    {
        $response = $this->getAlfrescoHttpClient()->request('GET', $this->getObjectRootUrlFromFileUrl($fileUrl));

        return $response->getContent();
    }

    public function getConfigurationDocumentation(): string
    {
        return <<<DOCUMENTATION
{
    "folderPath": "alfresco/api/-default-/public/cmis/versions/1.1/browser/root/Sites/sipatrimoine/documentLibrary/DOSSIER_1/DOSSIER_N",// Attention, ne pas mettre de "/" au début de l'url sauf en connaissance de cause, voir : https://symfony.com/doc/current/reference/configuration/framework.html#base-uri
    "objectTypeProperties": {// cmis:objectTypeId et cmis:name n'ont pas besoin d'êtres surchargés ici, ils seront définis par l'implémentation de base
        "cmis:description": "{{Object.decription}}"
    },
    "secondaryObjectTypes" : {// Assi appelés "aspects" sur Alfresco
        "P:cm:titled": {// aspect natif alfresco
            "cm:title":"{{Object.title}}"
        },
        "P:carl:WO": {// aspect personalisé
            "carl:woCode": "{{Object.code}}"
        },
        "P:silab2:main": {// aspect personalisé
            "silab2:mainApp": "{{ServiceOffer.title}}",
            "silab2:example1": "valeur example1",
            "silab2:example2": "valeur example2 avec valeur dynamique {{Class.property}}"
        }
    }
}
DOCUMENTATION;
    }
}
