<?php

namespace App\Entity\Ged;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Repository\Ged\GedClientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[Get(
    security: "is_granted('ROLE_SERVICEOFFER_CONSULTER_GED_CLIENTS')"
)]
#[GetCollection(
    security: "is_granted('ROLE_SERVICEOFFER_CONSULTER_GED_CLIENTS')"
)]
#[ORM\Entity(repositoryClass: GedClientRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'alfresco' => AlfrescoGedClient::class,
])]
abstract class GedClient
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([InterventionServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    private ?string $title = null;

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param array<mixed> $configuration
     * @param array<mixed> $documentData
     *
     * @return string L'url d'accès de l'image stockée
     */
    abstract public function persistFile(string $fileB64, string $name, array $configuration, array $documentData): string;

    abstract public function updateFile(string $fileUrl, string $newFileB64, ?string $filename): void; // note A terme il faudrait retourner l'url de l'image.

    abstract public function getFile(string $fileUrl): string;

    abstract public function getConfigurationDocumentation(): string;
}
