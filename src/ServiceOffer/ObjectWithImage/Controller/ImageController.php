<?php

namespace App\ServiceOffer\ObjectWithImage\Controller;

use App\Entity\Intervention;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Exception\RuntimeException;
use App\InterventionServiceOffer\DemandeIntervention\Entity\DemandeIntervention;
use App\InterventionServiceOffer\DemandeIntervention\Repository\DemandeInterventionRepository;
use App\InterventionServiceOffer\Intervention\Repository\InterventionRepository;
use App\ServiceOffer\ServiceOffer\Entity\GedUserServiceOfferInterface;
use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// cf. https://stackoverflow.com/a/64045615
#[Route(
    '/service-offers/{serviceOfferId}/{objectType}/{objectId}/image',
    name: 'serviceOffer_object_image',
    defaults: ['_signed' => true]
)]
class ImageController extends AbstractController
{
    public function __invoke(
        string $serviceOfferId,
        string $objectType,
        string $objectId,
        ServiceOfferRepository $serviceOfferRepository,
    ): Response {
        // étape 1 vérifier la signature de l'url, cf. https://les-tilleuls.coop/blog/url-signer-bundle-create-validate-signed-url-symfony-en
        // -> `defaults: ['_signed' => true]` dans les paramètres de la route

        // étape 2 récupérerer l'image sur la ged ou en direct
        $serviceOffer = $serviceOfferRepository->find($serviceOfferId);

        $objectRepository = match ($serviceOffer->getTemplate()) {
            'InterventionServiceOffer' => (function () use ($serviceOffer, $objectType) {
                assert($serviceOffer instanceof InterventionServiceOffer);

                return match ($objectType) {
                    Intervention::class => new InterventionRepository($serviceOffer),
                    DemandeIntervention::class => new DemandeInterventionRepository($serviceOffer),
                    default => throw new RuntimeException("Les objects de type $objectType ne sont pas supportées.")
                };
            })(),
            default => throw new RuntimeException('Les offres de service de type '.$serviceOffer->getTemplate().' ne sont pas supportées.')
        };

        $objectImageUrl = $objectRepository->find($objectId)->getImageUrl();

        if (empty($objectImageUrl)) {
            $response = new Response();
            $response->setStatusCode(Response::HTTP_NO_CONTENT);
            $response->headers->set('Cache-Control', 'no-cache');

            return $response;
        }

        if ($serviceOffer instanceof GedUserServiceOfferInterface && $fileData = $serviceOffer->utiliseUneGED()) {
            $fileData = $serviceOffer->getGedClient()->getFile($objectImageUrl);
        } else {
            $fileData = file_get_contents($objectImageUrl);
        }

        // étape 3 rendre l'image au client
        // see : https://stackoverflow.com/a/41805613
        $fileMimeType = finfo_buffer(finfo_open(), $fileData, FILEINFO_MIME);

        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', $fileMimeType);
        $response->headers->set('Content-length', (string) strlen($fileData));
        $response->sendHeaders();
        $response->setContent($fileData);

        return $response;
    }
}
