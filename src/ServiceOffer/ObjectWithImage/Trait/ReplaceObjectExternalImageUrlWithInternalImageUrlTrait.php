<?php

namespace App\ServiceOffer\ObjectWithImage\Trait;

use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\Shared\ObjectWithImage\Entity\ObjectWithImageInterface;
use CoopTilleuls\UrlSignerBundle\UrlSigner\UrlSignerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Service\Attribute\Required;

trait ReplaceObjectExternalImageUrlWithInternalImageUrlTrait
{
    private UrlSignerInterface $urlSigner;
    private RouterInterface $router;
    private LoggerInterface $logger;

    #[Required]
    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }

    #[Required]
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    #[Required]
    public function setUrlSigner(UrlSignerInterface $urlSigner): void
    {
        $this->urlSigner = $urlSigner;
    }

    /**
     * Note: si l'url de l'image en entrée en null, alors on ne remplace pas.
     *
     * @template T of ObjectWithImageInterface
     *
     * @param T $objectWithImage
     *
     * @return T
     */
    private function replaceObjectExternalImageUrlWithInternalImageUrl(ServiceOffer $serviceOffer, ObjectWithImageInterface $objectWithImage): ObjectWithImageInterface
    {
        if (is_null($objectWithImage->getImageUrl())) {
            return $objectWithImage;
        }

        $this->logger->debug(
            'router context scheme',
            [
                '$routerContext' => $this->router->getContext(),
                '$routerContext->getScheme()' => $this->router->getContext()->getScheme(),
                '$routerContext->getBaseUrl()' => $this->router->getContext()->getBaseUrl(),
                '$routerContext->getHost()' => $this->router->getContext()->getHost(),
                '$routerContext->getHttpPort()' => $this->router->getContext()->getHttpPort(),
                '$routerContext->getHttpsPort()' => $this->router->getContext()->getHttpsPort(),
                '$routerContext->getParameters()' => $this->router->getContext()->getParameters(),
                '$getallheaders()' => getallheaders(),
            ]
        );

        $internalImageUrl = $this->router->generate(
            'serviceOffer_object_image',
            [
                'serviceOfferId' => $serviceOffer->getId(),
                'objectType' => $objectWithImage::class,
                'objectId' => $objectWithImage->getId(),
            ],
            RouterInterface::ABSOLUTE_URL
        );

        $signedInternalImageUrl = $this->urlSigner->sign(
            $internalImageUrl,
            (new \DateTime('now'))->add(new \DateInterval('PT20M'))
        );

        return $objectWithImage->setImageUrl($signedInternalImageUrl);
    }

    /**
     * @template T of ObjectWithImageInterface
     *
     * @param array<T> $objectsWithImage
     *
     * @return array<T>
     */
    private function replaceObjectsExternalImageUrlWithInternalImageUrl(ServiceOffer $serviceOffer, array $objectsWithImage): array
    {
        return array_map(
            function ($objectsWithImage) use ($serviceOffer) {
                return $this->replaceObjectExternalImageUrlWithInternalImageUrl($serviceOffer, $objectsWithImage);
            },
            $objectsWithImage
        );
    }
}
