<?php

namespace App\ServiceOffer\ServiceOffer\Entity;

use App\Entity\Ged\GedClient;

interface GedUserServiceOfferInterface
{
    public function getGedClient(): ?GedClient;

    public function setGedClient(?GedClient $gedClient): self;

    /**
     * @return array<string,mixed>|null
     */
    public function getGedClientConfiguration(): ?array;

    /**
     * @param array<string,mixed>|null $gedClientConfiguration
     */
    public function setGedClientConfiguration(?array $gedClientConfiguration): self;

    public function getGedClientConfigurationDocumentation(): ?string;

    /**
     * @return array<string>
     */
    public static function getGedClientConfigurationVariableKeys(): array;

    public function utiliseUneGED(): bool;
}
