<?php

namespace App\ServiceOffer\ServiceOffer\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\ElusServiceOffer\ElusServiceOffer\Entity\ElusServiceOffer;
use App\Entity\ServiceOffer\Template\InterventionServiceOffer;
use App\Entity\ServiceOffer\Template\RedirectServiceOffer;
use App\LogisticServiceOffer\LogisticServiceOffer\Entity\LogisticServiceOffer;
use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use App\ServiceOffer\ServiceOffer\State\GetCollectionServiceOfferProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[GetCollection(
    provider: GetCollectionServiceOfferProvider::class,
    normalizationContext: ['groups' => [ServiceOffer::GROUP_LISTER_ODS]],
    order: ['title' => 'ASC']
)]
#[Get(
    security: "is_granted('SERVICEOFFER_' ~ object.getId() ~ '_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE')",
    normalizationContext: ['groups' => [ServiceOffer::GROUP_AFFICHER_DETAILS_ODS]]
)]
#[ORM\Entity(repositoryClass: ServiceOfferRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'template', type: 'string')]
#[ORM\DiscriminatorMap([
    'intervention' => InterventionServiceOffer::class,
    'redirect' => RedirectServiceOffer::class,
    'logistic' => LogisticServiceOffer::class,
    'elus' => ElusServiceOffer::class,
])]
abstract class ServiceOffer
{
    public const GROUP_AFFICHER_DETAILS_ODS = 'Afficher les détails d\'une offre de service';
    public const GROUP_LISTER_ODS = 'Lister les offres de service';
    public const GROUP_CREER_ODS = 'Créer une offre de service';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    protected ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $link = null;

    #[ORM\Column(length: 255)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $title = null;

    #[ORM\Column(length: 255)]
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS, ServiceOffer::GROUP_CREER_ODS])]
    protected ?string $image = null;

    /**
     * @return array<string,string>
     */
    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    public function getAvailableRoles(): array
    {
        return [
            'SERVICEOFFER_'.$this->getId().'_ROLE_SERVICEOFFER_ADMIN' => 'Administrateur',
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): static
    {
        $this->link = $link;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): static
    {
        $this->image = $image;

        return $this;
    }

    #[Groups([ServiceOffer::GROUP_LISTER_ODS, ServiceOffer::GROUP_AFFICHER_DETAILS_ODS])]
    abstract public function getTemplate(): string;
}
