<?php

namespace App\ServiceOffer\ServiceOffer\State;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\ServiceOffer\ServiceOffer\Entity\ServiceOffer;
use App\ServiceOffer\ServiceOffer\Repository\ServiceOfferRepository;
use App\Shared\Security\Role\ScopedRoleHierarchyInterface;
use App\Shared\User\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class GetCollectionServiceOfferProvider implements ProviderInterface
{
    public function __construct(private ServiceOfferRepository $serviceOfferRepository, private Security $security, private ScopedRoleHierarchyInterface $scopeRoleHierarchy)
    {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return array<ServiceOffer>|ServiceOffer
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|ServiceOffer
    {
        assert($operation instanceof CollectionOperationInterface);

        $curentUser = $this->security->getUser();

        assert($curentUser instanceof User);

        $curentUser->setReachableRoles($this->scopeRoleHierarchy->getReachableRoleNames($curentUser->getRoles()));

        // On récupère l'id des offres de services accessible à l'utilisateur. Elle sont incluse dans les rôles
        $serviceOffersIdAuthorized = array_reduce(
            $curentUser->getReachableRoles(),
            function ($serviceOffersIds, $role) {
                if (1 === preg_match('/SERVICEOFFER_(?<serviceOfferId>\d+)_ROLE_SERVICEOFFER_CONSULTER_OFFRE_DE_SERVICE/', $role, $matches)) {
                    $serviceOffersIds[] = $matches['serviceOfferId'];
                }

                return $serviceOffersIds;
            },
            []
        );

        $criteria = ['id' => $serviceOffersIdAuthorized];

        return $this->serviceOfferRepository->findBy($criteria, orderBy: $operation->getOrder());
    }
}
