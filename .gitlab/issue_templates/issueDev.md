# :woman: :speech_balloon: Objectif

> **En tant que** ... **je peux** ... **afin de** ...



# :heavy_check_mark: Definition Of Ready :

    -   [ ] Titre le plus court possible et compréhensible
    -   [ ] Objectif définit
    -   [ ] Epic attribuée
    -   [ ] Poids estimé
    -   [ ] Label `Affinage::Ready`

# :books: Ressources
