server {
	listen 5001;
    root /srv/api/public;
	## logging ##
	access_log /var/log/nginx/access.log combined;
	error_log /var/log/nginx/error.log error;
	# buffer configuration to prevent "send too big header" type errors
	proxy_buffer_size 256k;
	proxy_buffers 4 512k;
	proxy_busy_buffers_size 512k;

	location / {
		try_files $uri /index.php$is_args$args;
	}
	location ~ ^/index\.php(/|$) {
		fastcgi_pass unix:/var/run/php/php-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        # buffer configuration to prevent "send too big header" type errors
		fastcgi_buffer_size 32k;
		fastcgi_buffers 8 16k;

		include fastcgi_params;
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $document_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/app.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
	}
	# return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }
}
