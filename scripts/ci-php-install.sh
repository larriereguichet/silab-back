#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

command -v apt &> /dev/null || exit 0

set -eux

apt-get update && apt-get install -y git curl zip libzip-dev libpq-dev
curl -sSk https://getcomposer.org/installer | php -- --disable-tls && mv composer.phar /usr/local/bin/composer
docker-php-ext-install pdo pdo_pgsql zip
apt-get autoremove -y
pecl install xdebug
docker-php-ext-enable xdebug
