#! /bin/bash
set -eux
echo "Lancement des tests sur $TEST_CARL_BASEURL"
php bin/console doctrine:database:drop --if-exists -f -n -e test
php bin/console doctrine:database:create -n -e test
php bin/console doctrine:migration:migrate -n -e test
php bin/console doctrine:schema:update -f -n -e test
XDEBUG_MODE=coverage php bin/phpunit \
    --log-junit reports/phpunit.xml \
    --coverage-text \
    --coverage-cobertura reports/coverage-cobertura.xml \
    --coverage-clover reports/coverage.xml \
    --colors=never \
    tests
