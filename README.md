# [SI]lab back [![pipeline status](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/badges/main/pipeline.svg)](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/-/commits/main) [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=incubateur-territoires_silab-back&metric=coverage)](https://sonarcloud.io/summary/new_code?id=incubateur-territoires_silab-back) [![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=incubateur-territoires_silab-back)](https://sonarcloud.io/summary/new_code?id=incubateur-territoires_silab-back) [![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=incubateur-territoires_silab-back)

Dépôt pour la partie back de **[SI]lab** v2

## Sommaire

- [Installation de l'environnement de dev [⮫suivant]](docs/installation.md)
- [Développement](docs/developpement.md)
- [CI/CD](https://gitlab.com/incubateur-territoires/france-relance/silab/silab-back/-/ci/editor?branch_name=main&tab=2)
- [Déploiement en production](docs/environnements.md#production)
- [CONTRIBUTING](docs/CONTRIBUTING.md)
- [Configuration de silab](docs/configuration.md)
